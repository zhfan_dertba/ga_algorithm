Genetic Algorithm for catalysis
==========================

## Note:

   This script should be run by python2 (NOT python3)               
   Numba is strongly recommended to accelerate parts of the script  



## Prepare the input files

All the \*.py (ga.py, run_main.py, molecules_dev.py, rotation.py and interface.py) coordinates, and DFT input files should be in the same folder

### Population coordinates file 

Named as 0, 1, 2, ...; the number of these files should be the size of the population, each of the file will have:
Format: 

      lat[0,0], lat[0,1], lat[0,2]          # lattice vector 1
      lat[1,0], lat[1,1], lat[1,2]          # lattice vector 2
      lat[2,0], lat[2,1], lat[2,2]          # lattice vector 3
      1.0   NumAtoms                        # 1.0: not used; NumAtoms: number of total atoms (count all atoms)
      Element   x    y   z   0   0   0      # Element: atomic species (e.g. Cu); x,y,z are 
      Element   x    y   z   0   0   0      #    cartesian coordinates (Angstrom unit); 
      Element   x    y   z   0   0   0      #    0 0 0: if this atom will be fixed during DFT relaxations
      ...

**Note**: if the system has substrate and you want to let the program to 
        generate the molecules randomly, in Population coordinates files,
        you only need to specify the coordinates of the substrate, 
        e.g. 16 water molecules on top of Cu substrate:

                 10.56716000       0.00000000       0.00000000
                 -5.28358000       9.15142901       0.00000000
                  0.00000000       0.00000000      17.74720000
             1.0   64
             Cu         -5.279854         9.146894         2.078472  0   0   0
             Cu         -1.319508         2.290951         2.079613  0   0   0
             Cu         -2.645626         4.581287         2.076942  0   0   0
             Cu          6.602988         6.861446         2.067346  0   0   0
             Cu          2.642070         0.002239         2.072943  0   0   0
             Cu          1.327837         2.296099         2.070152  0   0   0
             Cu          0.015208         4.579716         2.078198  0   0   0
             Cu         -1.319653         6.861767         2.068882  0   0   0
             Cu         -0.005260         9.150237         2.078252  0   0   0
             Cu          3.955498         2.302524         2.069347  0   0   0
             Cu          2.635662         4.579578         2.313564  0   0   0
             Cu          1.320539         6.852213         2.071944  0   0   0
             Cu          2.640258         9.146060         2.079292  0   0   0
             Cu          6.599609         2.291139         2.074925  0   0   0
             Cu          5.258269         4.578494         2.071277  0   0   0
             Cu          3.962280         6.847565         2.069218  0   0   0
             ... ...

        In the above example, the coordinates of the water molecules are not needed to specify, but substrate coordinates are needed here.

### structure-energy.file

Format:

    GeneInd    NumPop
     FileName    TotEnergy
     FileName    TotEnergy
     FileName    TotEnergy
     FileName    TotEnergy
     FileName    TotEnergy
     ...

For initial setup, you can leave the energy blank (e.g. here there are 10 structures in this population) for example:

    0   10
     0 
     1
     2
     3
     4
     5
     6
     7
     8
     9

During the calculation, this file will be updated

All the input files used for DFT calculation, such as parameter file (INCAR/etot.input) and psp files should be 
           in the same folder


## setup run_main.py

### Setup information to describe system. Following is the classes needed to be setup.

**System parameters**: flags for parallelization, input/output file, DFT package, DFT relaxation

**Restart setting**: setup if restart if needed

**setup shape class**: shape is to describe the system structure (such as it is an interfacial structure or cluster structure, need any constrain on initial generation of molecules?)

**setup molecule class**: tell the program about the molecules for GA. e.g. which atoms belong to one molecule, their species, if one molecule is fixed, the relative coordinates within one molecule

**setup adjust class**: To avoid 'too-close' between two atoms after GA crossing (it will crash DFT relaxation), a steepest descent quick relaxation is performed 

**setup DFT-IO class**: To declare the DFT-IO 

**setup DFT-relax class**: To declare the DFT-relax for HPC running

**setup GA class**: To declare the GA methods

### setup system parameters 

This class contains parallelization/package information.

        structure_energy_file = 'structure-energy.dat'   # specify the file name to record structure and energy
        Num_Process = 10                                 # how many relaxations are launched at one time (best to be the same to num. of populations for relaxation)
        cores_adjust = 10                                # number of processes for GA adjusting (usually is the number of populations)
        para_cmd = 'srun -N 2 -n 64'                     # MPI launch wrapper and parallel info
        Package = 'qe'                                   # package string (which DFT package is used. qe, vasp, pwmat, lammps)
        exec_cmd = 'pw.x -npool 1 -ndiag 16 -ntg 4  '    # execution of DFT
        MaxGenerations = 1000                            # maximum generation allowed
        MaxRunTime = 5000                                # how long (second) can a DFT relaxation run (will kill job if it exceeds this time)
                                                         # kill an MPI job could be challenging. It is better to setup max relaxation
                                                         #    steps in a DFT input and give long enough MaxRunTime to finish the DFT
                                                         #    relaxations by itself
        files_dump = ['in_relax','out_relax']            # DFT output files you want to archive (saved in dump folder)
        SetupInput = ['in_relax','O.SG15.NCPP.PBE.UPF','H.SG15.NCPP.PBE.UPF','C.SG15.NCPP.PBE.UPF','Cu.SG15.NCPP.PBE.UPF']  
                                                         # this file list contains the files used for DFT relaxations, 
                                                         #    depending on the specific package used.


### setup restart 

if restart, the ga_cycle will do ga_cycle(1) at the beginning, 
        so structure-energy.dat should be in the directory, 
        $i coords file should also be there
 
In run_main.py, modify the following line:

     Restart = True    # restart from last step

or

     Restart = False   # start from scratch



### setup Shape class

explain of flags used in this class, value in the "()" is the default.


- Specify **constrain planes/sphere** (this is mainly to limit molecules to form in a specific shape)
  constrain plane/sphere can be added to *surface* or *cluster* cases
      
      **ConstrainPlane: [[a,b,c,d],+/-1]**.

        a, b, c, d defines the function of the plane.

        +1: the molecules should be in the side of norm-direction of the plane

        -1: the molecules should be at the opposite direction

      **ConstrainPolar: [[x,y,z],r1, r2]**. molecules should be between r1 and r2

        [x,y,z] is the center of the sphere

        r1: inner sphere radius (r1 could be None); 

        r2: outer sphere radius
        

      For example


         ConstrainPlane = [[[0.0, 0.0, 1.0, 2.1], 1],[[0.0,0.0,1.0, 14.0],-1]]   # means two planes, first-plane function z=2.1 (Angstrom), molecules should be above this plane
                                                                                 #     the other plane z=14.0, molecules should be below this plane.
                                                                                 #     the last number for each plane (+1/-1) specify if the generated molecules are in the same
                                                                                 #     direction of the plane-normal direction
         ConstrainPolar = [[[3.15, 6.83, 5.44],None, 5.5]]                       # means one spherical constrain, its center is at coord (3.15, 6.83, 5.44), the inner radius is not needed,
                                                                                 #     the outer radius is 5.5 Angstrom, molecules should be within outer- and innter-radius

- Shape of sample system (**'open'** / **'ortho'** / **'sphere'** / **'cylinder'**)

     - 'open' means the molecules are sampled to fill an empty space or a squre cuboid box
  
         other related flags:
      
         **MaxZ**(optional): maximum Z value for the open sampling       
      
         **MinZ**(optional): minimum Z value for the open sampling       
      
         **InitSampleDist2Center**(optional): half of the squre cuboid bottom box size
      
         **SampleMinDist**(2.3): minmum distance between two molecule for initial sample
  
     - 'sphere' means the molecules are sampled in a sphere-like cluster
  
          other related flags:
       
          **InitOuterRad**: radius of cluster outer size
       
          **OriginCoord**: coordinate of the center of sphere cluster
       
          **InitInnerRad**: radius of inner radius of cluster. In this case, the cluster could have the shape of a shell
       
          **SampleMinDist**(2.3): minmum distance between two molecule for initial sample
       
          **MaxZ**(optional): maximum Z value for the open sampling       
       
          **MinZ**(optional): minimum Z value for the open sampling       
       
     - 'ortho' means the molecules are sampled in a parallelepiped box
       
          other related flags:
       
          **A_vec**: defining plane 1
       
          **B_vec**: defining plane 2
       
          **C_vec**: defining plane 3
       
          **SampleMinDist**(2.3): minmum distance between two molecule for initial sample
       
     - 'cylinder' means the molecules are sampled in a cylinder-like cluster
       
          other related flags:
       
          **Radius**: radius of cylinder bottom
       
          **OriginCoord**: coordinate of the center of cylinder bottom
       
          **MaxZ**: maximum Z value for the cylinder
       
          **MinZ**: minimum Z value for the cylinder
       
          **SampleMinDist**(2.3): minmum distance between two molecule for initial sample
  
     - If this is the case and with a substrate :
  
         **WithSubstrate: (False)**:
               if there is a substrate in the system
  
         **SubstrateShapeStr**:    
               
               **'sphere'**: substrate has sphere shape
   
               **'surface'**: substrate is a surface (slab-type)

- Setup class, for example:

      d_shape = shape('cylinder', OriginCoord=np.array([4.4, 7.60, 6.]), Radius=7.2, MaxZ=8.0, MinZ=6.3, WithSubstrate=True, SubstrateShapeStr='surface', SubstrateAtomsSpecies=['Cu'])
      

### setup Molecule class

set up input molecules  *Note*: PWmat will re-sort atom for final.config
         
- The name of each atom.

   For example:

   
       InputMoleName =  ['C'] + ['O']*16 + ['H']*31
  
- InputMole: Specify which atoms belong to "a" molecule. Each element in the list indicates one molecule. Each number within this element will tell which atom belongs to this molecule
                       *Note*: Index of each atom is consistent to InputMoleName order

   For example based on InputMoleName:
   
   
       InputMole = [[0,1],[2,17,18, 19],[3,20,21],[4,22,23],[5,24,25],[6,26,27],[7,28,29],[8,30,31],[9,32,33],[10,34,35],[11,36,37],[12,38,39],[13,40,41],[14,42,43],[15,44,45],[16,46,47]]
  

- The coordinates for each atom mentioned in InputMoleName.

    For example, in the following example, first 7 molecules are fixed. The last 9 molecules are H2O molecules. 
                 Since one wants to generate them (the last 9 water) randomly, the coordinates given below (last line) for 
                 them are only the relatively positions for the three atoms (therefore they are the same for the 9 water). The program will generate the 
                 random 9 H2O molecules by shifting/rotating their relative coordinates.

   
        InputMoleCoord = [ np.array([[2.607064 ,  4.600990 ,  4.152463],  
                                     [2.598904 ,  4.613181 ,  5.331681]]),
                           np.array([[4.690877 ,  4.732276 ,  7.288227],  
                                     [4.711815 ,  5.760573 ,  7.542812],
                                     [4.351210 ,  4.094700 ,  8.114064],
                                     [4.068133 ,  4.621081 ,  6.538410]]),
                           np.array([[2.173721 ,  8.145266 ,  8.173797],  
                                     [2.137906 ,  8.777692 ,  8.919514],
                                     [1.848341 ,  8.648043 ,  7.399736]]),
                           np.array([[1.262592 ,  3.397285 ,  9.396255],  
                                     [0.918079 ,  3.107119 , 10.263202],
                                     [0.896307 ,  4.301945 ,  9.249942]]),
                           np.array([[3.919505 ,  3.200274 ,  9.077585],  
                                     [4.406701 ,  3.269019 ,  9.923079],
                                     [2.942043 ,  3.301851 ,  9.278010]]),
                           np.array([[4.759049 ,  7.171030 ,  7.925046],  
                                     [3.860692 ,  7.589241 ,  7.950799],
                                     [5.325180 ,  7.730763 ,  7.356735]]),
                           np.array([[0.206246 ,  5.956350 ,  8.946425],  
                                     [ 0.818419,   6.644462,   8.619243],
                                     [-0.559787,   5.979268,   8.340709]]) ] + 
                           [np.array([[0.0,0.0,0.0],[-0.7563,0.5867,0.0],[0.7563,0.5867,0.0]])]*9   # unfixed nine molecules H2O
  

- FixMolecule. Based on the order of InputMole, which molecule is fixed.

   For example:  `FixMolecules = [0,1,2,3,4,5,6]`.

   It mean the 0-th to 6-th molecules (from InputMole list) are fixed during the GA, its cooridnates will be 
   from InputMoleCoord directly. (There position may not be fixed during DFT relaxation, set FixMole_DFT=True to also fix them in DFT.)
         
- Setup class, for example:

       d_molecules = input_molecules(ForceInputMole=InputMole, ForceInputMoleName=InputMoleName, ForceInputMoleCoord=InputMoleCoord, FixMolecule=FixMolecules)

### setup Atom class

set up input molecules. This is very similar to molecule case. Only molecule or atom class will be set. If both are set, the program will complain.

- The name of each atom.

   For example:

       InputMoleName =  ['C']*15

- InputMole: Specify the order to the atoms acoording to InputMoleName
              *Note*: Index of each atom is consistent to InputMoleName order

   For example based on InputMoleName:

       InputMole = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]

- Setup class, for example:

       d_atoms = input_atoms(ForceInputMole=InputMole, ForceInputMoleName=InputMoleName, ForceInputMoleCoord=InputMoleCoord, FixMolecule=FixMolecules)

### setup Cross/Mute class

         
(explain of flags used in this class, value in the "()" is the default.)

**Mutation**: (False).
      If turn on mutation. Turn on mutation is important for 
      quick serarching. Better to turn it on all the time.

**MutationMethod**: (None).
      Select the mutation method used (2, 3, 4).

      1: Not implemented

      2: exchange two regions of the absorbers; may even rotate them randomly

      3: find a region (consisting random molecules), shift this region in random direction for random steps

      4: select a random region and rotated it randomly

**EveryNStep_Random**: (20)
      After EveryNStep_Random generations, a random structure is 
        added to replace the last one in the population

**CrossingMethod**: (none)
      Select the cross method used (1, 2, 3, 4, 5, 6).

      1: cut randomly, pick up two parts and combine (not recommended)

      2: pick up an atom randomly, rotate the structure so that this atom to be at z-axis. 
           convert atoms to 1D array based on their z coord for each structure. cross between these two 1D arrays

      3: pick up an atom randomly, rotate the structure so that this atom to be at z-axis. 
           convert atoms to 1D array based on their distance to mass center for each structure. cross between these two 1D arrays

      4: convert atoms to 1D array based on their distance to mass center for each structure. cross between these two 1D arrays.
           (no initial rotation compared to method 2 and 3)

      5: same to 2. But when crossing between two 1D arrays, three parts (belonging to one 1D array) are exchanged instead of two parts.

      6: pick up an atom randomly, rotate the structure so that this atom to be at x-axis. 
           convert atoms to 1D array based on their x coord for each structure. cross between these two 1D arrays
           (this method is appropriate for surface)

**MethodProbFile**: (None).
      This file is to record the crossing/mutation methods on the fly.      
      If provided MethodProbFile, FixCrossingMethod and FixMutationMethod should be set to False

**FixCrossingMethod**: (True).

**FixMutationMethod**: (True).

**MaxSamplePlane**: (5000)
      During crossing, the maximum trials for cut-plane

**Mute_Random_Pop_Ratio**: (0.15)
      For each generation, the probability of a structure to be mutated.

**Mute2_Search_Region_Radius**: (3.0)
      In mutation2 method, the size of the exchanged region (Angstrom)

**Turn_Off_Random_Rotation_When_MuteMole2**: (False).
      In mutation2 method, after two regions are exchanged, if they are also rotated randomly

**Mute3_Swap_Mole_Ratio**: (0.4).
      In mutation3 method, the probability if a molecule is chosen for the region to be shifted

**Mute3_Max_Displace_Steps**: (5).
      In mutation3 method, the maximum steps a region will be shifted. The actual shift steps is below
          this number 

**Mute4_Search_Region_Radius**: (5.0).
      In mutation4 method, the size of the sphere region randomly selected (Angstrom)


- Setup class, for example:

      d_crossmut = cross_mutation( Mutation=True, MutationMethod=2, EveryNStep_Random=10, CrossingMethod=6, CrossRotation=False, MethodProbFile=None, FixCrossingMethod=True, FixMutationMetion=True, Mute2_Search_Region_Radius=3.5 )


### setup Adjust class
         
(explain of flags used in this class, value in the "()" is the default.)

**MaxAdjust**: (5000 steps). 
      Before printing out mixed structure for relaxation, the 
      program do quick relaxation to avoid molecule overlap. 
      Here it defines the maximum steps for the relaxation.
      It uses steepest descent, with force=2*Coeff*(r-r0)^3

**Repul_Force_Thrd**: (0.7 eV/A). 
      This is the force thredshold for the quick relaxation.

**Repulsion_Coeff**: (5.0 eV/A^4). 
      This is the repulsion coefficient.

**Repulsion_Coeff_Surface**: (4.0 eV/A^4). 
      This is the repulsion coefficient used to be away from 
      the defined surface.

**EPSILON**: (0.004). 
      scale factor for steep descent relaxation.

**MinAtom2Surface_Thrd**: (2.6 Angstrom). 
      Defines the mimimum dist from the molecule to the defined 
      surface.

**Max_Move_Length**: (0.1 Angstrom). 
      Max movement for single step during relaxation.

**MinMole_Thrd**: (3.0 Angstrom). 
      Mimimum mole-mole distance.

**MinAtom_Thrd**: (1.5 Angstrom). 
      Mimimum atom-atom distance (used for atom case).

**Mole2Mole_D**: (0.02 eV).
      D_e used in Morse potential

**Mole2Mole_A**: (1.0 1/Angstrom).
      alpha used in Morse potential

**Atom2Surf_D**: (0.02 eV).
      D_e used in Morse potential

**Atom2Surf_A**: (1.0 1/Angstrom).
      alpha used in Morse potential

**RepulsionOnly**: (True)
      only consider replusion between two molecules (i.e. no Morse potential considered)

- Setup class, for example:

      d_adjust_positions = adjust_positions(Repul_Force_Thrd=0.2, MinMole_Thrd=2.7,MinAtom2Plane_Thrd=0.2,RepulsionOnly=True)



### setup DFT-IO class

**Package**: specify 'qe', 'vasp', 'lammps' or 'pwmat'

- Setup class, for example:

      IOrelaxation = relaxation_io( Package, pwd )
         


         
### setup DFT-running class

(this is the same to the above section "setup system parameters")

**package_str**: which package used to do relaxation.

**nproc**: how many tasks will be lanuched at the same time.

**nproc_task**: how many procs will be used for one task. The total number of procs will be nproc\*nproc_task.

**para_cmd**: wrapper to launch MPI task (e.g. mpirun, aprun, ...).

**package_cmd_path**: *path* for the executable.

**max_run_time**: maximum wall time for one DFT relaxation

- Setup class, for example:
 
      MPrelaxation = relaxation_run( Package, nproc=Num_Process, nproc_task=Num_CPU_Process, para_cmd=para_cmd, para_cmd_n=para_cmd_n, package_cmd_path=None, package_exec=exec_cmd, max_run_time=MaxRunTime )



         
### setup GA class
         
(explain of flags used in this class, value in the "()" is the default.)

**Process**: (mandatory input, only used for method "ga_cycle()").

    = 0 -> initial step (normally generate random molecules

    = 1 -> GA crossing/mutation and generate new structure for relax

    = 2 -> Based on the relaxed structure, 

**UseInputCoord**: (False).
    For initial step, if use the input coord or generate random molecules

**Child_Pop_Ratio**: (0.9).
    When the size of population is N, it will generate N*Child_Pop_Ratio 
      for child population
    
**Energy_Selection_Thrd**: (0.01 eV)
    If external relaxation obtained energy difference for two structures 
      are within this range, these two structures are believed to be the
      same structures.

**AddingRandom**: (True)
    In some cases (particularly at the beginning), once some structures 
      are removed owing to their close energies, random structures are 
      added

**FixMole_DFT**: (True)
    Add '0 0 0' (or 'F F F') for the fixed molecules (defined in d_mole) 
      when outputing coordinates

**NProc**: (1)
    The adjusting position is parallel, here, specifying how many processes
      are running at the same time (for best case, NProc should be the 
      same to the structures needs adjusting).

**ParentSelectionScaleFactor**: (2.0)
    Used for natural selection.

- Setup class, for example:

       ga = ga_search(  struct_energy, d_shape, d_crossmut, d_adjust_positions, d_molecule=d_molecules, UseInputCoord=False, NProc=cores_adjust, FixMole_DFT=True, Energy_Selection_Thrd=0.005 )


### Example of run_main.py

         # =========  setup system parameters =========
         structure_energy_file = 'structure-energy.dat'   # specify the file name to record structure and energy
         Num_Process = 9   # how many relaxations are launched at one time
         cores_adjust = 9  # number of processes for GA adjusting (usually is the number of populations)
         para_cmd = 'srun -n 2 -N 1 '   # MPI launch wrapper 
         Package = 'pwmat'        # package string
         exec_cmd = 'PWmat   '    # executable
         MaxGenerations = 1000    # maximum generation allowed
         MaxRunTime = 25000   # how long (second) can a DFT relaxation run (will kill job if it exceeds this time)
         files_dump = ['REPORT','RELAXSTEPS']   # DFT output files you want to archive (saved in dump folder)
         SetupInput = ['etot.input','O.SG15.NCPP.PBE.UPF','H.SG15.NCPP.PBE.UPF','ONCV.PWM.Cu.UPF','IN.SOLVENT']


         # =========  setup system parameters =========
         
         
         
         # ==============  setup restart  =============
         Restart = False
         # ==============  setup restart  =============
         
         
         
         # ==============  setup shape =============
         d_shape = shape('cylinder', OriginCoord=np.array([4.4, 7.60, 6.]), Radius=7.2, MaxZ=8.0, MinZ=6.3, WithSubstrate=True, SubstrateShapeStr='surface', SubstrateAtomsSpecies=['Cu'])
         # ==============  setup shape =============
         
         # ==============  setup Molecule =============
         NWater = 20
         InputMole = [[x,x*2+NWater,x*2+NWater+1] for x in range(0,NWater)]
         InputMoleName =  ['O']*NWater + ['H']*NWater*2
         InputMoleCoord = [np.array([[0.0,0.0,0.0],[-0.7563,0.5867,0.0],[0.7563,0.5867,0.0]])]*NWater   # unfixed nine molecules H2O
         d_molecules = input_molecules(ForceInputMole=InputMole, ForceInputMoleName=InputMoleName, ForceInputMoleCoord=InputMoleCoord )
         # ==============  setup Molecule =============

        
         # ==============  setup cross/mutation =============
         d_crossmut = cross_mutation( Mutation=True, MutationMethod=2, EveryNStep_Random=10, CrossingMethod=6, CrossRotation=False, MethodProbFile=None, FixCrossingMethod=True, FixMutationMetion=True, Mute2_Search_Region_Radius=3.5 )
         # ==============  setup cross/mutation =============
         

         # ==============  setup adjust =============
         d_adjust_positions = adjust_positions()
         # ==============  setup adjust =============
         

         # ==============  setup DFT IO =============
         IOrelaxation = relaxation_io( Package, pwd )
         # ==============  setup DFT IO =============
         

         # ==============  setup DFT relaxation =============
         MPrelaxation = relaxation_run( Package, nproc=Num_Process, para_cmd=para_cmd, package_cmd_path='./', package_exec=exec_cmd, max_run_time=MaxRunTime )
         # ==============  setup DFT relaxation =============
         

         # ==============  setup ga =============
         #ga = ga_search(  struct_energy, d_shape, d_crossmut, d_adjust_positions, d_molecule=d_molecules, UseInputCoord=False, NProc=cores_adjust, FixMole_DFT=True, Energy_Selection_Thrd=0.005 )
         ga = ga_search(  struct_energy, d_shape, d_crossmut, d_adjust_positions, d_atom=d_atoms, UseInputCoord=False, NProc=cores_adjust, FixMole_DFT=True, Energy_Selection_Thrd=0.005 )
         # ==============  setup ga =============

         # ===========  setup system details ==========


## run GA

execute the command ` python run_main.py >> log `

## restart

If a restart is needed, in run_main.py, the Restart variable should be set to True.
          Then, execute the command "python run_main.py >> log". The script should check the generation index by itself.


## TODO:

1. When running parallel relaxation for one generation, needs alternative routines when the resource 
            is not enough and all the relaxations are not carried out simultaneously

2. Implement grand canonical, e.g. useing free energy diff instead of total energy

3. Reorganize run_main to put all the parameters in another file



