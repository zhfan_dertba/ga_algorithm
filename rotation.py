
# this version can read and output more friendly than the previous version

'''
db = rotation( Coord, rot_axis, rot_angle )
db.apply_rotation_along_v()
NewCoord = db.rotated

# Coord is the coordinates with Cartesian
# rot_axis is a vector, not necessary to be normalized
# rot_angle is a number (rad) from 0 to 2*pi
# rotation will not change the order of the atoms
'''

import sys
import numpy as np
from math import radians, cos, sin, sqrt


class rotation:

    def __init__( self, Coord,  rot_axis, rot_angle, rx=0.0, ry=0.0, rz=0.0, RotationCenter=None ):
        self.coord = Coord
        self.rx_matrix = lambda rx: np.array( [[ 1.0,   0.0,    0.0  ],
                                               [0.0, cos(rx),-sin(rx)],
                                               [0.0, sin(rx), cos(rx)]] )
        self.ry_matrix = lambda ry: np.array( [[cos(ry),0.0, -sin(ry)],
                                               [  0.0  ,1.0,   0.0   ],
                                               [sin(ry),0.0,  cos(ry)]] )
        self.rz_matrix = lambda rz: np.array( [[cos(rz),-sin(rz), 0.0],
                                               [sin(rz), cos(rz), 0.0],
                                               [ 0.0,    0.0,    1.0 ]] )
        self.rx_m = lambda a, b: np.array( [[ 1.0,0.0,0.0 ],
                                            [0.0, a, -1.*b],
                                            [0.0, b,    a ]] )
        self.ry_m = lambda a, b: np.array( [[a ,0.0, -1.*b],
                                            [  0.0 ,1.0,0.0 ],
                                            [b ,0.0,  a]] )
        self.rz_m = lambda a, b: np.array( [[a ,-1.*b, 0.0],
                                            [b, a, 0.0],
                                            [ 0.0, 0.0,1.0 ]] )
        if RotationCenter is not None:
            self.origin_com = RotationCenter
        else:
            self.origin_com = _compute_center( self.coord )
        self.rot_axis = rot_axis
        self.rot_angle = rot_angle
        self.rotated = None
        self.angle_x = rx
        self.angle_y = ry
        self.angle_z = rz

    def apply_rotation_along_cartesian( self ):
        # x' = R_x*R_y*R_z * x
        return np.add( np.transpose( np.dot( rx_matrix(self.angle_x), np.dot( ry_matrix(self.angle_x), np.dot( rz_matrix(self.angle_x), np.transpose(np.subtract(self.coord,self.origin_com)) ) ) ) ) , self.origin_com )

    def apply_rotation_along_v( self ):
        a = self.rot_axis[0]; b = self.rot_axis[1]; c = self.rot_axis[2]
        cart  = np.transpose(np.subtract(self.coord, self.origin_com))
        cart  = _rotation( self.ry_m(sqrt(b**2+c**2)/sqrt(a**2+b**2+c**2),a/sqrt(a**2+b**2+c**2)), _rotation( self.rx_m(c/sqrt(b**2+c**2),b/sqrt(b**2+c**2)), cart ))
        cart1 = _rotation( self.rz_matrix(self.rot_angle), cart )
        cart1 = _rotation( self.rx_m(c/sqrt(b**2+c**2),-1.*b/sqrt(b**2+c**2)), _rotation(self.ry_m(sqrt(b**2+c**2)/sqrt(a**2+b**2+c**2),-1.*(a)/sqrt(a**2+b**2+c**2)),cart1) )
        cart1 = np.add( np.transpose(cart1), self.origin_com )
#       self.rotated = _reproduce_frac( cart1, self.lattice_para )
        self.rotated = cart1
        return 
   

def _rotation( r, v ):
    return  np.dot( r, v )

def _read_coord( filename ):
    c = np.loadtxt( filename, dtype=str )
    lattice = np.asfarray( c[0,1:], dtype=float )
    atoms   = c[1:,0]
    coord   = np.asfarray( c[1:,1:],dtype=float )
    return lattice, atoms, coord

def _compute_coord( frac_coord, lattice ):
    frac_coord[:,0] *= lattice[0] 
    frac_coord[:,1] *= lattice[1]
    frac_coord[:,2] *= lattice[2]
    return frac_coord

def _compute_center( cart_coord ):
#   print np.sum( cart_coord, axis=0 )/np.shape(cart_coord)[0]
    return np.sum( cart_coord, axis=0 )/np.shape(cart_coord)[0]

def _reproduce_frac( coord, lattice ):
    coord[:,0] *= 1.0/lattice[0]
    coord[:,1] *= 1.0/lattice[1]
    coord[:,2] *= 1.0/lattice[2]
    return coord


#
#
