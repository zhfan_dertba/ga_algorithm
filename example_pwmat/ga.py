#!/usr/bin/python2

import sys, os, shutil
import numpy as np
import multiprocessing as mp
from molecules_dev import *
from rotation import *
from copy import deepcopy

try:
    from numba import njit
    numba_import = True
except ImportError:
    print 'Recommend installing numba to accelerate'
    numba_import = False


# =============================================================================
# TO DO: (TODO file)
# =============================================================================
 

# setup random seed to reproduce results
#np.random.seed(1)

class substrate_shape:

    def __init__(self):
        self.SubstrateShapeName    = None
        self.Surf_High             = None
        self.Surf_Low              = None
        self.SubInitInnerRad       = None


class shape_open:

    def __init__(self, InitSampleDist2Center=None, MaxZ=None, MinZ=None, d_SubShape=None, SampleMinDist=None):
        self.Shape                 = 'open'
        self.InitSampleDist2Center = InitSampleDist2Center
        self.MaxZ                  = MaxZ
        self.MinZ                  = MinZ
        self.d_SubShape            = d_SubShape
        if SampleMinDist is None: 
            self.SampleMinDist     = 2.3
        else:
            self.SampleMinDist     = SampleMinDist


        if self.MaxZ is not None or self.MinZ is not None:
            print 'require z perpendicular to x-y plane'

        if self.d_SubShape is not None:
            if self.d_SubShape.SubstrateShapeName == 'surface':
                if self.MaxZ is not None:
                    self.MaxZ      = min(MaxZ,self.d_SubShape.Surf_Low)
                else:
                    self.MaxZ      = self.d_SubShape.Surf_Low
                if self.MinZ is not None:
                    self.MinZ      = max(MinZ,self.d_SubShape.Surf_High)
                else:
                    self.MinZ      = self.d_SubShape.Surf_High

    def gen_init_rand_points(self, NumPoint, Lat):
        PointCoord = np.zeros((NumPoint,3)); PointCoord_frac = np.zeros((NumPoint,3))
        PointCoord_frac[:,0] = np.random.random_sample(NumPoint)
        PointCoord_frac[:,1] = np.random.random_sample(NumPoint)
        PointCoord_frac[:,2] = np.random.random_sample(NumPoint)
        if self.InitSampleDist2Center is not None:
            OriginCoord = np.dot(np.array([0.5,0.5,0.5]),Lat) - np.ones(3)*3.0**0.5*self.InitSampleDist2Center
            x_vec = np.array([2.0*self.InitSampleDist2Center,0.0,0.0])
            y_vec = np.array([0.0,2.0*self.InitSampleDist2Center,0.0])
            z_vec = np.array([0.0,0.0,2.0*self.InitSampleDist2Center])
            PointCoord = np.dot( PointCoord_frac, np.array(x_vec,y_vec,z_vec) ) + OriginCoord
        else:
            PointCoord = np.dot( PointCoord_frac, Lat )
        if self.MaxZ is not None or self.MinZ is not None:
            PointCoord[:,2] = PointCoord_frac[:,2]*(self.MaxZ- self.MinZ) + self.MinZ
        return np.array(PointCoord)

    def gen_init_constrain(self):
        ConstrainPlane = []; ConstrainPolar = []
        if self.MinZ is not None:
            ConstrainPlane.append([[0.0,0.0,1.0,z_min-0.5],1])
        if self.MaxZ is not None:
            ConstrainPlane.append([[0.0,0.0,1.0,z_max+0.5],-1])
        return ConstrainPlane, ConstrainPolar


# TO DO: add SubShape to this class
class shape_ortho:

    def __init__(self, OriginCoord, A_vec, B_vec, C_vec, d_SubShape=None, SampleMinDist=None):
        self.Shape                 = 'ortho'
        self.OriginCoord           = OriginCoord
        self.A_vec                 = np.array(A_vec)
        self.B_vec                 = np.array(B_vec)
        self.C_vec                 = np.array(C_vec)
        self.d_SubShape            = d_SubShape
        if SampleMinDist is None: 
            self.SampleMinDist     = 2.3
        else:
            self.SampleMinDist     = SampleMinDist

    def gen_init_rand_points(self,NumPoint):
        PointCoord = np.zeros((NumPoint,3))
        # frac coord sampling
        PointCoord[:,0] = np.random.random_sample(NumPoint)
        PointCoord[:,1] = np.random.random_sample(NumPoint)
        PointCoord[:,2] = np.random.random_sample(NumPoint)
        # transform to cart
        PointCoord = np.dot( PointCoord, np.array([self.A_vec,self.B_vec,self.C_vec]) ) + self.OriginCoord
        return np.array(PointCoord)

    def gen_init_constrain(self):
        ConstrainPlane = []; ConstrainPolar = []       
        v1 = np.cross( self.A_vec, self.B_vec )
        ConstrainPlane.append([[v1[0],v1[1],v1[2],v1[0]*self.OriginCoord[0]+v1[1]*self.OriginCoord[1]+v1[2]*self.OriginCoord[2]],1])
        ConstrainPlane.append([[v1[0],v1[1],v1[2],v1[0]*(self.OriginCoord[0]+self.C_vec)+v1[1]*(self.OriginCoord[1]+self.C_vec)+v1[2]*(self.OriginCoord[2]+self.C_vec)],-1])
        v2 = np.cross( self.B_vec, self.C_vec )
        ConstrainPlane.append([[v2[0],v2[1],v2[2],v2[0]*self.OriginCoord[0]+v2[1]*self.OriginCoord[1]+v2[2]*self.OriginCoord[2]],1])
        ConstrainPlane.append([[v2[0],v2[1],v2[2],v2[0]*(self.OriginCoord[0]+self.A_vec)+v2[1]*(self.OriginCoord[1]+self.A_vec)+v2[2]*(self.OriginCoord[2]+self.A_vec)],-1])
        v3 = np.cross( self.C_vec, self.A_vec )
        ConstrainPlane.append([[v3[0],v3[1],v3[2],v3[0]*self.OriginCoord[0]+v3[1]*self.OriginCoord[1]+v3[2]*self.OriginCoord[2]],1])
        ConstrainPlane.append([[v3[0],v3[1],v3[2],v3[0]*(self.OriginCoord[0]+self.B_vec)+v3[1]*(self.OriginCoord[1]+self.B_vec)+v3[2]*(self.OriginCoord[2]+self.B_vec)],-1])
        return ConstrainPlane, ConstrainPolar


class shape_sphere:

    def __init__(self, OriginCoord, InitOuterRad, InitInnerRad=None, Z_min=None, Z_max=None, d_SubShape=None, SampleMinDist=None, MaxRandNum=10000):
        self.Shape                 = 'sphere'
        self.OriginCoord           = OriginCoord
        self.InitOuterRad          = InitOuterRad
        self.d_SubShape            = d_SubShape
        self.InitInnerRad          = InitInnerRad
        self.Z_min                 = Z_min
        self.Z_max                 = Z_max
        self.MaxRandNum            = MaxRandNum
        if SampleMinDist is None: 
            self.SampleMinDist     = 2.3
        else:
            self.SampleMinDist     = SampleMinDist

        if self.d_SubShape is not None:
            if self.d_SubShape.SubstrateShapeName == 'sphere':
                if self.InitInnerRad is not None:
                    self.InitInnerRad  = max(InitInnerRad,self.d_SubShape.SubInitInnerRad)
                else:
                    self.InitInnerRad  = self.d_SubShape.SubInitInnerRad
            if self.d_SubShape.SubstrateShapeName == 'surface':
                if self.Z_max is not None:
                    self.Z_max     = min(self.Z_max,self.d_SubShape.Surf_Low)
                else:
                    self.Z_max     = self.d_SubShape.Surf_Low
                if self.Z_min is not None:
                    self.Z_min     = max(self.Z_min,self.d_SubShape.Surf_High)
                else:
                    self.Z_min     = self.d_SubShape.Surf_High

    def gen_init_rand_points(self,NumPoint):
        PointCoord = []; count = 0
        while count < NumPoint:
            coord = self.gen_init_rand_1point(PointCoord, count)
            PointCoord.append( coord )
            count = count + 1
        return np.array(PointCoord)

    def gen_init_rand_1point(self,PointCoord,count):
        distAllMin = 0.0; count2 = 0
        while distAllMin < self.SampleMinDist:
            count2 = count2 + 1
            if count2 > self.MaxRandNum:
                print_and_exit('Initial generate is wrong, either constrained space is too small or SampleMinDist is too large')
            r, theta, phi = gen_rand_r_theta_phi(self.InitOuterRad)
            coord = spherical_to_cart(theta,phi,r) + self.OriginCoord
            if self.InitInnerRad is not None:
                if r < self.InitInnerRad: continue
            if self.Z_min is not None:
                if coord[2] < self.Z_min: continue
            if self.Z_max is not None:
                if coord[2] > self.Z_max: continue
            distAllMin = comp_all_dist_cluster( coord, PointCoord, count )
#           print '==>', count2, distAllMin
#           print coord
#           print PointCoord
        return coord

    def gen_addi_rand_points(self,AddiNumPoint,PointCoord):
        NewPointCoord = []
        NumPointBefAdd = len(PointCoord)
        count = 0
        while count < AddiNumPoint:
            coord = self.gen_init_rand_1point(PointCoord, count+NumPointBefAdd)
            PointCoord.append( coord )
            NewPointCoord.append( coord )
            count = count + 1
        return NewPointCoord

    def gen_init_constrain(self):
        ConstrainPlane = []; ConstrainPolar = []       
        if self.InitInnerRad is None:
            ConstrainPolar.append([self.OriginCoord.tolist(),None,self.InitOuterRad])
        else:
            ConstrainPolar.append([self.OriginCoord.tolist(),self.InitInnerRad,self.InitOuterRad])
        if self.Z_min is not None:
            ConstrainPlane.append([[0.0,0.0,1.0,self.Z_min],1])
        if self.Z_max is not None:
            ConstrainPlane.append([[0.0,0.0,1.0,self.Z_max],-1])
        return ConstrainPlane, ConstrainPolar


class shape_cylinder:

    def __init__(self, OriginCoord, Radius, Z_min, Z_max, d_SubShape=None, SampleMinDist=None, MaxRandNum=10000):
        self.Shape                 = 'cylinder'
        self.OriginCoord           = OriginCoord
        self.d_SubShape            = d_SubShape
        self.Radius                = Radius
        self.Z_min                 = Z_min
        self.Z_max                 = Z_max
        self.MaxRandNum            = MaxRandNum
        if SampleMinDist is None: 
            self.SampleMinDist     = 2.3
        else:
            self.SampleMinDist     = SampleMinDist

    def gen_init_rand_points(self,NumPoint):
        PointCoord = []; count = 0
        while count < NumPoint:
            coord = self.gen_init_rand_1point(PointCoord, count)
            PointCoord.append( coord )
            count = count + 1
        return np.array(PointCoord)

    def gen_init_rand_1point(self,PointCoord,count):
        distAllMin = 0.0; count2 = 0
        while distAllMin < self.SampleMinDist:
            count2 = count2 + 1
            if count2 > self.MaxRandNum:
                print_and_exit('Initial generate is wrong, either constrained space is too small or SampleMinDist is too large')
            r, theta, phi = gen_rand_r_theta_phi(self.Radius)
            z = np.random.random()*(self.Z_max-self.Z_min) + self.Z_min
            coord = polar_to_cart(z,phi,r) + np.array([self.OriginCoord[0], self.OriginCoord[1], 0.0])
            distAllMin = comp_all_dist_cluster( coord, PointCoord, count )
        return coord

    def gen_addi_rand_points(self,AddiNumPoint,PointCoord):
        NewPointCoord = []
        NumPointBefAdd = len(PointCoord)
        count = 0
        while count < AddiNumPoint:
            coord = self.gen_init_rand_1point(PointCoord, count+NumPointBefAdd)
            PointCoord.append( coord )
            NewPointCoord.append( coord )
            count = count + 1
        return NewPointCoord

    def gen_init_constrain(self):
        ConstrainPlane = []; ConstrainPolar = []       
        ConstrainPolar.append([self.OriginCoord.tolist(),None,self.Radius])
        ConstrainPlane.append([[0.0,0.0,1.0,self.Z_min],1])
        ConstrainPlane.append([[0.0,0.0,1.0,self.Z_max],-1])
        return ConstrainPlane, ConstrainPolar



class shape:

    def __init__(self, ShapeStr, InitSampleDist2Center=None, MaxZ=None, MinZ=None, OriginCoord=None, A_vec=None, B_vec=None, C_vec=None, InitOuterRad=None, InitInnerRad=None, Radius=None, SampleMinDist=None, WithSubstrate=False, SubstrateShapeStr=None, SubstrateAtomsSpecies=None, ConstrainPlane=None, ConstrainPolar=None, IncludeInitConstrain=True, MixSubstrate=False, MoveToCenter=False):
        ''' This class defines description of the system. Also it defines some 
                basic parameters for adjusting.

            args (bracket is its default value):

              WithSubstrate: (False)
                    Specify if there will be substrate

              If this is *surface* case, such surface must be at x/y plane:

                  MixSubstrate: (False).
                        When crossing two parent structure, if the coordinates of the 
                        DFT calculated substrates are also averaged.

                  IniSampleDistToCenter: (4.5 Angstrom).
                        If generate mole random at first step, the radius defines the
                        range of the generated moles.

              If this is *cluster* case, such surface should be close to a sphere-like type:

                  BoxSize and InitOuterRadius are used for initial mole generation:

                  BoxSize: generated moles are in an cubic box with box size defined 
                        here. This is used when there is no substrate

                  InitOuterRadius: generated moles are in a sphere. This is used with 
                        or without substrate.

              if including the constrain plane/sphere: 
                  
                  ConstrainPlane: [[a,b,c,d],+/-1].
                        a, b, c, d defines the function of the plane
                        +1: the molecules should be in the side of norm-direction of 
                              the plane
                        -1: the molecules should be at the opposite direction

                  ConstrainPolar: [[x,y,z],r1, r2].
                        [x,y,z] is the center of the sphere
                        r1: inner sphere radius (r1 could be None); 
                        r2: outer sphere radius
                        molecules should be between r1 and r2
'''

        self.Shape                   = ShapeStr
        self.WithSubstrate           = WithSubstrate
        self.SubstrateAtomsSpecies   = SubstrateAtomsSpecies
        self.ConstrainPlane          = ConstrainPlane
        self.ConstrainPolar          = ConstrainPolar
        self.IncludeInitConstrain    = IncludeInitConstrain
        self.MixSubstrate            = MixSubstrate
        self.MoveToCenter            = MoveToCenter
        self.SampleMinDist           = SampleMinDist
        self.d_substrate_shape       = None
        self.d_InputShape            = None

        if (self.SubstrateAtomsSpecies is None or SubstrateShapeStr is None) and self.WithSubstrate: 
            print_and_exit( 'Substrate atom species/substrate shape has to be input since there is a substrate' )

        if WithSubstrate:

            TestFileWithSubstrate = '0'
            name, lat, coord_cart, fix = _read_coord_single_file( TestFileWithSubstrate )
            Natom = len(name)  # Natom is 0 if no coords read in (e.g. for the case without substrate)
            if Natom==0: print_and_exit('Must set substrate coords if WithSubstrate is set to True (check file 0)')
            SubstrateCoord = np.array([ coord_cart[x,:] for x in range(0,Natom) if name[x] in self.SubstrateAtomsSpecies ])
            SubstrateName = [ x for x in name if x in self.SubstrateAtomsSpecies ]
            NumSubstrateAtoms = len(SubstrateName)

            self.d_substrate_shape = substrate_shape()
            self.d_substrate_shape.SubstrateShapeName = SubstrateShapeStr.lower()

            # for cluster with substrate case, estimate size of the substrate
            if SubstrateShapeStr == 'sphere':
                self.d_substrate_shape.SubstrateShapeName = 'sphere'
                r0 = _find_mass_center( SubstrateCoord )
                Sub_InitInnerRad = max([ np.linalg.norm(r0-SubstrateCoord[i,:]) for i in range(0,NumSubstrateAtoms) ])
                self.d_substrate_shape.SubInitInnerRad = Sub_InitInnerRad
            # for surface, estimate high and low of the substrate
            if SubstrateShapeStr == 'surface':
                self.d_substrate_shape.SubstrateShapeName = 'surface'
                Sub_Surf_High = np.amax( SubstrateCoord[:,2] )
                Sub_Surf_Low  = np.amin( SubstrateCoord[:,2] )
                if Sub_Surf_Low < Sub_Surf_High: Sub_Surf_Low = Sub_Surf_Low + lat[2,2]
                self.d_substrate_shape.Surf_High = Sub_Surf_High
                self.d_substrate_shape.Surf_Low  = Sub_Surf_Low

        if self.Shape == 'open':
            self.d_InputShape = shape_open(InitSampleDist2Center=InitSampleDist2Center, MaxZ=MaxZ, MinZ=MinZ, d_SubShape=self.d_substrate_shape, SampleMinDist=self.SampleMinDist)

        if self.Shape == 'ortho':
            if A_vec is None or B_vec is None or C_vec is None or OriginCoord is None:
                print_and_exit( 'missing input for shape==ortho' )
            self.d_InputShape = shape_ortho(OriginCoord, A_vec, B_vec, C_vec, d_SubShape=self.d_substrate_shape, SampleMinDist=self.SampleMinDist)

        if self.Shape == 'sphere':
            if InitOuterRad is None:
                print_and_exit( 'missing input InitOuterRad for shape==sphere' )
            self.d_InputShape = shape_sphere(OriginCoord, InitOuterRad, InitInnerRad=InitInnerRad, Z_min=MinZ, Z_max=MaxZ, d_SubShape=self.d_substrate_shape, SampleMinDist=self.SampleMinDist)

        if self.Shape == 'cylinder':
            if MinZ is None or MaxZ is None:
                print_and_exit( 'missing input MinZ or MaxZ for shape==cylinder' )
            self.d_InputShape = shape_cylinder(OriginCoord, Radius, MinZ, MaxZ, d_SubShape=self.d_substrate_shape, SampleMinDist=self.SampleMinDist)

        if self.d_InputShape is None:
            print_and_exit( 'need to specify the shape of molecules in shape class' )
           
        if self.IncludeInitConstrain:
            InputShapeConstrain = self.d_InputShape.gen_init_constrain()
            if self.ConstrainPlane is None:
                self.ConstrainPlane = InputShapeConstrain[0]
            else:
                self.ConstrainPlane = self.ConstrainPlane + InputShapeConstrain[0]
            if self.ConstrainPolar is None:
                self.ConstrainPolar = InputShapeConstrain[1]
            else:
                self.ConstrainPolar = self.ConstrainPolar + InputShapeConstrain[1]


class input_molecules:

    def __init__( self, ForceInputMole=None, ForceInputMoleName=None, ForceInputMoleCoord=None, FixMolecule=None ):
        self.ForceInputMole = ForceInputMole
        self.ForceInputMoleCoord = ForceInputMoleCoord
        self.ForceInputMoleName = ForceInputMoleName
        self.FixMolecule = FixMolecule
        self.NMolecule = len(self.ForceInputMole)
        self.Natom = len(self.ForceInputMoleName)
        self.AtomicMass = {'C':12,'O':16,'N':14,'H':1,'Li':3,'S':32}




class input_atoms:

    def __init__( self, ForceInputMole=None, ForceInputMoleName=None, ForceInputMoleCoord=None, FixMolecule=None):
        self.ForceInputMole = ForceInputMole
        self.ForceInputMoleCoord = ForceInputMoleCoord
        self.ForceInputMoleName = ForceInputMoleName
        if FixMolecule is None:
            FixMolecule = []
        self.FixMolecule = FixMolecule
        self.NMolecule = len(self.ForceInputMole)
        self.Natom = len(self.ForceInputMoleName)
        self.ForceInputMole = range(0,self.Natom)
        self.AtomicMass = {'C':12,'O':16,'N':14,'H':1,'Li':3,'S':32}




class cross_mutation:

    def __init__( self,Mutation=False,MutationMethod=None,EveryNStep_Random=20,CrossingMethod=None,CrossRotation=True,MethodProbFile=None,FixCrossingMethod=True,FixMutationMetion=True, **kargs ):
        ''' This class defines the cross method (including mutation) used during GA mixing

            args (bracket is its default value):

                Mutation: (False).
                      If turn on mutation. Turn on mutation is important for 
                      quick serarching.

                MutationMethod: (None).
                      Select the mutation method used (2, 3, 4).

                EveryNStep_Random: (20)
                      After EveryNStep_Random generations, a random structure is 
                        added to replace the last one in the population

                CrossingMethod: (none)
                      Select the cross method used (1, 2, 3, 4, 5, 6).

                MethodProbFile: (None).
                      This file is to record the crossing/mutation methods 
                      on the fly.      
              
                FixCrossingMethod: (True).

                FixMutationMethod: (True).

                MaxSamplePlane: (5000)

                Mute_Random_Pop_Ratio: (0.15)

                Mute2_Search_Region_Radius: (3.0)

                Turn_Off_Random_Rotation_When_MuteMole2: (False).

                Mute3_Swap_Mole_Ratio: (0.4).

                Mute3_Max_Displace_Steps: (5).

                Mute4_Search_Region_Radius: (5.0).

                Mute2Atom_Swap_Search_Radius: (2.5 Angstrom)

                Mute4Atom_Shift_Thrd: (0.8 Angstrom)
        '''

        self.Mutation = Mutation
        if MethodProbFile is not None:
            self.MethodProb = True
            self.MethodProbFile = MethodProbFile
            CrossingMethod_f,CrossingProb_f,MutationMethod_f,MutationProb_f = read_method_prob_file(MethodProbFile)
            self.CrossingMethod = CrossingMethod_f
            self.CrossingProb = CrossingProb_f
            self.MutationMethod = MutationMethod_f
            self.MutationProb = MutationProb_f
            self.FixCrossingMethod = FixCrossingMethod
            if FixCrossingMethod:
                self.CrossingMethod = CrossingMethod 
            self.FixMutationMethod = FixMutationMethod
            if FixMutationMethod:
                self.CrossingMethod = MutationMethod
        else:
            self.MethodProb = False
            self.MethodProbFile = None
            self.CrossingMethod = CrossingMethod
            self.MutationMethod = MutationMethod
            self.CrossingProb = None
            self.MutationProb = None
        self.CrossRotation = CrossRotation
        self.EveryNStep_Random = EveryNStep_Random

        args_dic = {  'MaxSamplePlane'                          : 5000,
                      'Mute_Random_Pop_Ratio'                   : 0.15,
                      'Mute2_Search_Region_Radius'              : 3.0,
                      'Turn_Off_Random_Rotation_When_MuteMole2' : False,
                      'Mute3_Swap_Mole_Ratio'                   : 0.4,
                      'Mute3_Max_Displace_Steps'                : 5,
                      'Mute4_Search_Region_Radius'              : 5.0,
                      'Mute1HowManyAtom'                        : 3,
                      'Mute1RandShiftMax'                       : 5,   
                      'Mute2Atom_Swap_Search_Radius'            : 2.0,
                      'Mute3Atom_Shift_Thrd'                    : 1.0   }

        for iarg in args_dic.keys():
            if kargs.has_key( iarg ):  args_dic[iarg] = kargs[iarg]

        self.MaxSamplePlane = args_dic['MaxSamplePlane']
        self.Mute_Random_Pop_Ratio = args_dic['Mute_Random_Pop_Ratio']
        self.Mute2_Search_Region_Radius = args_dic['Mute2_Search_Region_Radius']
        self.Turn_Off_Random_Rotation_When_MuteMole2 = args_dic['Turn_Off_Random_Rotation_When_MuteMole2']
        self.Mute3_Swap_Mole_Ratio = args_dic['Mute3_Swap_Mole_Ratio']
        self.Mute3_Max_Displace_Steps = args_dic['Mute3_Max_Displace_Steps']
        self.Mute4_Search_Region_Radius = args_dic['Mute4_Search_Region_Radius']
        self.Mute1HowManyAtom = args_dic['Mute1HowManyAtom']
        self.Mute1RandShiftMax = args_dic['Mute1RandShiftMax']
        self.Mute2Atom_Swap_Search_Radius = args_dic['Mute2Atom_Swap_Search_Radius']
        self.Mute3Atom_Shift_Thrd = args_dic['Mute3Atom_Shift_Thrd']


    def update_method_prob( self ):
        with open( self.MethodProbFile, 'w' ) as f:
            f.write('%d    %8.3f  %8.3f  %8.3f  %8.3f  %8.3f  %8.3f\n'%(self.CrossingMethod,self.CrossingProb[0],self.CrossingProb[1],self.CrossingProb[2],self.CrossingProb[3],self.CrossingProb[4],self.CrossingProb[5]))
            f.write('%d    %8.3f  %8.3f  %8.3f\n'%(self.MutationMethod,self.MutationProb[0],self.MutationProb[1],self.MutationProb[2]))
        return


    def mutation(self,Coord,Lat,d_molecule,d_shape,MinMole_Thrd):
        if self.MutationMethod == 2: OutputCoord =  self.muteMole2( Coord,Lat,d_molecule.ForceInputMole,d_molecule.FixMolecule)
        if self.MutationMethod == 3: OutputCoord =  self.muteMole3( Coord,Lat,d_molecule.ForceInputMole,d_molecule.FixMolecule,MinMole_Thrd)
        if self.MutationMethod == 4: OutputCoord =  self.muteMole4( Coord,Lat,d_molecule.ForceInputMole,d_molecule.FixMolecule)
        return OutputCoord
    
    def mutation_atom(self,Coord,Lat,d_molecule,d_shape,MinAtom_Thrd):
        if self.MutationMethod == 1: OutputCoord =  self.muteAtom1( Coord,Lat,d_molecule.ForceInputMole,d_molecule.FixMolecule,MinAtom_Thrd)
        if self.MutationMethod == 2: OutputCoord =  self.muteAtom2( Coord,Lat,d_molecule.ForceInputMole,d_molecule.FixMolecule)
        if self.MutationMethod == 3: OutputCoord =  self.muteAtom3( Coord,Lat,d_molecule.ForceInputMole,d_molecule.FixMolecule)
        return OutputCoord

    # mole mutation method 2: exchange two regions of the absorbers; may even rotate them
    def muteMole2(self,Coord,Lat,ForceInputMole,FixMolecule):
        # Coord = [Name, Coord]
        CoordNew = np.copy( Coord[1] )
        if FixMolecule is None: FixMolecule = []
        MolesRuleList2 = [ imole for i,imole in enumerate(ForceInputMole) if i not in FixMolecule ]
        NMole = len(MolesRuleList2)
        Mole1Mole2Dist = 0.0
        icount = 0
        while Mole1Mole2Dist <= 2.0 * self.Mute2_Search_Region_Radius:
            MoleIndex1 = np.random.randint( NMole )
            MoleIndex2 = np.random.randint( NMole )
            MassMole1 = find_selected_mass_center( Coord[1],MolesRuleList2[MoleIndex1] )
            MassMole2 = find_selected_mass_center( Coord[1],MolesRuleList2[MoleIndex2] )
            Mole1Mole2Dist = np.linalg.norm( MassMole1-MassMole2 )
            icount = icount + 1
            if icount >= 1000:
                print_and_exit( 'Too large MUTE2_SEARCH_REGION_RADIUS')
        Mole1Search = []; Mole2Search = []
        for i, imole in enumerate(MolesRuleList2):
            Massimole = find_selected_mass_center( Coord[1],imole )
            if np.linalg.norm( Massimole-MassMole1 ) <= self.Mute2_Search_Region_Radius:
                Mole1Search.append(imole)
            if np.linalg.norm( Massimole-MassMole2 ) <= self.Mute2_Search_Region_Radius:
                Mole2Search.append(imole)
        print '  - region 1, num of molecules ', len(Mole1Search)
        print '  - region 2, num of molecules ', len(Mole2Search)
        Mole1Search_Flat = sum(Mole1Search,[]); Mole2Search_Flat = sum(Mole2Search,[])
        MassMole1Search = find_selected_mass_center( Coord[1], Mole1Search_Flat )
        MassMole2Search = find_selected_mass_center( Coord[1], Mole2Search_Flat )
        if self.Turn_Off_Random_Rotation_When_MuteMole2 == False:
            Angle = np.random.random_sample((2,))*2.0*np.pi
            NormalVector1 = rand_norm_vector()
            NormalVector2 = rand_norm_vector()
            rotate_selected_atoms( CoordNew, Mole1Search, NormalVector1, Angle[0], MassMole1Search)
            rotate_selected_atoms( CoordNew, Mole2Search, NormalVector2, Angle[1], MassMole2Search)
        CoordNew[Mole1Search_Flat] = CoordNew[Mole1Search_Flat] - MassMole1Search + MassMole2Search
        CoordNew[Mole2Search_Flat] = CoordNew[Mole2Search_Flat] - MassMole2Search + MassMole1Search
        return [Coord[0],CoordNew]
    
    # mole mutation method 3: find a spherical region, shift it in random direction for random steps
    def muteMole3(self,Coord,Lat,ForceInputMole,FixMolecule,d_shape,MinMole_Thrd):
        # Coord = [Name, Coord]
        CoordNew = np.copy( Coord[1] )
        if d_shape.Shape == 'sphere' and d_shape.WithSubstrate:
            InnerRadius = d_shape.d_substrate_shape.SubInitInnerRad
            OuterRadius = d_shape.d_InputShape.InitOuterRad
        MinMole_Thrd = MinMole_Thrd
        if FixMolecule is None: FixMolecule = []
        MolesRuleList2 = [ imole for i,imole in enumerate(ForceInputMole) if i not in FixMolecule ]
        NMole = len(MolesRuleList2)
        MoleList = np.random.randint( NMole, size=int(self.Mute3_Swap_Mole_Ratio *NMole) )
        for imole in MoleList:
            NumMutedSteps = np.random.randint(self.Mute3_Max_Displace_Steps)
            theta = np.random.random_sample(NumMutedSteps)*np.pi
            phi = np.random.random_sample(NumMutedSteps)*2.0*np.pi
            if (d_shape.Shape == 'open' or d_shape.Shape == 'ortho') and d_shape.WithSubstrate: 
                theta = np.pi*0.5*np.ones(NumMutedSteps)  # this is for uppper plane
                phi = np.random.random_sample(NumMutedSteps)*2.0*np.pi
            if d_shape.Shape == 'sphere' and d_shape.WithSubstrate:
                theta = np.random.random_sample(NumMutedSteps)*np.pi*MinMole_Thrd/InnerRadius*0.5
                phi = np.random.random_sample(NumMutedSteps)*2.0*np.pi*MinMole_Thrd/InnerRadius*0.5
                r = (np.random.random_sample(NumMutedSteps)*(OuterRadius-InnerRadius))*0.5
            for i in range(0,NumMutedSteps):
                Shift = np.array([ np.sin(theta[i])*np.cos(phi[i]), np.sin(theta[i])*np.sin(phi[i]), np.cos(theta[i]) ])*MinMole_Thrd*0.4
                if d_shape.Shape == 'sphere' and d_shape.WithSubstrate:
                    theta0, phi0, r0 = cart_to_spherical( CoordNew[MolesRuleList2[imole][0],:] )
                    Shift = spherical_to_cart( theta0+theta[i], phi0+phi[i], r0+r[i] )
                CoordNew[MolesRuleList2[imole]] = CoordNew[MolesRuleList2[imole]] + Shift
        return [Coord[0],CoordNew]
    
    # muteMole4: select a random region and rotated it randomly
    def muteMole4(self,Coord,Lat,ForceInputMole,FixMolecule):
        # Coord = [Name, Coord]
        CoordNew = np.copy( Coord[1] )
        if FixMolecule is None: FixMolecule = []
        MolesRuleList2 = [ imole for i,imole in enumerate(ForceInputMole) if i not in FixMolecule ]
        NMole = len(MolesRuleList2)
        MoleIndex1 = np.random.randint( NMole )
        MassMole1 = find_selected_mass_center( Coord[1],MolesRuleList2[MoleIndex1] )
        Mole1Search = []
        for i, imole in enumerate(MolesRuleList2):
            Massimole = find_selected_mass_center( Coord[1],imole )
            if np.linalg.norm( Massimole-MassMole1 ) <= self.Mute4_Search_Region_Radius:
                Mole1Search.append(imole)
        print '  - search region, num of molecules ', len(Mole1Search)
        Angle = np.random.random_sample()*2.0*np.pi
        NormalVector1 = rand_norm_vector()
        rotate_selected_atoms( CoordNew, Mole1Search, NormalVector1, Angle, MassMole1)
        return [Coord[0],CoordNew]

    # mutation 1: move "a" random atom in random direction for random steps (each step is a min bond length)
    def muteAtom1(self, coord, Lat ,ForceInputMole,FixMolecule,MinBondLength_Thrd):
        CoordNew = np.copy( coord[1] )
        if FixMolecule is None: FixMolecule = []
        NotInFixAtoms = [ x for x in ForceInputMole if x not in FixMolecule ]
        N_NotInFixAtoms = len(NotInFixAtoms)
        for it in range(0,self.Mute1HowManyAtom):
            AtomIndex = NotInFixAtoms[np.random.randint(N_NotInFixAtoms)]
            if AtomIndex not in FixMolecule:
                for i in range(0,np.random.randint(self.Mute1RandShiftMax)):
                    NormalVector = rand_norm_vector()
                    CoordNew[AtomIndex,:] = CoordNew[AtomIndex,:] + NormalVector*MinBondLength_Thrd
        return [coord[0],CoordNew]
    
    # mutation 2: exchange two groups of atoms
    def muteAtom2(self, coord,Lat, ForceInputMole,FixMolecule ):
        CoordNew = np.copy( coord[1] )
        if FixMolecule is None: FixMolecule = []
        NotInFixAtoms = [ x for x in ForceInputMole if x not in FixMolecule ]
        N_NotInFixAtoms = len(NotInFixAtoms)
        DistTwoCenter = 0.0; icount = 0
        while (DistTwoCenter <= 1.7*self.Mute2Atom_Swap_Search_Radius):
            AtomIndex1 = NotInFixAtoms[np.random.randint(N_NotInFixAtoms)]
            AtomIndex2 = NotInFixAtoms[np.random.randint(N_NotInFixAtoms)]
            DistTwoCenter = np.linalg.norm( coord[1][AtomIndex1,:] - coord[1][AtomIndex2,:] )
            icount = icount + 1
            if icount > 500: 
                print 'Too large Mute2Atom_Swap_Search_Radius? exceeding trial limit, exit'; sys.exit()
        ShiftVector = coord[1][AtomIndex1,:] - coord[1][AtomIndex2,:]
        GroupAtom1 = []; GroupAtom2 = []
        for i in NotInFixAtoms:
            if np.linalg.norm( coord[1][i,:]-coord[1][AtomIndex1,:] )<=self.Mute2Atom_Swap_Search_Radius: GroupAtom1.append(i)
            if np.linalg.norm( coord[1][i,:]-coord[1][AtomIndex2,:] )<=self.Mute2Atom_Swap_Search_Radius and (i not in GroupAtom1): GroupAtom2.append(i)
        CoordNew[GroupAtom1] = CoordNew[GroupAtom1] - ShiftVector
        CoordNew[GroupAtom2] = CoordNew[GroupAtom2] + ShiftVector
        return [coord[0],CoordNew]
    
    # mutation 3: all the atoms are randomly shifted with a random length (<= MinAtom_thrd)
    def muteAtom3(self, coord, Lat, ForceInputMole,FixMolecule ):
        CoordNew = np.copy( coord[1] )
        if FixMolecule is None: FixMolecule = []
        NotInFixAtoms = [ x for x in ForceInputMole if x not in FixMolecule ]
        for i in NotInFixAtoms:
            NormalVector = rand_norm_vector()
            ShiftLength = np.random.random_sample()*self.Mute3Atom_Shift_Thrd
            CoordNew[1][i,:] = CoordNew[i,:] + NormalVector*ShiftLength
        return [coord[0],CoordNew]
    
    # apply the above mutation pattern multiple times (mutate multiple molecules)
    def multi_mole_muteMole1(self,MuteTime,Coord,Lat,SurfacePlane_Z,UpSurfacePlane_Z,ForceInputMole,FixMolecule=None):
        Coord4Mute = [Coord[0],np.copy(Coord[1])]
        for imute in range(0,MuteTime):
            Coord4Mute =  self.muteMole1(Coord4Mute,Lat,SurfacePlane_Z,UpSurfacePlane_Z,ForceInputMole,FixMolecule=None)
        return Coord4Mute

    def crossing_mole( self, CrossingMethod, coord1, coord2, SpeciesCount, MaxSamplePlane, Rotate=True, ForceInputMole=None, FixMolecule=None ):
        if CrossingMethod == 1: CombineName, CombineCoord = self.cross1( coord1, coord2, SpeciesCount, MaxSamplePlane, Rotate=Rotate, ForceInputMole=ForceInputMole, FixMolecule=FixMolecule )
        if CrossingMethod == 2: CombineName, CombineCoord = self.cross2( coord1, coord2, SpeciesCount, MaxSamplePlane,                ForceInputMole=ForceInputMole, FixMolecule=FixMolecule )
        if CrossingMethod == 3: CombineName, CombineCoord = self.cross3( coord1, coord2, SpeciesCount, MaxSamplePlane,                ForceInputMole=ForceInputMole, FixMolecule=FixMolecule )
        if CrossingMethod == 4: CombineName, CombineCoord = self.cross4( coord1, coord2, SpeciesCount, MaxSamplePlane,                ForceInputMole=ForceInputMole, FixMolecule=FixMolecule )
        if CrossingMethod == 5: CombineName, CombineCoord = self.cross5( coord1, coord2, SpeciesCount, MaxSamplePlane,                ForceInputMole=ForceInputMole, FixMolecule=FixMolecule )
        if CrossingMethod == 6: CombineName, CombineCoord = self.cross6( coord1, coord2, SpeciesCount, MaxSamplePlane,                ForceInputMole=ForceInputMole, FixMolecule=FixMolecule )
        return CombineName, CombineCoord

    def crossing_atom( self, CrossingMethod, coord1, coord2, SpeciesCount, MaxSamplePlane, Rotate=True, ForceInputMole=None, FixMolecule=None ):
        if CrossingMethod == 1: CombineName, CombineCoord = self.cross_atom1( coord1, coord2, SpeciesCount, MaxSamplePlane, Rotate=Rotate, ForceInputMole=ForceInputMole, FixMolecule=FixMolecule )
        if CrossingMethod == 2: CombineName, CombineCoord = self.cross_atom2( coord1, coord2, SpeciesCount, MaxSamplePlane,                ForceInputMole=ForceInputMole, FixMolecule=FixMolecule )
        if CrossingMethod == 3: CombineName, CombineCoord = self.cross_atom3( coord1, coord2, SpeciesCount, MaxSamplePlane,                ForceInputMole=ForceInputMole, FixMolecule=FixMolecule )
        if CrossingMethod == 4: CombineName, CombineCoord = self.cross_atom4( coord1, coord2, SpeciesCount, MaxSamplePlane,                ForceInputMole=ForceInputMole, FixMolecule=FixMolecule )
        if CrossingMethod == 5: CombineName, CombineCoord = self.cross_atom5( coord1, coord2, SpeciesCount, MaxSamplePlane,                ForceInputMole=ForceInputMole, FixMolecule=FixMolecule )
        if CrossingMethod == 6: CombineName, CombineCoord = self.cross_atom6( coord1, coord2, SpeciesCount, MaxSamplePlane,                ForceInputMole=ForceInputMole, FixMolecule=FixMolecule )
        return CombineName, CombineCoord
     
    # slice and split
    @staticmethod
    def cross1( coord1, coord2, SpeciesCount, MaxSamplePlane, Rotate=True, ForceInputMole=None, FixMolecule=None ):
        natom = np.shape( coord1[1] )[0]
        mass1 = _find_mass_center( coord1[1] )
        mass2 = _find_mass_center( coord2[1] )
        CombineMass = (mass1 + mass2)/2.0
        for iplane in range(0,MaxSamplePlane):
            g1up = []; g2dn = []
            NormalVector = rand_norm_vector()
            Atomg1upNotFixed = [x for x in sum(ForceInputMole[0],[]) if x not in sum(FixMolecule[0],[])]
            Atomg2dnNotFixed = [x for x in sum(ForceInputMole[1],[]) if x not in sum(FixMolecule[1],[])]
            mass1 = find_selected_mass_center( coord1[1],Atomg1upNotFixed )
            mass2 = find_selected_mass_center( coord2[1],Atomg2dnNotFixed )
            g1up = [ x for x in ForceInputMole[0] if _point2plane( find_selected_mass_center( coord1[1],x ) - mass1, NormalVector ) >= 0.0 and sorted(x) not in sorted_all(FixMolecule[0]) ]
            g2dn = [ x for x in ForceInputMole[1] if _point2plane( find_selected_mass_center( coord2[1],x ) - mass2, NormalVector ) <  0.0 and sorted(x) not in sorted_all(FixMolecule[1]) ]
            FixMolecule_for_count = FixMolecule[0][:]
            if len(sum(FixMolecule[0],[]))==1 and sum(FixMolecule[0],[])[0] is None: FixMolecule_for_count = []
            if _count_mole_if_correct( g1up, g2dn, coord1[0], coord2[0], ForceInputMole[0], FixMolecule_for_count ):
                if Rotate:
                    coord2Rotate = rotate_selected_atoms( coord2[1], g2dn, NormalVector, np.pi, CombineMass )
                    coord2Copy = coord2Rotate
                else:
                    coord2Copy = np.copy(coord2[1])
                FixMolecule_Flat = [ sum(FixMolecule[0],[]), sum(FixMolecule[1],[]) ]
                UnFixMolecule_Flat = [ [x for x in range(0,natom) if x not in FixMolecule_Flat[0]], [y for y in range(0,natom) if y not in FixMolecule_Flat[1]] ]
                coord1[1][UnFixMolecule_Flat[0]] = coord1[1][UnFixMolecule_Flat[0]]-mass1+CombineMass
                coord2Copy[UnFixMolecule_Flat[1]] = coord2Copy[UnFixMolecule_Flat[1]]-mass2+CombineMass
                CombineCoord, CombineName = combine_moles_g1up_g2dn( g1up, g2dn, coord1[1], coord2Copy, coord1[0],coord2[0], ForceInputMole, FixMolecule_for_count )
                return CombineName, CombineCoord 
        print 'Reach MAXSAMPLEPLANE (cross1), exit'; sys.exit()
    
    # slice and split: rotate to z axis, using z components of molecules
    @staticmethod
    def cross2( coord1, coord2, SpeciesCount, MaxSamplePlane, ForceInputMole=None, FixMolecule=None ):
        natom = np.shape( coord1[1] )[0]
        mass1 = _find_mass_center( coord1[1] )
        mass2 = _find_mass_center( coord2[1] )
        CombineMass = (mass1 + mass2)/2.0
        CombineCoord = np.copy( coord1[1] )
        MolesNotFixed1 = [x for x in ForceInputMole[0] if sorted(x) not in sorted_all(FixMolecule[0])]
        MolesNotFixed2 = [x for x in ForceInputMole[1] if sorted(x) not in sorted_all(FixMolecule[1])]
        NMole = len(MolesNotFixed1)
        if len(MolesNotFixed1) != len(MolesNotFixed2):
            print 'Fatal error, not equal number of molecules'
            sys.exit()
        for ipick in range(0,MaxSamplePlane):
            [PickMole1,PickMole2] = np.random.randint(0,high=NMole,size=2)
            coord1Rotate = pick_mole_and_rotate(MolesNotFixed1, PickMole1, coord1[1], mass1, 2)
            coord2Rotate = pick_mole_and_rotate(MolesNotFixed2, PickMole2, coord2[1], mass2, 2)
            z_component_sort1 = sorted(zip( [find_selected_mass_center( coord1Rotate, x )[2] for x in MolesNotFixed1], MolesNotFixed1 ))
            z_component_sort2 = sorted(zip( [find_selected_mass_center( coord2Rotate, x )[2] for x in MolesNotFixed2], MolesNotFixed2 ))
            PickMole = np.random.randint(1,high=NMole-1)
            g1up = [ x[1] for x in z_component_sort1[:PickMole] ]
            g2dn = [ x[1] for x in z_component_sort2[PickMole:] ]
            FixMolecule_for_count = FixMolecule[0][:]
            if len(sum(FixMolecule[0],[]))==1 and sum(FixMolecule[0],[])[0] is None: FixMolecule_for_count = []
            if _count_mole_if_correct( g1up, g2dn, coord1[0], coord2[0], ForceInputMole[0], FixMolecule_for_count ):
                coord2Copy = np.copy(coord2Rotate)
                FixMolecule_Flat = [ sum(FixMolecule[0],[]), sum(FixMolecule[1],[]) ]
                UnFixMolecule_Flat = [ [x for x in range(0,natom) if x not in FixMolecule_Flat[0]], [y for y in range(0,natom) if y not in FixMolecule_Flat[1]] ]
                coord1Rotate[UnFixMolecule_Flat[0]] = coord1Rotate[UnFixMolecule_Flat[0]]-mass1+CombineMass
                coord2Copy[UnFixMolecule_Flat[1]] = coord2Copy[UnFixMolecule_Flat[1]]-mass2+CombineMass
                CombineCoord, CombineName = combine_moles_g1up_g2dn( g1up, g2dn, coord1Rotate, coord2Copy, coord1[0],coord2[0], ForceInputMole, FixMolecule_for_count )
                return CombineName, CombineCoord 
        print 'Reach MAXSAMPLEPLANE (cross2), exit'; sys.exit()
     
    # pick one atom, rotate this atom  to z axis, but use shell shape (but the shell center is not necessarily to conincide with mass center)
    @staticmethod
    def cross3( coord1, coord2, SpeciesCount, MaxSamplePlane, ForceInputMole=None, FixMolecule=None ):
        natom = np.shape( coord1[1] )[0]
        mass1 = _find_mass_center( coord1[1] )
        mass2 = _find_mass_center( coord2[1] )
        CombineMass = (mass1 + mass2)/2.0
        CombineCoord = np.copy( coord1[1] )
        MolesNotFixed1 = [x for x in ForceInputMole[0] if sorted(x) not in sorted_all(FixMolecule[0])]
        MolesNotFixed2 = [x for x in ForceInputMole[1] if sorted(x) not in sorted_all(FixMolecule[1])]
        NMole = len(MolesNotFixed1)
        if len(MolesNotFixed1) != len(MolesNotFixed2):
            print 'Fatal error, not equal number of molecules'
            sys.exit()
        for ipick in range(0,MaxSamplePlane):
            [PickMole1,PickMole2] = np.random.randint(0,high=NMole,size=2)
            coord1Rotate = pick_mole_and_rotate(MolesNotFixed1, PickMole1, coord1[1], mass1, 2)
            coord2Rotate = pick_mole_and_rotate(MolesNotFixed2, PickMole2, coord2[1], mass2, 2)
            z_component_sort1 = sorted(zip( [np.linalg.norm(find_selected_mass_center( coord1Rotate, x )) for x in MolesNotFixed1], MolesNotFixed1 ))
            z_component_sort2 = sorted(zip( [np.linalg.norm(find_selected_mass_center( coord2Rotate, x )) for x in MolesNotFixed2], MolesNotFixed2 ))
            PickMole = np.random.randint(1,high=NMole-1)
            g1up = [ x[1] for x in z_component_sort1[:PickMole] ]
            g2dn = [ x[1] for x in z_component_sort2[PickMole:] ]
            FixMolecule_for_count = FixMolecule[0][:]
            if len(sum(FixMolecule[0],[]))==1 and sum(FixMolecule[0],[])[0] is None: FixMolecule_for_count = []
            if _count_mole_if_correct( g1up, g2dn, coord1[0], coord2[0], ForceInputMole[0], FixMolecule_for_count ):
                coord2Copy = np.copy(coord2Rotate)
                FixMolecule_Flat = [ sum(FixMolecule[0],[]), sum(FixMolecule[1],[]) ]
                UnFixMolecule_Flat = [ [x for x in range(0,natom) if x not in FixMolecule_Flat[0]], [y for y in range(0,natom) if y not in FixMolecule_Flat[1]] ]
                coord1Rotate[UnFixMolecule_Flat[0]] = coord1Rotate[UnFixMolecule_Flat[0]]-mass1+CombineMass
                coord2Copy[UnFixMolecule_Flat[1]] = coord2Copy[UnFixMolecule_Flat[1]]-mass2+CombineMass
                CombineCoord, CombineName = combine_moles_g1up_g2dn( g1up, g2dn, coord1Rotate, coord2Copy, coord1[0],coord2[0], ForceInputMole, FixMolecule_for_count )
                return CombineName, CombineCoord 
        print 'Reach MAXSAMPLEPLANE (cross3), exit'; sys.exit()
     
    # shell shape mixing (shell shape center is in the same location of mass center)
    @staticmethod
    def cross4( coord1, coord2, SpeciesCount, MaxSamplePlane, ForceInputMole=None, FixMolecule=None ):
        natom = np.shape( coord1[1] )[0]
        mass1 = _find_mass_center( coord1[1] )
        mass2 = _find_mass_center( coord2[1] )
        CombineMass = (mass1 + mass2)/2.0
        CombineCoord = np.copy( coord1[1] )
        MolesNotFixed1 = [x for x in ForceInputMole[0] if sorted(x) not in sorted_all(FixMolecule[0])]
        MolesNotFixed2 = [x for x in ForceInputMole[1] if sorted(x) not in sorted_all(FixMolecule[1])]
        NMole = len(MolesNotFixed1)
        if len(MolesNotFixed1) != len(MolesNotFixed2):
            print 'Fatal error, not equal number of molecules'
            sys.exit()
        coord1Rotate = np.copy( coord1[1] )
        coord2Rotate = np.copy( coord2[1] )
        for ipick in range(0,MaxSamplePlane):
            z_component_sort1 = sorted(zip( [np.linalg.norm(find_selected_mass_center( coord1Rotate, x )-mass1) for x in MolesNotFixed1], MolesNotFixed1 ))
            z_component_sort2 = sorted(zip( [np.linalg.norm(find_selected_mass_center( coord2Rotate, x )-mass2) for x in MolesNotFixed2], MolesNotFixed2 ))
            PickMole = np.random.randint(1,high=NMole-1)
            g1up = [ x[1] for x in z_component_sort1[:PickMole] ]
            g2dn = [ x[1] for x in z_component_sort2[PickMole:] ]
            FixMolecule_for_count = FixMolecule[0][:]
            if len(sum(FixMolecule[0],[]))==1 and sum(FixMolecule[0],[])[0] is None: FixMolecule_for_count = []
            if _count_mole_if_correct( g1up, g2dn, coord1[0], coord2[0], ForceInputMole[0], FixMolecule_for_count ):
                coord2Copy = np.copy(coord2Rotate)
                FixMolecule_Flat = [ sum(FixMolecule[0],[]), sum(FixMolecule[1],[]) ]
                UnFixMolecule_Flat = [ [x for x in range(0,natom) if x not in FixMolecule_Flat[0]], [y for y in range(0,natom) if y not in FixMolecule_Flat[1]] ]
                coord1Rotate[UnFixMolecule_Flat[0]] = coord1Rotate[UnFixMolecule_Flat[0]]-mass1+CombineMass
                coord2Copy[UnFixMolecule_Flat[1]] = coord2Copy[UnFixMolecule_Flat[1]]-mass2+CombineMass
                CombineCoord, CombineName = combine_moles_g1up_g2dn( g1up, g2dn, coord1Rotate, coord2Copy, coord1[0],coord2[0], ForceInputMole, FixMolecule_for_count )
                return CombineName, CombineCoord 
        print 'Reach MAXSAMPLEPLANE (cross4), exit'; sys.exit()
    
    # slice and split: rotate to z axis, using z components of molecules, but *two* picking points
    @staticmethod
    def cross5( coord1, coord2, SpeciesCount, MaxSamplePlane, ForceInputMole=None, FixMolecule=None ):
        natom = np.shape( coord1[1] )[0]
        mass1 = _find_mass_center( coord1[1] )
        mass2 = _find_mass_center( coord2[1] )
        CombineMass = (mass1 + mass2)/2.0
        CombineCoord = np.copy( coord1[1] )
        MolesNotFixed1 = [x for x in ForceInputMole[0] if sorted(x) not in sorted_all(FixMolecule[0])]
        MolesNotFixed2 = [x for x in ForceInputMole[1] if sorted(x) not in sorted_all(FixMolecule[1])]
        NMole = len(MolesNotFixed1)
        if len(MolesNotFixed1) != len(MolesNotFixed2):
            print 'Fatal error, not equal number of molecules'
            sys.exit()
        for ipick in range(0,MaxSamplePlane):
            [PickMole1,PickMole2] = np.random.randint(0,high=NMole,size=2)
            coord1Rotate = pick_mole_and_rotate(MolesNotFixed1, PickMole1, coord1[1], mass1, 2)
            coord2Rotate = pick_mole_and_rotate(MolesNotFixed2, PickMole2, coord2[1], mass2, 2)
            z_component_sort1 = sorted(zip( [find_selected_mass_center( coord1Rotate, x )[2] for x in MolesNotFixed1], MolesNotFixed1 ))
            z_component_sort2 = sorted(zip( [find_selected_mass_center( coord2Rotate, x )[2] for x in MolesNotFixed2], MolesNotFixed2 ))
            PickMole = [0, 0]
            while (PickMole[0]==PickMole[1]):
                PickMole = np.random.randint(1,high=NMole-1,size=2)
            g1up = [ x[1] for x in z_component_sort1[:PickMole[0]]+z_component_sort1[PickMole[1]:] ]
            g2dn = [ x[1] for x in z_component_sort2[PickMole[0]:PickMole[1]] ]
            FixMolecule_for_count = FixMolecule[0][:]
            if len(sum(FixMolecule[0],[]))==1 and sum(FixMolecule[0],[])[0] is None: FixMolecule_for_count = []
            if _count_mole_if_correct( g1up, g2dn, coord1[0], coord2[0], ForceInputMole[0], FixMolecule_for_count ):
                coord2Copy = np.copy(coord2Rotate)
                FixMolecule_Flat = [ sum(FixMolecule[0],[]), sum(FixMolecule[1],[]) ]
                UnFixMolecule_Flat = [ [x for x in range(0,natom) if x not in FixMolecule_Flat[0]], [y for y in range(0,natom) if y not in FixMolecule_Flat[1]] ]
                coord1Rotate[UnFixMolecule_Flat[0]] = coord1Rotate[UnFixMolecule_Flat[0]]-mass1+CombineMass
                coord2Copy[UnFixMolecule_Flat[1]] = coord2Copy[UnFixMolecule_Flat[1]]-mass2+CombineMass
                CombineCoord, CombineName = combine_moles_g1up_g2dn( g1up, g2dn, coord1Rotate, coord2Copy, coord1[0],coord2[0], ForceInputMole, FixMolecule_for_count )
                return CombineName, CombineCoord 
        print 'Reach MAXSAMPLEPLANE (cross5), exit'; sys.exit()
    
    # slice and split: pick one mole, rotate to x axis, using x components of molecules
    # appropriate for surface
    @staticmethod
    def cross6( coord1, coord2, SpeciesCount, MaxSamplePlane, ForceInputMole=None, FixMolecule=None ):
        natom = np.shape( coord1[1] )[0]
        mass1 = _find_mass_center( coord1[1] )
        mass2 = _find_mass_center( coord2[1] )
        CombineMass = (mass1 + mass2)/2.0
        CombineCoord = np.copy( coord1[1] )
        MolesNotFixed1 = [xmole for x,xmole in enumerate(ForceInputMole[0]) if x not in FixMolecule[0]]
        MolesNotFixed2 = [xmole for x,xmole in enumerate(ForceInputMole[1]) if x not in FixMolecule[1]]
        NMole = len(MolesNotFixed1)
        if len(MolesNotFixed1) != len(MolesNotFixed2):
            print 'Fatal error, not equal number of molecules'
            sys.exit()
        for ipick in range(0,MaxSamplePlane):
            [PickMole1,PickMole2] = np.random.randint(0,high=NMole,size=2)
            coord1Rotate = pick_mole_and_rotate(MolesNotFixed1, PickMole1, coord1[1], mass1, 0)
            coord2Rotate = pick_mole_and_rotate(MolesNotFixed2, PickMole2, coord2[1], mass2, 0)
            z_component_sort1 = sorted(zip( [find_selected_mass_center( coord1Rotate, x )[0] for x in MolesNotFixed1], MolesNotFixed1 ))
            z_component_sort2 = sorted(zip( [find_selected_mass_center( coord2Rotate, x )[0] for x in MolesNotFixed2], MolesNotFixed2 ))
            PickMole = np.random.randint(1,high=NMole-1)
            g1up = [ x[1] for x in z_component_sort1[:PickMole] ]
            g2dn = [ x[1] for x in z_component_sort2[PickMole:] ]
            if len(FixMolecule[0]) != 0:
                FixMolecule_for_count = [ ForceInputMole[0][i] for i in FixMolecule[0] ]
            else:
                FixMolecule_for_count = [ ]
            if _count_mole_if_correct( g1up, g2dn, coord1[0], coord2[0], ForceInputMole[0], FixMolecule_for_count ):
                coord2Copy = np.copy(coord2Rotate)
                UnFixMolecule_Flat = [ sum(MolesNotFixed1,[]), sum(MolesNotFixed2,[]) ]
                coord1Rotate[UnFixMolecule_Flat[0]] = coord1Rotate[UnFixMolecule_Flat[0]]-mass1+CombineMass
                coord2Copy[UnFixMolecule_Flat[1]] = coord2Copy[UnFixMolecule_Flat[1]]-mass2+CombineMass
                CombineCoord, CombineName = combine_moles_g1up_g2dn( g1up, g2dn, coord1Rotate, coord2Copy, coord1[0], coord2[0], ForceInputMole, FixMolecule_for_count )
                return CombineName, CombineCoord 
        print 'Reach MAXSAMPLEPLANE (cross6), exit'; sys.exit()
 
    # slice and split
    @staticmethod
    def cross_atom1( coord1, coord2, SpeciesCount, MaxSamplePlane, Rotate=True, ForceInputMole=None, FixMolecule=None ):
        natom = np.shape( coord1[1] )[0]
        mass1 = _find_mass_center( coord1[1] )
        mass2 = _find_mass_center( coord2[1] )
        CombineMass = (mass1 + mass2)/2.0
        for iplane in range(0,MaxSamplePlane):
            g1up = []; g2dn = []
            NormalVector = rand_norm_vector()
            Atomg1upNotFixed = [x for x in ForceInputMole[0] if x not in FixMolecule[0]]
            Atomg2dnNotFixed = [x for x in ForceInputMole[1] if x not in FixMolecule[1]]
            mass1 = find_selected_mass_center( coord1[1],Atomg1upNotFixed )
            mass2 = find_selected_mass_center( coord2[1],Atomg2dnNotFixed )
            g1up = [ x for x in ForceInputMole[0] if _point2plane( coord1[1][x,:] - mass1, NormalVector ) >= 0.0 and x not in FixMolecule[0] ]
            g2dn = [ x for x in ForceInputMole[1] if _point2plane( coord2[1][x,:] - mass2, NormalVector ) <  0.0 and x not in FixMolecule[1] ]
            FixMolecule_for_count = FixMolecule[0][:]
            if _count_atom_if_correct( g1up, g2dn, coord1[0], coord2[0], ForceInputMole[0], FixMolecule_for_count ):
                if Rotate:
                    coord2Rotate = rotate_selected_atoms_noflat( coord2[1], g2dn, NormalVector, np.pi, CombineMass )
                    coord2Copy = coord2Rotate
                else:
                    coord2Copy = np.copy(coord2[1])
                UnFixMolecule_Flat = [ [x for x in ForceInputMole[0] if x not in FixMolecule[0]], [y for y in ForceInputMole[1] if y not in FixMolecule[1]] ]
                coord1[1][UnFixMolecule_Flat[0]] = coord1[1][UnFixMolecule_Flat[0]]-mass1+CombineMass
                coord2Copy[UnFixMolecule_Flat[1]] = coord2Copy[UnFixMolecule_Flat[1]]-mass2+CombineMass
                CombineCoord, CombineName = combine_atoms_g1up_g2dn( g1up, g2dn, coord1[1], coord2Copy, coord1[0],coord2[0], ForceInputMole, FixMolecule_for_count )
                return CombineName, CombineCoord 
        print 'Reach MAXSAMPLEPLANE (cross1), exit'; sys.exit()
 
    # slice and split: rotate to z axis, using z components of molecules
    @staticmethod
    def cross_atom2( coord1, coord2, SpeciesCount, MaxSamplePlane, ForceInputMole=None, FixMolecule=None ):
        natom = np.shape( coord1[1] )[0]
        mass1 = _find_mass_center( coord1[1] )
        mass2 = _find_mass_center( coord2[1] )
        CombineMass = (mass1 + mass2)/2.0
        CombineCoord = np.copy( coord1[1] )
        AtomNotFixed1 = [x for x in ForceInputMole[0] if x not in FixMolecule[0]]
        AtomNotFixed2 = [x for x in ForceInputMole[1] if x not in FixMolecule[1]]
        NMole = len(AtomNotFixed1)
        for ipick in range(0,MaxSamplePlane):
            [PickMole1,PickMole2] = np.random.randint(0,high=NMole,size=2)
            coord1Rotate = pick_atom_and_rotate( AtomNotFixed1, PickMole1, coord1[1], mass1, 2 )
            coord2Rotate = pick_atom_and_rotate( AtomNotFixed2, PickMole2, coord2[1], mass2, 2 )
            z_component_sort1 = z_component_sort = sorted(zip( [ coord1Rotate[x,2] for x in AtomNotFixed1], AtomNotFixed1 ))
            z_component_sort2 = z_component_sort = sorted(zip( [ coord2Rotate[x,2] for x in AtomNotFixed2], AtomNotFixed2 ))
            PickMole = np.random.randint(1,high=NMole-1)
            g1up = [ x[1] for x in z_component_sort1[:PickMole] ]
            g2dn = [ x[1] for x in z_component_sort2[PickMole:] ]
            FixMolecule_for_count = FixMolecule[0][:]
            if _count_atom_if_correct( g1up, g2dn, coord1[0], coord2[0], ForceInputMole[0], FixMolecule_for_count ):
                coord2Copy = np.copy(coord2Rotate)
                UnFixMolecule_Flat = [ [x for x in ForceInputMole[0] if x not in FixMolecule[0]], [y for y in ForceInputMole[1] if y not in FixMolecule[1]] ]
                coord1Rotate[UnFixMolecule_Flat[0]] = coord1Rotate[UnFixMolecule_Flat[0]]-mass1+CombineMass
                coord2Copy[UnFixMolecule_Flat[1]] = coord2Copy[UnFixMolecule_Flat[1]]-mass2+CombineMass
                CombineCoord, CombineName = combine_atoms_g1up_g2dn( g1up, g2dn, coord1Rotate, coord2Copy, coord1[0],coord2[0], ForceInputMole, FixMolecule_for_count )
                return CombineName, CombineCoord 
        print 'Reach MAXSAMPLEPLANE (cross2), exit'; sys.exit()
     
    # pick one atom, rotate this atom  to z axis, but use shell shape (but the shell center is not necessarily to conincide with mass center)
    @staticmethod
    def cross_atom3( coord1, coord2, SpeciesCount, MaxSamplePlane, ForceInputMole=None, FixMolecule=None ):
        natom = np.shape( coord1[1] )[0]
        mass1 = _find_mass_center( coord1[1] )
        mass2 = _find_mass_center( coord2[1] )
        CombineMass = (mass1 + mass2)/2.0
        CombineCoord = np.copy( coord1[1] )
        AtomNotFixed1 = [x for x in ForceInputMole[0] if x not in FixMolecule[0]]
        AtomNotFixed2 = [x for x in ForceInputMole[1] if x not in FixMolecule[1]]
        NMole = len(AtomNotFixed1)
        for ipick in range(0,MaxSamplePlane):
            [PickMole1,PickMole2] = np.random.randint(0,high=NMole,size=2)
            coord1Rotate = pick_atom_and_rotate( AtomNotFixed1, PickMole1, coord1[1], mass1, 2 )
            coord2Rotate = pick_atom_and_rotate( AtomNotFixed2, PickMole2, coord2[1], mass2, 2 )
            z_component_sort1 = z_component_sort = sorted(zip( [ np.linalg.norm(coord1Rotate[x,:]) for x in AtomNotFixed1], AtomNotFixed1 ))
            z_component_sort2 = z_component_sort = sorted(zip( [ np.linalg.norm(coord2Rotate[x,:]) for x in AtomNotFixed2], AtomNotFixed2 ))
            PickMole = np.random.randint(1,high=NMole-1)
            g1up = [ x[1] for x in z_component_sort1[:PickMole] ]
            g2dn = [ x[1] for x in z_component_sort2[PickMole:] ]
            FixMolecule_for_count = FixMolecule[0][:]
            if _count_atom_if_correct( g1up, g2dn, coord1[0], coord2[0], ForceInputMole[0], FixMolecule_for_count ):
                coord2Copy = np.copy(coord2Rotate)
                UnFixMolecule_Flat = [ [x for x in ForceInputMole[0] if x not in FixMolecule[0]], [y for y in ForceInputMole[1] if y not in FixMolecule[1]] ]
                coord1Rotate[UnFixMolecule_Flat[0]] = coord1Rotate[UnFixMolecule_Flat[0]]-mass1+CombineMass
                coord2Copy[UnFixMolecule_Flat[1]] = coord2Copy[UnFixMolecule_Flat[1]]-mass2+CombineMass
                CombineCoord, CombineName = combine_atoms_g1up_g2dn( g1up, g2dn, coord1Rotate, coord2Copy, coord1[0],coord2[0], ForceInputMole, FixMolecule_for_count )
                return CombineName, CombineCoord 
        print 'Reach MAXSAMPLEPLANE (cross2), exit'; sys.exit()
 
    # shell shape mixing (shell shape center is in the same location of mass center)
    @staticmethod
    def cross_atom4( coord1, coord2, SpeciesCount, MaxSamplePlane, ForceInputMole=None, FixMolecule=None ):
        natom = np.shape( coord1[1] )[0]
        mass1 = _find_mass_center( coord1[1] )
        mass2 = _find_mass_center( coord2[1] )
        CombineMass = (mass1 + mass2)/2.0
        CombineCoord = np.copy( coord1[1] )
        AtomNotFixed1 = [x for x in ForceInputMole[0] if x not in FixMolecule[0]]
        AtomNotFixed2 = [x for x in ForceInputMole[1] if x not in FixMolecule[1]]
        NMole = len(AtomNotFixed1)
        coord1Rotate = np.copy(coord1[1])
        coord2Rotate = np.copy(coord2[1])
        for ipick in range(0,MaxSamplePlane):
            z_component_sort1 = z_component_sort = sorted(zip( [ np.linalg.norm(coord1Rotate[x,:]-mass1) for x in AtomNotFixed1], AtomNotFixed1 ))
            z_component_sort2 = z_component_sort = sorted(zip( [ np.linalg.norm(coord2Rotate[x,:]-mass2) for x in AtomNotFixed2], AtomNotFixed2 ))
            PickMole = np.random.randint(1,high=NMole-1)
            g1up = [ x[1] for x in z_component_sort1[:PickMole] ]
            g2dn = [ x[1] for x in z_component_sort2[PickMole:] ]
            FixMolecule_for_count = FixMolecule[0][:]
            if _count_atom_if_correct( g1up, g2dn, coord1[0], coord2[0], ForceInputMole[0], FixMolecule_for_count ):
                coord2Copy = np.copy(coord2Rotate)
                UnFixMolecule_Flat = [ [x for x in ForceInputMole[0] if x not in FixMolecule[0]], [y for y in ForceInputMole[1] if y not in FixMolecule[1]] ]
                coord1Rotate[UnFixMolecule_Flat[0]] = coord1Rotate[UnFixMolecule_Flat[0]]-mass1+CombineMass
                coord2Copy[UnFixMolecule_Flat[1]] = coord2Copy[UnFixMolecule_Flat[1]]-mass2+CombineMass
                CombineCoord, CombineName = combine_atoms_g1up_g2dn( g1up, g2dn, coord1Rotate, coord2Copy, coord1[0],coord2[0], ForceInputMole, FixMolecule_for_count )
                return CombineName, CombineCoord 
        print 'Reach MAXSAMPLEPLANE (cross2), exit'; sys.exit()
 
    # slice and split: rotate to z axis, using z components of molecules, but *two* picking points
    @staticmethod
    def cross_atom5( coord1, coord2, SpeciesCount, MaxSamplePlane, ForceInputMole=None, FixMolecule=None ):
        natom = np.shape( coord1[1] )[0]
        mass1 = _find_mass_center( coord1[1] )
        mass2 = _find_mass_center( coord2[1] )
        CombineMass = (mass1 + mass2)/2.0
        CombineCoord = np.copy( coord1[1] )
        AtomNotFixed1 = [x for x in ForceInputMole[0] if x not in FixMolecule[0]]
        AtomNotFixed2 = [x for x in ForceInputMole[1] if x not in FixMolecule[1]]
        NMole = len(AtomNotFixed1)
        for ipick in range(0,MaxSamplePlane):
            [PickMole1,PickMole2] = np.random.randint(0,high=NMole,size=2)
            coord1Rotate = pick_atom_and_rotate( AtomNotFixed1, PickMole1, coord1[1], mass1, 2 )
            coord2Rotate = pick_atom_and_rotate( AtomNotFixed2, PickMole2, coord2[1], mass2, 2 )
            z_component_sort1 = z_component_sort = sorted(zip( [ coord1Rotate[x,2] for x in AtomNotFixed1], AtomNotFixed1 ))
            z_component_sort2 = z_component_sort = sorted(zip( [ coord2Rotate[x,2] for x in AtomNotFixed2], AtomNotFixed2 ))
            PickMole = [0, 0]
            while (PickMole[0]==PickMole[1]):
                PickMole = np.random.randint(1,high=NMole-1,size=2)
            g1up = [ x[1] for x in z_component_sort1[:PickMole[0]]+z_component_sort1[PickMole[1]:] ]
            g2dn = [ x[1] for x in z_component_sort2[PickMole[0]:PickMole[1]] ]
            FixMolecule_for_count = FixMolecule[0][:]
            if _count_atom_if_correct( g1up, g2dn, coord1[0], coord2[0], ForceInputMole[0], FixMolecule_for_count ):
                coord2Copy = np.copy(coord2Rotate)
                UnFixMolecule_Flat = [ [x for x in ForceInputMole[0] if x not in FixMolecule[0]], [y for y in ForceInputMole[1] if y not in FixMolecule[1]] ]
                coord1Rotate[UnFixMolecule_Flat[0]] = coord1Rotate[UnFixMolecule_Flat[0]]-mass1+CombineMass
                coord2Copy[UnFixMolecule_Flat[1]] = coord2Copy[UnFixMolecule_Flat[1]]-mass2+CombineMass
                CombineCoord, CombineName = combine_atoms_g1up_g2dn( g1up, g2dn, coord1Rotate, coord2Copy, coord1[0],coord2[0], ForceInputMole, FixMolecule_for_count )
                return CombineName, CombineCoord 
        print 'Reach MAXSAMPLEPLANE (cross2), exit'; sys.exit()
 
    # slice and split: pick one mole, rotate to x axis, using x components of molecules
    # appropriate for surface
    @staticmethod
    def cross_atom6( coord1, coord2, SpeciesCount, MaxSamplePlane, ForceInputMole=None, FixMolecule=None ):
        natom = np.shape( coord1[1] )[0]
        mass1 = _find_mass_center( coord1[1] )
        mass2 = _find_mass_center( coord2[1] )
        CombineMass = (mass1 + mass2)/2.0
        CombineCoord = np.copy( coord1[1] )
        AtomNotFixed1 = [x for x in ForceInputMole[0] if x not in FixMolecule[0]]
        AtomNotFixed2 = [x for x in ForceInputMole[1] if x not in FixMolecule[1]]
        NMole = len(AtomNotFixed1)
        for ipick in range(0,MaxSamplePlane):
            [PickMole1,PickMole2] = np.random.randint(0,high=NMole,size=2)
            coord1Rotate = pick_atom_and_rotate( AtomNotFixed1, PickMole1, coord1[1], mass1, 0 )
            coord2Rotate = pick_atom_and_rotate( AtomNotFixed2, PickMole2, coord2[1], mass2, 0 )
            z_component_sort1 = z_component_sort = sorted(zip( [ coord1Rotate[x,0] for x in AtomNotFixed1], AtomNotFixed1 ))
            z_component_sort2 = z_component_sort = sorted(zip( [ coord2Rotate[x,0] for x in AtomNotFixed2], AtomNotFixed2 ))
            PickMole = np.random.randint(1,high=NMole-1)
            g1up = [ x[1] for x in z_component_sort1[:PickMole] ]
            g2dn = [ x[1] for x in z_component_sort2[PickMole:] ]
            FixMolecule_for_count = FixMolecule[0][:]
            if _count_atom_if_correct( g1up, g2dn, coord1[0], coord2[0], ForceInputMole[0], FixMolecule_for_count ):
                coord2Copy = np.copy(coord2Rotate)
                UnFixMolecule_Flat = [ [x for x in ForceInputMole[0] if x not in FixMolecule[0]], [y for y in ForceInputMole[1] if y not in FixMolecule[1]] ]
                coord1Rotate[UnFixMolecule_Flat[0]] = coord1Rotate[UnFixMolecule_Flat[0]]-mass1+CombineMass
                coord2Copy[UnFixMolecule_Flat[1]] = coord2Copy[UnFixMolecule_Flat[1]]-mass2+CombineMass
                CombineCoord, CombineName = combine_atoms_g1up_g2dn( g1up, g2dn, coord1Rotate, coord2Copy, coord1[0],coord2[0], ForceInputMole, FixMolecule_for_count )
                return CombineName, CombineCoord 
        print 'Reach MAXSAMPLEPLANE (cross2), exit'; sys.exit()


class structure_energy_list:

    '''This file record the energy and structure for each GA generation

       file-energy.dat file format:
         #############################################
         Generation_number  Num_structures
         filename_1    file1_energy
         filename_2    file2_energy
         filename_3    file3_energy
         ...
         #############################################
       (when process=0, the second column could be empty.)
    '''

    def __init__( self, StructureEnergyFilename ):
        self.StructureEnergyFilename = StructureEnergyFilename
        self.Generation = None
        self.NImage = None
        self.FileName = None
        self.FileEnergy = None

    def reset( self ):
        self.Generation = None
        self.NImage = None
        self.FileName = None
        self.FileEnergy = None

    def read_file( self ):
        Generation,NImage,FileName,FileEnergy = read_structure_energy_file(self.StructureEnergyFilename)
        self.Generation = Generation
        self.NImage = NImage
        self.FileName = FileName
        self.FileEnergy = FileEnergy

    def update_file( self, Generation,NImage,FileEnergy ):
        with open(self.StructureEnergyFilename, 'w') as f:
            f.write('%d   %d\n'%(self.Generation+1, self.NImage))
            for i in range(0,self.NImage):
                f.write('%s    %25.14e\n'%(str(i),FileEnergy[i]))

    def append_file( self, FileNameList, OutputFileEnergy, flag=None ):
        with open(self.StructureEnergyFilename, 'a+') as f:
            for i, ifile in enumerate(FileNameList):
                if flag is None:
                    f.write('%s    %25.14e\n'%(ifile,OutputFileEnergy[i]))
                else:
                    f.write('%s    %25.14e\n'%(ifile+'-'+flag,OutputFileEnergy[i]))






class adjust_positions:
    ''' this method approximate molecules as spheres, therefore, suitable for **small and round** molecules such as water 
                MaxAdjust: (5000 steps). 
                      Before printing out mixed structure for relaxation, the 
                      program do quick relaxation to avoid molecule overlap. 
                      Here it defines the maximum steps for the relaxation.
                      It uses steepest descent, with force=2*Coeff*(r-r0)^3

                Repul_Force_Thrd: (0.7 eV/A). 
                      This is the force thredshold for the quick relaxation.

                Repulsion_Coeff: (5.0 eV/A^4). 
                      This is the repulsion coefficient.

                Repulsion_Coeff_Surface: (4.0 eV/A^4). 
                      This is the repulsion coefficient used to be away from 
                      the defined surface.

                MinAtom2Plane_Thrd: (0.5 Angstrom). 
                      This is the min dist from the molecule to the contrained
                      plane

                EPSILON: (0.004). 
                      scale factor for steep descent relaxation.

                Max_Move_Length: (0.1 Angstrom). 
                      Max movement for single step during relaxation.

                MinMole_Thrd: (3.0 Angstrom). 
                      Mimimum mole-mole distance.

                MinAtom_Thrd: (1.5 Angstrom). 
                      Mimimum atom-atom distance.

                Atom2Surf_D: (0.02 eV).
                      D_e used in Morse potential

                Atom2Surf_A: (1.0 1/Angstrom).
                      alpha used in Morse potential

                MinAtom2Surface_Thrd: (1.5 Angstrom). 
                      Defines the mimimum dist from the molecule to the defined 
                      substrate surface.
            
    '''

    def __init__(self,InitScaling=1.0,MinScaling=0.08,ScalingFactor=0.8,Max_Move_Length=0.1,Repul_Force_Thrd=0.7,Repulsion_Coeff=5.0,Repulsion_Coeff_Surface=4.0,EPSILON=0.004,MaxAdjust=5000,Mole2Mole_D=0.02,Mole2Mole_A=1.0,MinMole_Thrd=3.0,MinAtom_Thrd=1.5,RepulsionOnly=False,Atom2Surf_D=0.02,Atom2Surf_A=1.0,MinAtom2Surface_Thrd=1.5,MinAtom2Plane_Thrd=0.5,SetMinToSub=False):
        self.InitScaling = InitScaling
        self.MinScaling = MinScaling
        self.ScalingFactor = ScalingFactor
        self.Max_Move_Length = Max_Move_Length
        self.Repul_Force_Thrd = Repul_Force_Thrd
        self.Repulsion_Coeff = Repulsion_Coeff
        self.Repulsion_Coeff_Surface = Repulsion_Coeff_Surface
        self.EPSILON = EPSILON
        self.MaxAdjust = MaxAdjust
        self.Mole2Mole_D = Mole2Mole_D
        self.Mole2Mole_A = Mole2Mole_A
        self.MinMole_Thrd = MinMole_Thrd
        self.MinAtom_Thrd = MinAtom_Thrd
        self.RepulsionOnly = RepulsionOnly
        self.Atom2Surf_D = Atom2Surf_D
        self.Atom2Surf_A = Atom2Surf_A
        self.MinAtom2Surface_Thrd = MinAtom2Surface_Thrd
        self.MinAtom2Plane_Thrd = MinAtom2Plane_Thrd
        self.SetMinToSub = SetMinToSub

    # main driver for molecular position adjusting
    def adjust_molecules2(self, name, coord, lat, image, SubstrateCoords, d_shape, d_molecule):
        if d_molecule.FixMolecule is None: 
            FixMolecule = []
        else:
            FixMolecule = d_molecule.FixMolecule[:]
        MolesRuleList = d_molecule.ForceInputMole[:]
        NMole = d_molecule.NMolecule
        scaling = self.InitScaling
        for i in range(0,self.MaxAdjust):
            if (i+1)%250 == 0:
                scaling = scaling * self.ScalingFactor
            if scaling <= self.MinScaling: scaling = self.MinScaling
            MassSphere = [ find_selected_mass_center(coord,imole) for imole in MolesRuleList ]
            coord, MassSphere = self.fast_adjust_by_constrain( name, coord, lat, MolesRuleList, FixMolecule, MassSphere, d_shape )
            if d_shape.WithSubstrate:
                Force = self.overlapping_force2(name, coord, lat, MolesRuleList, FixMolecule, MassSphere, d_shape, SubstrateCoords=SubstrateCoords)
            else:
                Force = self.overlapping_force2(name, coord, lat, MolesRuleList, FixMolecule, MassSphere, d_shape, )
            ForceMag = [ np.linalg.norm(Force[x,:]) for x in range(0,NMole) ]
            if max(ForceMag) <= self.Repul_Force_Thrd:
                print 'Adjusting image finish: ', image, '; steps: ', i
                return coord
            if i==self.MaxAdjust-1:
                print 'Adjusting image to max adjust: ', image, '; steps: ', i
                return coord
            self.adjust_moles2(name, coord, lat, MolesRuleList, scaling, Force, self.Max_Move_Length, self.EPSILON)

    # main driver for molecular position adjusting
    def adjust_atom(self, name, coord, lat, image, SubstrateCoords, d_shape, d_molecule):
        if d_molecule.FixMolecule is None: 
            FixMolecule = []
        else:
            FixMolecule = d_molecule.FixMolecule[:]
        NAtom = d_molecule.NMolecule
        MolesRuleList = d_molecule.ForceInputMole[:]
        scaling = self.InitScaling
        for i in range(0,self.MaxAdjust):
            if (i+1)%250 == 0:
                scaling = scaling * self.ScalingFactor
            if scaling <= self.MinScaling: scaling = self.MinScaling
            MassSphere = [ coord[i,:] for i in range(0,NAtom) ]
            if d_shape.WithSubstrate:
                Force = self.overlapping_force2(name, coord, lat, MolesRuleList, FixMolecule, MassSphere, d_shape, self.MinAtom_Thrd, self.MinAtom2Surface_Thrd,SubstrateCoords=SubstrateCoords)
            else:
                Force = self.overlapping_force2(name, coord, lat, MolesRuleList, FixMolecule, MassSphere, d_shape, self.MinAtom_Thrd, self.MinAtom2Surface_Thrd,)
            ForceMag = [ np.linalg.norm(Force[x,:]) for x in range(0,NAtom) ]
            if max(ForceMag) <= self.Repul_Force_Thrd:
                print 'Adjusting image finish: ', image, '; steps: ', i
                return coord
            self.adjust_atom2(name, coord, lat, MolesRuleList, scaling, Force, self.Max_Move_Length, self.EPSILON)
        print 'Warning: Reach MAXADJUST (will continue)', image, ForceMag
        return coord

    def fast_shift_by_constrain( self, name, coord, lat, MolesRuleList, FixMolecule, MassSphere, d_shape ):
        for i, istr in enumerate( d_shape.ConstrainPlane ):
            NormVec = np.array([istr[0][0],istr[0][1],istr[0][2]])
            D = istr[0][3]
            Sign = istr[1]
            for j, jmole in enumerate(MolesRuleList):
                DistPoint2Plane = (np.dot(NormVec,MassSphere[j]) - D)/np.linalg.norm(NormVec)
                if Sign*DistPoint2Plane < 0.0:
                    Shift = -2.0*DistPoint2Plane*NormVec/np.linalg.norm(NormVec)*Sign
                    for ja in jmole:
                        coord[ja,:] = coord[ja,:] + Shift
                    MassSphere[j] = MassSphere[j] + Shift
        for i, istr in enumerate( d_shape.ConstrainPolar ):
            Origin = istr[0]
            Inner  = istr[1]
            Outer  = istr[2]
            for j, jmole in enumerate(MolesRuleList):
                Dist, DistDir = distance_2d( Origin, MassSphere[j], lat )
                MassSphere[j] = MassSphere[j] + DistDir[0]*lat[0,:] + DistDir[1]*lat[1,:]
                for ja in jmole:
                    coord[ja,:] = coord[ja,:] + DistDir[0]*lat[0,:] + DistDir[1]*lat[1,:]
                theta, phi, Dist = cart_to_spherical( MassSphere[j] - Origin )
                if Dist > Outer:
                    Shift = 2.0*spherical_to_cart(theta, phi, Outer-Dist)
                    for ja in jmole:
                        coord[ja,:] = coord[ja,:] + Shift
                    MassSphere[j] = MassSphere[j] + Shift
                if Inner is not None: 
                    if Dist < Inner:
                        Shift = 2.0*spherical_to_cart(theta, phi, Inner-Dist)
                        for ja in jmole:
                            coord[ja,:] = coord[ja,:] + Shift
                        MassSphere[j] = MassSphere[j] + Shift
        return coord, MassSphere

    def fast_adjust_by_constrain( self, name, coord, lat, MolesRuleList, FixMolecule, MassSphere, d_shape ):
        UpdateMassSphere = []; NewIndex = []
        for i, imole in enumerate(MolesRuleList):
             InsideConstrain = self.check_if_point_in_ConsPlane( MassSphere[i], lat, d_shape ) and self.check_if_point_in_ConsPolar( MassSphere[i], lat, d_shape )
             if InsideConstrain:
                 UpdateMassSphere.append( MassSphere[i] )
             else:
                 NewIndex.append( i )
        AddiNumPoint = len(NewIndex)
       #print AddiNumPoint, NewIndex
        if AddiNumPoint > 0:
            NewPointCoord = d_shape.d_InputShape.gen_addi_rand_points(AddiNumPoint,UpdateMassSphere)
            for i, imole in enumerate(NewPointCoord):
                Shift = imole - MassSphere[NewIndex[i]]
                for ia in MolesRuleList[NewIndex[i]]:
                    coord[ia,:] = coord[ia,:] + Shift
                MassSphere[NewIndex[i]] = MassSphere[NewIndex[i]] + Shift
        return coord, MassSphere

    def check_if_point_in_ConsPlane( self, Point, Lat, d_shape ):
        Inside = True
        for i, istr in enumerate( d_shape.ConstrainPlane ):
            NormVec = np.array([istr[0][0],istr[0][1],istr[0][2]])
            D = istr[0][3]
            Sign = istr[1]
            DistPoint2Plane = (np.dot(NormVec,Point) - D)/np.linalg.norm(NormVec)
            if Sign*DistPoint2Plane < 0.0:
                Inside = Inside and False
        return Inside

    def check_if_point_in_ConsPolar( self, Point, Lat, d_shape ):
        Inside = True
        for i, istr in enumerate( d_shape.ConstrainPolar ):
            Origin = istr[0]
            Inner  = istr[1]
            Outer  = istr[2]
            Dist, DistDir = distance_2d( Origin, Point, Lat )
            Point = Point + DistDir[0]*Lat[0,:] + DistDir[1]*Lat[1,:]
            theta, phi, Dist = cart_to_spherical( Point - Origin )
            if Dist > Outer:
                Inside = Inside and False
            if Inner is not None: 
                if Dist < Inner:
                    Inside = Inside and False
        return Inside          

    # main driver to compute force
    def overlapping_force2( self, name, coord, lat, MolesRuleList, FixMolecule, MassSphere, d_shape, SubstrateCoords=None ):
        NMoles = len(MolesRuleList)
        Force = np.zeros((NMoles,3))
        if SubstrateCoords is not None:
            NumSub = np.shape(SubstrateCoords)[0]
        for i, imoles in enumerate(MolesRuleList):
            ForceOneAtom = np.zeros(3)
            if i not in FixMolecule:
                if numba_import:
                    force_mole = self.force_imole_nb( i, np.array(MassSphere), lat, self.Repulsion_Coeff, self.Mole2Mole_D, self.Mole2Mole_A, self.MinMole_Thrd, self.RepulsionOnly )
                else:
                    force_mole = self.force_imole( i, MassSphere, lat, self.Repulsion_Coeff, self.Mole2Mole_D, self.Mole2Mole_A, self.MinMole_Thrd, self.RepulsionOnly )
                ForceOneAtom = ForceOneAtom + force_mole
                if d_shape.WithSubstrate and self.SetMinToSub:
                    if numba_import:
                        force_sub = self.force_imole_surface_morse_pot_nb(i,np.array(MassSphere),lat,self.Atom2Surf_D,self.Atom2Surf_A, self.MinAtom2Surface_Thrd,SubstrateCoords,NumSub)
                    else:
                        force_sub = self.force_imole_surface_morse_pot(i,imoles,MassSphere,lat,self.Atom2Surf_D,self.Atom2Surf_A, self.MinAtom2Surface_Thrd,SubstrateCoords)
                    ForceOneAtom = ForceOneAtom + force_sub
                if d_shape.ConstrainPlane is not None:
                    for ist in range(0,len(d_shape.ConstrainPlane)):
                        ForceOneAtom = ForceOneAtom + self.force_imole_plane(i,imoles,MassSphere,coord,lat,self.Repulsion_Coeff_Surface, self.MinAtom2Plane_Thrd,d_shape.ConstrainPlane[ist])
                if d_shape.ConstrainPolar is not None:
                    for ist in range(0,len(d_shape.ConstrainPolar)):
                        ForceOneAtom = ForceOneAtom + self.force_imole_polar(i,imoles,MassSphere,coord,lat,self.Repulsion_Coeff_Surface, self.MinAtom2Plane_Thrd,d_shape.ConstrainPolar[ist])
            else:
                ForceOneAtom = np.zeros(3)
            Force[i,:] = ForceOneAtom
        return Force

    # move molecules
    @staticmethod
    def adjust_moles2(name, coord, lat, MolesRuleList, scaling, Force, Max_Move_Length, Epsilon ):
        for im, imoles in enumerate( MolesRuleList ):
            CoordShift = Epsilon * Force[im,:] * scaling
            for i in range(0,3):
                if np.absolute(CoordShift[i])>=Max_Move_Length: CoordShift[i] = np.sign(CoordShift[i])*Max_Move_Length
            for jatom in imoles:
                coord[jatom,:] = coord[jatom,:] + CoordShift
        return

    # move atoms
    @staticmethod
    def adjust_atom2(name, coord, lat, MolesRuleList, scaling, Force, Max_Move_Length, Epsilon ):
        for im, imoles in enumerate( MolesRuleList ):
            CoordShift = Epsilon * Force[im,:] * scaling
            for i in range(0,3):
                if np.absolute(CoordShift[i])>=Max_Move_Length: CoordShift[i] = np.sign(CoordShift[i])*Max_Move_Length
            coord[im,:] = coord[im,:] + CoordShift
        return

    @staticmethod
    @njit
    def force_imole_nb( i, MassSphere, lat, Repulsion_Coeff, Mole2Mole_D, Mole2Mole_A, MinMole_Thrd, RepulsionOnly ):
        # force between two moles
        ForceOneAtom = np.zeros( 3 )
        for j in range(0,len(MassSphere)):
            if j != i:
                Dist = distance_nb( MassSphere[j], MassSphere[i], lat )
                if Dist[0] < MinMole_Thrd:
                    R_ij = MassSphere[j] - (MassSphere[i]+Dist[1]*lat[0,:]+Dist[2]*lat[1,:])
                    Dist_l = np.linalg.norm( R_ij )
                    if RepulsionOnly:
                        ForceOneAtom = ForceOneAtom + 2.0*Repulsion_Coeff*(Dist_l-MinMole_Thrd)**3*R_ij/Dist_l
                    else:
                        ForceOneAtom = ForceOneAtom - 2.0*Mole2Mole_D*(1.0-np.exp(-Mole2Mole_A*(R_ij-MinMole_Thrd)))*Mole2Mole_A*np.exp(-Mole2Mole_A*(R_ij-MinMole_Thrd))
        return ForceOneAtom

    @staticmethod
    def force_imole( i, MassSphere, lat, Repulsion_Coeff, Mole2Mole_D, Mole2Mole_A, MinMole_Thrd, RepulsionOnly ):
        # force between two moles
        ForceOneAtom = np.zeros( 3 )
        for j in range(0,len(MassSphere)):
            if j != i:
                minDist, minDistDir = distance( MassSphere[j], MassSphere[i], lat )
                if minDist < MinMole_Thrd:
                    R_ij = MassSphere[j] - (MassSphere[i]+minDistDir[0]*lat[0,:]+minDistDir[1]*lat[1,:])
                    Dist_l = np.linalg.norm( R_ij )
                    if RepulsionOnly:
                        ForceOneAtom = ForceOneAtom + 2.0*Repulsion_Coeff*(Dist_l-MinMole_Thrd)**3*R_ij/Dist_l
                    else:
                        ForceOneAtom = ForceOneAtom - 2.0*Mole2Mole_D*(1.0-np.exp(-Mole2Mole_A*(R_ij-MinMole_Thrd)))*Mole2Mole_A*np.exp(-Mole2Mole_A*(R_ij-MinMole_Thrd))
        return ForceOneAtom

    @staticmethod
    def force_imole_surface_morse_pot( i, imoles, MassSphere, lat, Atom2Surf_D, Atom2Surf_A, MinAtom2Surface_Thrd, SubstrateCoords ):
        # energy:  E(eV) = 0.2*(1-exp(-0.6*(x-3)))^2 ;  Force: F(r) = -2*0.2*[1-exp(-0.6(x-3))]*0.6exp(-0.6(x-3))
        # force between two moles
        ForceOneAtom = np.zeros( 3 )
        NumSub = np.shape(SubstrateCoords)[0]
        for j in range(0,NumSub):
            minDist, minDistDir = distance_2d( SubstrateCoords[j,:], MassSphere[i], lat )
            ForceOneAtom = ForceOneAtom -2.0*Atom2Surf_D*(1.0-np.exp(-Atom2Surf_A*(minDist-MinAtom2Surface_Thrd)))*Atom2Surf_A*np.exp(-Atom2Surf_A*(minDist-MinAtom2Surface_Thrd))
        return ForceOneAtom
    
    @staticmethod
    @njit
    def force_imole_surface_morse_pot_nb( i, MassSphere, lat, Atom2Surf_D, Atom2Surf_A, MinAtom2Surface_Thrd, SubstrateCoords, NumSub ):
        # energy:  E(eV) = 0.2*(1-exp(-0.6*(x-3)))^2 ;  Force: F(r) = -2*0.2*[1-exp(-0.6(x-3))]*0.6exp(-0.6(x-3))
        # force between two moles
        ForceOneAtom = np.zeros( 3 )
        for j in range(0,NumSub):
            minDist = distance_2d_dist_nb( SubstrateCoords[j,:], MassSphere[i,:], lat )
            ForceOneAtom = ForceOneAtom -2.0*Atom2Surf_D*(1.0-np.exp(-Atom2Surf_A*(minDist-MinAtom2Surface_Thrd)))*Atom2Surf_A*np.exp(-Atom2Surf_A*(minDist-MinAtom2Surface_Thrd))
        return ForceOneAtom
 
    @staticmethod
    def force_imole_plane(i,imoles,MassSphere,coord,lat,Repulsion_Coeff_Surface,MinAtom2Surface_Thrd,ConstrainPlane):
        # ConstrainPlane is a list including a 4-element list and a scalar
        # the scalar decides which part of the cutted space the molecule wants to go: 1-> in the side pointed by normal vector; -1-> the other side
        Plane = np.array(ConstrainPlane[0][:3])
        D = ConstrainPlane[0][3]
        Direction = ConstrainPlane[1]
        ForceOneAtom = np.zeros( 3 )
        DistMolePlane = ( np.dot( Plane,MassSphere[i] ) - D )/np.linalg.norm( Plane ) * Direction
        if DistMolePlane <= MinAtom2Surface_Thrd:  # including negative value
            ForceOneAtom = ForceOneAtom -2.0*Repulsion_Coeff_Surface*(DistMolePlane-MinAtom2Surface_Thrd)**3 * Plane/np.linalg.norm(Plane) * Direction
        return ForceOneAtom
        
    @staticmethod
    def force_imole_polar(i,imoles,MassSphere,coord,lat,Repulsion_Coeff_Surface,MinAtom2Surface_Thrd,ConstrainPolar):
        # ConstrainPlane is a list including a 3-element list and two scalar
        # the scalar decides two radius, and the molecule will be in-between
        Center = np.array(ConstrainPolar[0])
        R_inner = ConstrainPolar[1]
        R_outer = ConstrainPolar[2]
        ForceOneAtom = np.zeros( 3 )
        Center2Mole = MassSphere[i]-Center
        if R_inner is not None: 
            DistMolePlane_inner = np.linalg.norm( Center2Mole ) - R_inner
        DistMolePlane_outer = R_outer - np.linalg.norm( Center2Mole )
        if R_inner is not None: 
            if DistMolePlane_inner < MinAtom2Surface_Thrd:
                ForceOneAtom = ForceOneAtom -2.0*Repulsion_Coeff_Surface*(DistMolePlane_inner-MinAtom2Surface_Thrd)**3 * Center2Mole/np.linalg.norm(Center2Mole)
        if DistMolePlane_outer < MinAtom2Surface_Thrd:
        #   print imoles,Repulsion_Coeff_Surface,DistMolePlane_outer,MinAtom2Surface_Thrd , Center2Mole,np.linalg.norm(Center2Mole) 
            ForceOneAtom = ForceOneAtom -2.0*Repulsion_Coeff_Surface*(DistMolePlane_outer-MinAtom2Surface_Thrd)**3 * (-1.0*Center2Mole)/np.linalg.norm(Center2Mole)
        return ForceOneAtom
 





 
class ga_search:

    ''' This class defines the main GA process

        args:
            Process: (mandatory input).
                = 0 -> initial step (normally generate random molecules
                = 1 -> GA crossing/mutation and generate new structure for relax
                = 2 -> Based on the relaxed structure, 

            SelectionRule: (total energy).
                = 'total energy' -> use total energy to select minimum energy structure
                = 'formation energy' (not implemented)

            UseInputCoord: (False).
                For initial step, if use the input coord or generate random molecules

            Child_Pop_Ratio: (0.9).
                When the size of population is N, it will generate N*Child_Pop_Ratio 
                  for child population
                
            Energy_Selection_Thrd: (0.01 eV)
                If external relaxation obtained energy difference for two structures 
                  are within this range, these two structures are believed to be the
                  same structures.

            AddingRandom: (True)
                In some cases (particularly at the beginning), once some structures 
                  are removed owing to their close energies, random structures are 
                  added

            FixMole_DFT: (True)
                Add '0 0 0' (or 'F F F') for the fixed molecules (defined in d_mole) 
                  when outputing coordinates

            NProc: (1)
                The adjusting position is parallel, here, specifying how many processes
                  are running at the same time (for best case, NProc should be the 
                  same to the structures needs adjusting).

            ParentSelectionScaleFactor: (2.0)
                Used for natural selection.

            UseInputCoord: (False)
                Use input coordinates instead of generating random structures
    '''
    def __init__( self, structure_energy, d_shape, d_crossmut, d_adjust_positions, ParentSelectionScaleFactor=2.0, NProc=1, d_molecule=None, d_atom=None, UseInputCoord=False, AdjustInputCoord=False, Child_Pop_Ratio=0.9, Energy_Selection_Thrd=0.01, AddingRandom=True, SelectionRule='total-energy', FixMole_DFT=True ):
        #
        self.structure_energy = structure_energy
        #
        self.ParentSelectionScaleFactor = ParentSelectionScaleFactor
        self.NProc = NProc
        self.UseInputCoord = UseInputCoord
        self.AdjustInputCoord = AdjustInputCoord
        self.Lat = None
        self.FixSub = None
        self.FixLig = None
        self.OutputCoords = None
        self.d_shape = d_shape
        self.PopFileNameList = None
        self.RelaxFileNameList = None
        self.NumSubstrateAtoms = None
        self.SubstrateCoords = None
        if d_molecule is not None and d_atom is None:
            self.system_type = 'mole'
            self.d_molecule = d_molecule
            self.NAtom = d_molecule.Natom
            print 'Input molecules are set as :'
            formula = gen_formula_from_name( self.d_molecule.ForceInputMoleName, self.d_molecule.ForceInputMole )
            print '  '+(', ').join(formula)
            print
        elif d_molecule is None and d_atom is not None:
            self.system_type = 'atom'
            self.d_molecule = d_atom
            self.NAtom = d_atom.Natom
            print 'Input atoms are set as :'
            self.SpeciesCount = _count_species( self.d_molecule.ForceInputMoleName )
            formula = deepcopy( self.SpeciesCount.keys() )
            for i, ikey in enumerate( self.SpeciesCount ):
                formula[i] = formula[i]+str(self.SpeciesCount[ikey])
            print '  '+(', ').join(formula)
            print
        else:
            print 'Miss/Duplicate setup of molecule/atom system'
            sys.exit()
        self.d_adjust_positions= d_adjust_positions
        self.d_crossmut = d_crossmut
        #
        self.SelectionRule = SelectionRule
        #
        self.Child_Pop_Ratio = Child_Pop_Ratio
        self.Energy_Selection_Thrd = Energy_Selection_Thrd
        self.AddingRandom = AddingRandom
        self.FixMole_DFT = FixMole_DFT

    def ga_read_structure_energy( self ):
        self.structure_energy.reset()
        self.structure_energy.read_file()
        self.StructureEnergyFile = self.structure_energy.StructureEnergyFilename
        self.NumCandi = self.structure_energy.NImage
        self.GenerationIndex = self.structure_energy.Generation
        self.FileNameList = self.structure_energy.FileName
        self.FileEnergyList = self.structure_energy.FileEnergy  # FileEnergyList = [] when Process == 0
        self.NStruct = len(self.FileNameList)
 
    def ga_read_coords( self ):
        self.read_coords()
        if self.d_shape.WithSubstrate:
            print 'Substrate elements are: ', self.d_shape.SubstrateAtomsSpecies
            print 'Number of substrate atoms are ', self.NumSubstrateAtoms
        self.SpeciesCount = _count_species( self.InputCoords[0][0] )
        print 'Number of genes are ', self.NumCandi
        print 'Populations are ', (', ').join(self.FileNameList)

    def ga_cycle( self, Process ):
        if Process == 0: 
            print '=> Generating 0 generation '
            self.ga_read_structure_energy()
            if self.GenerationIndex != 0:
                print_and_exit('structure-energy GenerationIndex should be set to 0 for initial generation')
            self.ga_read_coords()
            self.ga_initiate_coord()
            self.OutputCoords = deepcopy(self.InputCoords)
            FileNameList, FileNameListInd = self.write_coords()
            self.ga_initiate_dir(FileNameList)
            self.PopFileNameList = FileNameListInd
        elif Process == 1:
            self.ga_read_structure_energy( )
            print '=> Generating the child generation from generation %d '%(self.GenerationIndex)
            self.ga_read_coords()
            ParentPairs = self.ga_generate()
            self.opt_substrate( ParentPairs )
            FileNameList, FileNameListInd = self.write_coords(flag='RelaxIn')
            self.RelaxFileNameList = FileNameListInd
        elif Process == 2:
            self.ga_read_structure_energy( )
            print '=> Generating the next generation %d '%(self.GenerationIndex+1)
            self.ga_read_coords()
            ZipFileEnergy,EnergyRemove,SortBinder,CompensateFileList = self.ga_natural_selection()
            self.ga_next_generation_dir( ZipFileEnergy, EnergyRemove, CompensateFileList )
            FileNameList, FileNameListInd = self.write_coords(binder=SortBinder,flag='GA_1')
            self.PopFileNameList = FileNameListInd
        else:
            print 'Unknown Process, exit'
            sys.exit()

    # destroy SubstrateCoords but renew with the order of ParentPairs
    def opt_substrate( self, ParentPairs ):
        if not self.d_shape.WithSubstrate: return
        if self.d_shape.MixSubstrate:
            SubstrateCoords_tmp = deepcopy( self.SubstrateCoords )
            for i, ipair in enumerate(ParentPairs):
                self.SubstrateCoords[i][1] = (SubstrateCoords_tmp[ipair[0]][1] + SubstrateCoords_tmp[ipair[1]][1])/2.0


    def ga_generate( self ):
        ParentPairs = self.select_pairs( int(self.NumCandi*self.Child_Pop_Ratio) ) #test 80% structures of the Pop to check if they can be parents
        print 'Num. of mixed Pop is ' , len(ParentPairs)
        print 'Selected parent pairs: ' , ParentPairs
        self.ga_cross( ParentPairs )
        if self.d_crossmut.Mutation:
            self.ga_mute()
        if self.d_crossmut.MethodProb: 
            self.d_crossmut.update_method_prob()
        return ParentPairs

    def select_pairs( self, NumPairs ):
        ParentPairs = []
        for i in range(0,NumPairs):
            Pair = random_choose_pairs(self.FileEnergyList,self.ParentSelectionScaleFactor)
            while( Pair in ParentPairs ):
                Pair = random_choose_pairs(self.FileEnergyList,self.ParentSelectionScaleFactor)
            ParentPairs.append(Pair)
        return ParentPairs

    def ga_cross( self, ParentPairs ):
        self.OutputCoords = []
        CoordsPairs = pair_coords( self.InputCoords, ParentPairs )
        if self.d_crossmut.MethodProb:
            if not self.d_crossmut.FixCrossingMethod:
                self.d_crossmut.CrossingMethod = find_method_with_prob( self.d_crossmut.CrossingProb )
        print 'Using crossing method: ', self.d_crossmut.CrossingMethod
        CoordsForAdjust = []
        for i, icoord in enumerate( CoordsPairs ):
            PairedForceInputMole = [self.d_molecule.ForceInputMole,self.d_molecule.ForceInputMole]
            if self.d_molecule.FixMolecule is None: 
                PairedFixMolecule = [[],[]]
            else:
                PairedFixMolecule = [self.d_molecule.FixMolecule,self.d_molecule.FixMolecule]
            if self.system_type=='mole':
                newbornName, newbornCoord = self.d_crossmut.crossing_mole( self.d_crossmut.CrossingMethod, icoord[0], icoord[1], self.SpeciesCount, self.d_crossmut.MaxSamplePlane, Rotate=self.d_crossmut.CrossRotation, ForceInputMole=PairedForceInputMole, FixMolecule=PairedFixMolecule )
            if self.system_type=='atom':
                newbornName, newbornCoord = self.d_crossmut.crossing_atom( self.d_crossmut.CrossingMethod, icoord[0], icoord[1], self.SpeciesCount, self.d_crossmut.MaxSamplePlane, Rotate=self.d_crossmut.CrossRotation, ForceInputMole=PairedForceInputMole, FixMolecule=PairedFixMolecule )
            CoordsForAdjust.append([newbornName, newbornCoord])
        if self.d_molecule is not None:
            nprocess = len(CoordsForAdjust)
#           print 'debug>>>', nprocess
            if nprocess > self.NProc:
                print_and_exit('Not enough processes for adjusting molecules')
            pool = mp.Pool(processes=nprocess)
            if self.system_type=='mole': AllnewbornCoords = [ pool.apply_async(wrapper_adjust_molecules ,args=(icoord[0], icoord[1], self.Lat, i, self.SubstrateCoords[i][1], self.d_shape, self.d_molecule, self.d_adjust_positions)) for i, icoord in enumerate(CoordsForAdjust) ]
            if self.system_type=='atom': AllnewbornCoords = [ pool.apply_async(wrapper_adjust_atoms ,args=(icoord[0], icoord[1], self.Lat, i, self.SubstrateCoords[i][1], self.d_shape, self.d_molecule, self.d_adjust_positions)) for i, icoord in enumerate(CoordsForAdjust) ]
            AllnewbornCoords = [p.get() for p in AllnewbornCoords]
            pool.close()
            pool.join()
            self.OutputCoords = [ [CoordsForAdjust[i][0],AllnewbornCoords[i]] for i in range(0,len(CoordsForAdjust)) ]
        return

    def ga_mute( self ):
        MutationStructure = []
        if self.d_crossmut.MethodProb:
            if not self.d_crossmut.FixMutationMethod:
                self.d_crossmut.MutationMethod = find_method_with_prob( self.d_crossmut.MutationProb )
        print 'Using mutation method: ', self.d_crossmut.MutationMethod
        if self.OutputCoords is not None:
            for i in range(0,len(self.OutputCoords)):
                icoord = self.OutputCoords[i]
                NMole = self.d_molecule.NMolecule
                if np.random.random_sample() < self.d_crossmut.Mute_Random_Pop_Ratio:
                    MutationStructure.append(i)
                    print 'Mutating %s'%(str(i))
                    if self.system_type=='mole':
                        OutputCoords_tmp = self.d_crossmut.mutation( icoord,self.Lat,self.d_molecule,self.d_shape,self.d_adjust_positions.MinMole_Thrd )
                        self.OutputCoords[i][1] = np.copy(self.d_adjust_positions.adjust_molecules2(OutputCoords_tmp[0],OutputCoords_tmp[1],self.Lat,i,self.SubstrateCoords[i][1],self.d_shape,self.d_molecule))
                    if self.system_type=='atom':
                        OutputCoords_tmp = self.d_crossmut.mutation_atom( icoord,self.Lat,self.d_molecule,self.d_shape,self.d_adjust_positions.MinAtom_Thrd )
                        self.OutputCoords[i][1] = np.copy(self.d_adjust_positions.adjust_atom(OutputCoords_tmp[0],OutputCoords_tmp[1],self.Lat,i,self.SubstrateCoords[i][1],self.d_shape,self.d_molecule))
        print 'Muted structures from the selected parent pairs are: ' , MutationStructure

    def read_coords( self ):
        self.InputCoords = []
        Natom_d_molecule = len( self.d_molecule.ForceInputMoleName )
        self.SubstrateCoords = []; NumSubstrateAtom = []
        for i,ifile in enumerate( self.FileNameList ):
            name, lat, coord_cart, fix = _read_coord_single_file( ifile )
            if self.Lat is None: self.Lat = lat
            Natom = len(name)  # Natom is 0 if no coords read in (e.g. for the case without substrate)

            # read substrate coords
            if self.d_shape.WithSubstrate:
                if Natom==0:
                    print_and_exit('Must set substrate coords if WithSubstrate is set to True')
                SubstrateCoord = np.array([ coord_cart[x,:] for x in range(0,Natom) if name[x] in self.d_shape.SubstrateAtomsSpecies ])
                SubstrateName = [ x for x in name if x in self.d_shape.SubstrateAtomsSpecies ]
                self.SubstrateCoords.append( [SubstrateName, SubstrateCoord] )
                NumSubstrateAtom.append( len(SubstrateName) )
                self.NumSubstrateAtoms = NumSubstrateAtom[0]
                self.FixSub = fix[:self.NumSubstrateAtoms,:]
                # read ligand coords, if num of atom is more than self.NumSubstrateAtom
                if Natom > self.NumSubstrateAtoms:
                    self.FixLig = fix[self.NumSubstrateAtoms:,:]
                    LigandCoord = np.array([ coord_cart[x,:] for x in range(0,Natom) if name[x] not in self.d_shape.SubstrateAtomsSpecies ])
                    LigandName = [ x for x in name if x not in self.d_shape.SubstrateAtomsSpecies ]
                    self.InputCoords.append( [LigandName, LigandCoord] )
                    if self.d_molecule is not None and self.system_type=='mole':
                        check_and_correct_mole( LigandCoord, LigandName, lat, self.d_molecule.ForceInputMole, self.d_molecule.ForceInputMoleCoord, ifile, self.d_molecule.FixMolecule, self.d_molecule.AtomicMass )
                    if self.FixMole_DFT and self.d_molecule.FixMolecule is not None:
                        if self.system_type=='mole':
                            for ifix in self.d_molecule.FixMolecule:
                                self.FixLig[self.d_molecule.ForceInputMole[ifix]] = 0
                        if self.system_type=='atom':
                            for ifix in self.d_molecule.FixMolecule:
                                self.FixLig[ifix,:] = 0
                # otherwise give empty coords for ligands
                else:
                    LigandCoord = []; LigandName = []
                    self.InputCoords.append( [LigandName, LigandCoord] )
                    print 'No ligand/fix coord, only read input lattice and substrate'
                    self.FixLig = np.ones((Natom_d_molecule,3))
                    if self.FixMole_DFT and self.d_molecule.FixMolecule is not None:
                        if self.system_type=='mole':
                            for ifix in self.d_molecule.FixMolecule:
                                self.FixLig[self.d_molecule.ForceInputMole[ifix]] = 0
                        if self.system_type=='atom':
                            for ifix in self.d_molecule.FixMolecule:
                                self.FixLig[ifix,:] = 0

            # no substrate condition
            else:
                # read ligand coords if Natom is more than 0 (i.e. read_coord_single_file reads in some coordinates)
                self.NumSubstrateAtoms = 0  # need to set this for output
                self.SubstrateCoords.append( [None, None] )
                if Natom > 0:
                    self.FixLig = np.copy(fix)
                    if self.FixMole_DFT and self.d_molecule.FixMolecule is not None:
                        if self.system_type=='mole':
                            for ifix in self.d_molecule.FixMolecule:
                                self.FixLig[self.d_molecule.ForceInputMole[ifix]] = 0
                        if self.system_type=='atom':
                            for ifix in self.d_molecule.FixMolecule:
                                self.FixLig[ifix,:] = 0
                    LigandCoord = np.array([ coord_cart[x,:] for x in range(0,Natom)])
                    LigandName = [ x for x in name]
                    self.InputCoords.append( [LigandName, LigandCoord] )
                    if self.d_molecule is not None and self.system_type=='mole':
                        check_and_correct_mole( LigandCoord, LigandName, lat, self.d_molecule.ForceInputMole, self.d_molecule.ForceInputMoleCoord, ifile, self.d_molecule.FixMolecule, self.d_molecule.AtomicMass )
                # otherwise give empty coords for ligands
                else:
                    LigandCoord = []; LigandName = []
                    self.InputCoords.append( [LigandName, LigandCoord] )
                    print 'No coord/fix info, only read input lattice'
                    self.FixLig = np.ones((Natom_d_molecule,3))
                    if self.FixMole_DFT and self.d_molecule.FixMolecule is not None:
                        if self.system_type=='mole':
                            for ifix in self.d_molecule.FixMolecule:
                                self.FixLig[self.d_molecule.ForceInputMole[ifix]] = 0
                        if self.system_type=='atom':
                            for ifix in self.d_molecule.FixMolecule:
                                self.FixLig[ifix,:] = 0
        return

    def ga_initiate_coord( self ):
        if self.UseInputCoord: 
            print 'Coordinates provided from input structures'
            if self.AdjustInputCoord:
                for i in range(0,self.NumCandi):
                    self.adjust_single_structure(i,i)
            return 
        for i in range(0,self.NumCandi):
            AdjustedCoord = self.initiate_new_single_structure(i, i)
            self.InputCoords[i][0] = self.d_molecule.ForceInputMoleName
            self.InputCoords[i][1] = AdjustedCoord
        return

    def ga_natural_selection( self ):
        if self.SelectionRule == 'total-energy': ZipFileEnergy, EnergyRemove, ChildBinder, CompensateFileList = self.ga_natural_selection_total_energy()
        if self.SelectionRule == 'formation-energy': ZipFileEnergy, EnergyRemove, ChildBinder, CompensateFileList = self.ga_natural_selection_formation_energy()
        return ZipFileEnergy, EnergyRemove, ChildBinder, CompensateFileList
        
    def ga_natural_selection_total_energy( self ):
        OutputCoords_tmp = []; ChildBinder = {}; CompensateFileList = []
        OldLowestEnergy = self.FileEnergyList[0]
        ZipFileEnergy = sorted(zip( self.FileEnergyList,self.FileNameList ))
        print 'Sorted energy is: '
        for item in ZipFileEnergy:
            print '    %15s :    %15.4f'%(item[1],item[0])
        EnergyRemove = energy_selection( ZipFileEnergy, self.NumCandi, self.Energy_Selection_Thrd )
        if ZipFileEnergy[0][0] < OldLowestEnergy:
            print 'Lowest energy is %15.10f'%(ZipFileEnergy[0][0]), ' -*-'
        else:
            print 'Lowest energy is %15.10f'%(ZipFileEnergy[0][0])
        if self.d_crossmut.MethodProb:
            print 'Current method Prob, ', self.CrossingProb, self.MutationProb
            if ZipFileEnergy[0][0] < OldLowestEnergy:
                if self.CrossingProb[self.CrossingMethod] <= 0.8 and self.CrossingProb[self.CrossingMethod] >= 0.1:
                    self.CrossingProb[self.CrossingMethod] = self.CrossingProb[self.CrossingMethod] + 0.01
                if self.MutationProb[self.MutationMethod] <= 0.9 and self.MutationProb[self.MutationMethod] >= 0.1:
                    self.MutationProb[self.MutationMethod] = self.MutationProb[self.MutationMethod] + 0.01
            print 'Updating method Prob, ', self.CrossingProb, self.MutationProb
        OldNonRepeatNum = len(EnergyRemove)
        for i, istruct in enumerate(EnergyRemove):
            StructIndex = self.FileNameList.index( istruct[1] )
            OutputCoords_tmp.append( self.InputCoords[StructIndex] )
            ChildBinder[i] = StructIndex
        self.OutputCoords = deepcopy( OutputCoords_tmp[:OldNonRepeatNum] )
        if OldNonRepeatNum < self.NumCandi: 
            if self.AddingRandom == True:
                for i in range(0,self.NumCandi-OldNonRepeatNum):
                    print 'Compensate for NumCandi, adding randomed structures', i+OldNonRepeatNum
                    AdjustedCoord = self.initiate_new_single_structure(i+OldNonRepeatNum, i)
                    OutputCoords_tmp.append( [self.InputCoords[0][0],AdjustedCoord] ) # simply use 1st structure Name for the additional one
                    ChildBinder[i+OldNonRepeatNum] = 0  # simply use the substrate from 1st structure
                    EnergyRemove.append( (EnergyRemove[OldNonRepeatNum-1][0]+self.Energy_Selection_Thrd, str(i+OldNonRepeatNum)) )
                self.OutputCoords = deepcopy( OutputCoords_tmp[:self.NumCandi] )

        # every adding_random step, replace the last structure by a random structure
        if self.d_crossmut.EveryNStep_Random != 0:
            if (self.GenerationIndex % self.d_crossmut.EveryNStep_Random == 0) and self.GenerationIndex != 0:
                print 'Adding a randomed structure to replace the last one %d'%(self.NumCandi-1)
                AdjustedCoord = self.initiate_new_single_structure(self.NumCandi-1, 0)
                ChildBinder[self.NumCandi-1] = 0  # simply use the substrate from 1st structure
                EnergyRemove[self.NumCandi-1] = (EnergyRemove[self.NumCandi-2][0]+self.Energy_Selection_Thrd*0.1, str(self.NumCandi-1))  # the energy will be same to the last second one
                self.OutputCoords[self.NumCandi-1] = [self.InputCoords[0][0],AdjustedCoord]
        return ZipFileEnergy, EnergyRemove, ChildBinder, CompensateFileList

    def ga_natural_selection_formation_energy( self ):
        pass

    def ga_next_generation_dir( self, ZipFileEnergy, EnergyRemove, CompensateFileList ):
        # move files to dump directory
        # update structure-energy.dat file
        self.structure_energy.update_file( self.GenerationIndex, self.NumCandi, [x[0] for x in EnergyRemove] )
        # update method prob file
        if self.d_crossmut.MethodProb:
            self.d_crossmut.update_method_prob()
        # move tmp/ files into current directory, clean tmp
        if len(CompensateFileList) != 0:
            if len(os.listdir('./tmp')) == 0:
                print 'Empty tmp, it should have compensated files'
            else:
                for ifile in CompensateFileList:
                    shutil.move( './tmp/%s'%(ifile), './')
            if len(os.listdir('./tmp')) != 0:
                for ifile in os.listdir('tmp'):
                    os.remove( './tmp/%s'%(ifile) )

    def write_coords( self, flag=None, path=None, binder=None ):
        ''' binder is dict; it associates the OutputCoords with SubstrateCoords,
               binder = { OutputCoords_Index:SubstrateCoords_Index, ... }'''
        FileNameList = []; FileNameListInd = []
        for i in range(0,len(self.OutputCoords)):
            FileName = str(i)
            if flag is not None: FileName = FileName + '-' + flag
            if path is not None: FileName = './' + path + '/' + FileName
            FileNameList.append(FileName)
            FileNameListInd.append(str(i))
            TotalNumAtoms = self.NAtom + self.NumSubstrateAtoms
            # first print substrate atoms, then the ligands
            with open( FileName,'w' ) as f:
                f.write('%15.8f  %15.8f  %15.8f\n'%(self.Lat[0,0],self.Lat[0,1],self.Lat[0,2]) )
                f.write('%15.8f  %15.8f  %15.8f\n'%(self.Lat[1,0],self.Lat[1,1],self.Lat[1,2]) )
                f.write('%15.8f  %15.8f  %15.8f\n'%(self.Lat[2,0],self.Lat[2,1],self.Lat[2,2]) )
                f.write('1.0   %d\n'%(TotalNumAtoms))
                if binder is not None: 
                    SubInd = binder[i]
                else:
                    SubInd = i
                # if there is substrate
                if self.d_shape.WithSubstrate:
                    for iatom in range(0,self.NumSubstrateAtoms):
                        atom_coord = self.SubstrateCoords[SubInd][1][iatom,:]
                        f.write('%-2s   %15.6f  %15.6f  %15.6f  %d   %d   %d\n'%( self.SubstrateCoords[SubInd][0][iatom],atom_coord[0],atom_coord[1],atom_coord[2],self.FixSub[iatom,0],self.FixSub[iatom,1],self.FixSub[iatom,2]))
                # print ligands
                for iatom in range(0,self.NAtom):
                    if self.FixLig is None: 
                        self.FixLig = np.ones((self.NAtom,3),dtype=int)
                    atom_coord = self.OutputCoords[i][1][iatom,:]
                    f.write('%-2s   %15.6f  %15.6f  %15.6f  %d  %d  %d\n'%( self.OutputCoords[i][0][iatom],atom_coord[0],atom_coord[1],atom_coord[2],self.FixLig[iatom,0],self.FixLig[iatom,1],self.FixLig[iatom,2] ))
        return FileNameList, FileNameListInd

    def check_molecular_integrity( self, Name, Coord, Lat, ForceInputMole ): 
        moles = molecules( Coord, Name, Lat, Play=False, Record=True )
        moles.record_structure()
        UpdateMolesRule = []
        Results, UpdateMolesRule = check_if_same_formula( Name, moles.MolesRule, ForceInputMole )
        if Results:
            MolesRule = UpdateMolesRule[:]
            del moles
            print 'Found expected molecules'
            return flatten_list_loops_return( MolesRule )
        else:
            print 'Cannot find the expected molecules, exit'; del moles; sys.exit()

    def initiate_new_single_structure(self, WhichIndex, WhichSub):
        if self.system_type == 'mole':
            Coord_tmp = initiate_mole_coord( self.d_shape, self.d_molecule, self.Lat )
            AdjustedCoord = self.d_adjust_positions.adjust_molecules2( self.d_molecule.ForceInputMoleName, Coord_tmp, self.Lat, WhichIndex, self.SubstrateCoords[WhichSub][1], self.d_shape, self.d_molecule )
        if self.system_type == 'atom':
            Coord_tmp = initiate_atom_coord( self.d_shape, self.d_molecule, self.Lat )
            AdjustedCoord = self.d_adjust_positions.adjust_atom( self.d_molecule.ForceInputMoleName, Coord_tmp, self.Lat, WhichIndex, self.SubstrateCoords[WhichSub][1], self.d_shape, self.d_molecule )
        return AdjustedCoord

    def adjust_single_structure(self, WhichIndex, WhichSub):
        if self.system_type == 'mole':
            AdjustedCoord = self.d_adjust_positions.adjust_molecules2( self.d_molecule.ForceInputMoleName, self.InputCoords[WhichIndex][1], self.Lat, WhichIndex, self.SubstrateCoords[WhichSub][1], self.d_shape, self.d_molecule )
            self.InputCoords[WhichIndex][1] = np.copy( AdjustedCoord )
        if self.system_type == 'atom':
            AdjustedCoord = self.d_adjust_positions.adjust_atom( self.d_molecule.ForceInputMoleName, self.InputCoords[WhichIndex][1], self.Lat, WhichIndex, self.SubstrateCoords[WhichSub][1], self.d_shape, self.d_molecule )
            self.InputCoords[WhichIndex][1] = np.copy( AdjustedCoord )
        return AdjustedCoord

    def ga_cross_dir( self, ChildFileNameList ):
        # copy ChildFileNameList to ./work
        Destination = './work/'
        for ifile in ChildFileNameList:
            shutil.copy( ifile, Destination )

    def ga_initiate_dir( self, FileNameList ):
        # copy newly generated FileNameList to ./work/relax-*, and change name to *-RelaxIn
        if not os.path.exists( 'dump' ):
            os.makedirs( 'dump' )
        if not os.path.exists( 'work' ):
            os.makedirs( 'work' )
        Destination = './work/'
        return
        
###
# end class
###


def _pair_coords( InputCoords ):
    NumCandi = len( InputCoords ) -1
    PairedCoords = []; PairedCoordsIndex = []
    for i in range(0,NumCandi):
        PairedCoords.append( [InputCoords[i],InputCoords[(i+1)%NumCandi]] )
        PairedCoordsIndex.append([i,(i+1)%NumCandi])
    return PairedCoords, PairedCoordsIndex

def pair_coords( InputCoords, ParentPairs ):
    CoordsPairs = []
    for ipair in ParentPairs:
        CoordsPairs.append( [InputCoords[ipair[0]], InputCoords[ipair[1]]] )
    return CoordsPairs
   

def energy_selection( ZipFileEnergy, NumCandi, Energy_Selection_Thrd ):
    Energy_tmp = [[]]
    for i,istruct in enumerate(ZipFileEnergy):
        if len(Energy_tmp[-1]) == 0:
            Energy_tmp[-1].append(istruct)
        elif abs(istruct[0]-Energy_tmp[-1][-1][0]) <= Energy_Selection_Thrd:
            Energy_tmp[-1].append(istruct)
        else:
            Energy_tmp.append([istruct])
    EnergyRemove = [ istruct[0] for istruct in Energy_tmp ]
    print 'Selected Child population for next cycle: ', (', ').join( [ istruct[1] for istruct in EnergyRemove ] )
    if len(EnergyRemove)>NumCandi: return EnergyRemove[:NumCandi]
    return EnergyRemove

def initiate_mole_coord( d_shape, d_molecule, Lat ):
    NumAtom = d_molecule.NAtom
    if d_shape.Shape == 'open':
        CoordForAdjust = initiate_atom_coord_open( NumMole, Lat, d_molecule, d_shape )
    if d_shape.Shape == 'sphere':
        CoordForAdjust = initiate_atom_coord_sphere( NumMole, Lat, d_molecule, d_shape )
    if d_shape.Shape == 'ortho':
        CoordForAdjust = initiate_atom_coord_ortho( NumMole, Lat, d_molecule, d_shape )
    if d_shape.Shape == 'cylinder':
        CoordForAdjust = initiate_atom_coord_cylinder( NumMole, Lat, d_molecule, d_shape )
    return CoordForAdjust

def initiate_atom_coord_open( NumMole, Lat, d_mole, d_shape ):
    MassCoord = d_shape.d_InputShape.gen_init_rand_points( NumMole,Lat )
    CoordForAdjust = np.copy(MassCoord)
    return CoordForAdjust

def initiate_atom_coord_sphere( NumMole, Lat, d_mole, d_shape ):
    MassCoord = d_shape.d_InputShape.gen_init_rand_points( NumMole )
    CoordForAdjust = np.copy(MassCoord)
    return CoordForAdjust

def initiate_atom_coord_ortho( NumMole, Lat, d_mole, d_shape ):
    MassCoord = d_shape.d_InputShape.gen_init_rand_points( NumMole )
    CoordForAdjust = np.copy(MassCoord)
    return CoordForAdjust

def initiate_mole_coord( d_shape, d_molecule, Lat ):
    NumMole = d_molecule.NMolecule
    if d_shape.Shape == 'open':
        CoordForAdjust = initiate_mole_coord_open( NumMole, Lat, d_molecule, d_shape )
    if d_shape.Shape == 'sphere':
        CoordForAdjust = initiate_mole_coord_sphere( NumMole, Lat, d_molecule, d_shape )
    if d_shape.Shape == 'ortho':
        CoordForAdjust = initiate_mole_coord_ortho( NumMole, Lat, d_molecule, d_shape )
    if d_shape.Shape == 'cylinder':
        CoordForAdjust = initiate_mole_coord_cylinder( NumMole, Lat, d_molecule, d_shape )
    return CoordForAdjust

def initiate_mole_coord_open( NumMole, Lat, d_mole, d_shape ):
    MassCoord = d_shape.d_InputShape.gen_init_rand_points( NumMole,Lat )
    CoordForAdjust = initiate_mole_coord_gen( d_mole.ForceInputMole,d_mole.FixMolecule,d_mole.ForceInputMoleCoord, MassCoord )
    return CoordForAdjust

def initiate_mole_coord_sphere( NumMole, Lat, d_mole, d_shape ):
    MassCoord = d_shape.d_InputShape.gen_init_rand_points( NumMole )
    CoordForAdjust = initiate_mole_coord_gen( d_mole.ForceInputMole,d_mole.FixMolecule,d_mole.ForceInputMoleCoord, MassCoord )
    return CoordForAdjust

def initiate_mole_coord_cylinder( NumMole, Lat, d_mole, d_shape ):
    MassCoord = d_shape.d_InputShape.gen_init_rand_points( NumMole )
    CoordForAdjust = initiate_mole_coord_gen( d_mole.ForceInputMole,d_mole.FixMolecule,d_mole.ForceInputMoleCoord, MassCoord )
    return CoordForAdjust

def initiate_mole_coord_ortho( NumMole, Lat, d_mole, d_shape ):
    MassCoord = d_shape.d_InputShape.gen_init_rand_points( NumMole )
    CoordForAdjust = initiate_mole_coord_gen( d_mole.ForceInputMole,d_mole.FixMolecule,d_mole.ForceInputMoleCoord, MassCoord )
    return CoordForAdjust

def initiate_mole_coord_gen( ForceInputMole,FixMolecule,ForceInputMoleCoord, mass_random ):
    NAtom = sum([len(x) for x in ForceInputMole])
    CoordForAdjust = np.zeros((NAtom,3))
    if FixMolecule is None: 
        FixMolecule2 = []
    else:
        FixMolecule2 = FixMolecule[:]
    for j,jmole in enumerate(ForceInputMole):
        if j not in FixMolecule2:
            massj = np.sum( ForceInputMoleCoord[j], axis=0 )/np.shape(ForceInputMoleCoord[j])[0]
            MoleCoordj = init_random_mole_orientation( ForceInputMoleCoord[j] - massj + mass_random[j,:] )
            for jindex,jatom in enumerate(jmole):
                CoordForAdjust[jatom,:] = MoleCoordj[jindex,:]
        else:
            FixMoleCoord = ForceInputMoleCoord[j]
            for jindex,jatom in enumerate(jmole):
                CoordForAdjust[jatom,:] = FixMoleCoord[jindex,:]
    return np.vstack(CoordForAdjust)

def sample_in_circle( NumMole, CenterOfCell, InnerRadius, OuterRadius ):
    phi = np.random.random_sample(NumMole)*2.0*np.pi
    r = np.random.random_sample(NumMole)*(OuterRadius-InnerRadius)+InnerRadius
    PointCoord = np.zeros( (NumMole,3) )
    for j in range(0,NumMole):
        PointCoord[j,:] = spherical_to_cart( np.pi,phi[j],r[j] ) + CenterOfCell
    return PointCoord

def spherical_to_cart( theta, phi, r ):
    # theta is along z, phi is in x-y plane
    return np.array([ np.sin(theta)*np.cos(phi)*r, np.sin(theta)*np.sin(phi)*r, np.cos(theta)*r ])

def polar_to_cart( z, phi, r ):
    # theta is along z, phi is in x-y plane
    return np.array([ np.cos(phi)*r, np.sin(phi)*r, z ])

def cart_to_spherical( coord ):
    r = np.linalg.norm( coord )
    theta = np.arccos(coord[2]/r)
    return theta, np.arccos(coord[0]/(np.sin(theta)*r)), r

def init_random_mole_orientation( Coord ):
    NormalVector = rand_norm_vector()
    Angle = np.random.random_sample()*2.0*np.pi
    db = rotation( Coord, NormalVector, Angle, RotationCenter=np.sum(Coord,axis=0)/np.shape(Coord)[0] )
    db.apply_rotation_along_v()
    return db.rotated

def rand_norm_vector():
    theta = np.random.random_sample()*np.pi
    phi = np.random.random_sample()*2.0*np.pi
    NormalVector = np.array([ np.sin(theta)*np.cos(phi), np.sin(theta)*np.sin(phi), np.cos(theta) ])
    return NormalVector

###
def moles2element(moles,Name):
    return [np.array(Name)[x].tolist() for x in moles]

def atoms2element(moles,Name):
    return [np.array(Name)[x] for x in moles]

def combine_atoms_g1up_g2dn( g1up, g2dn, coord1, coord2, Name1, Name2, ForceInputMole, FixMolecule ):
    natom = len(Name1)
    CombName = [None]*natom
    #Moles_NotIn_g1up does not contain the FixMolecule
    Moles_NotIn_g1up = [x for x in ForceInputMole[0] if x not in g1up]
    Moles_NotIn_g1up_Name = atoms2element(Moles_NotIn_g1up,Name1)
    Moles_NotIn_g1up_Name_sort = resort(Moles_NotIn_g1up_Name)
    Moles_In_g2dn_Name = atoms2element(g2dn,Name2)
    Moles_In_g2dn_Name_sort = resort(Moles_In_g2dn_Name)
    CombCoord = np.zeros( (natom,3) ); FoundMoles = Moles_In_g2dn_Name_sort[:]
    # g1up and FixMolecule[0] are all from coord1, and will 
    #   be copied to CombCoord directly
    for i, imole in enumerate(g1up + FixMolecule):
        CombCoord[imole,:] = coord1[imole,:]
        CombName[imole] = Name1[imole]
    for i, imole in enumerate(Moles_NotIn_g1up_Name_sort):
        if imole in FoundMoles:
            g2dn_Index = FoundMoles.index( imole )
            FoundMoles[g2dn_Index] = None
            CombCoord[Moles_NotIn_g1up[i],:] = coord2[g2dn[g2dn_Index],:]
            CombName[Moles_NotIn_g1up[i]] = Name2[g2dn[g2dn_Index]]
        else:
            print 'found error when combing g1up and g2dn'
            print imole, Moles_In_g2dn_Name_sort
            sys.exit()
    return CombCoord, CombName

def combine_moles_g1up_g2dn( g1up, g2dn, coord1, coord2, Name1, Name2, ForceInputMole, FixMolecule ):
    natom = len(Name1)
    CombName = [None]*natom
    #Moles_NotIn_g1up does not contain the FixMolecule
    Moles_NotIn_g1up = [x for x in ForceInputMole[0] if x not in g1up and sorted(x) not in sorted_all(FixMolecule)]
    Moles_NotIn_g1up_Name = [ moles2element(x,Name1) for x in Moles_NotIn_g1up ]
    Moles_NotIn_g1up_Name_sort = [ resort(x) for x in Moles_NotIn_g1up_Name ]
    Moles_In_g2dn_Name = [ moles2element(x,Name2) for x in g2dn ]
    Moles_In_g2dn_Name_sort = [ resort(x) for x in Moles_In_g2dn_Name ]
    CombCoord = np.zeros( (natom,3) ); FoundMoles = Moles_In_g2dn_Name_sort[:]
    # g1up and FixMolecule[0] are all from coord1, and will 
    #   be copied to CombCoord directly
    for i, imole in enumerate(g1up + FixMolecule):
        for j, jatom in enumerate(imole):
            CombCoord[jatom,:] = coord1[jatom,:]
            CombName[jatom] = Name1[jatom]
    for i, imole in enumerate(Moles_NotIn_g1up_Name_sort):
        if imole in FoundMoles:
            g2dn_Index = FoundMoles.index( imole )
            FoundMoles[g2dn_Index] = []
            for j, jatom in enumerate(Moles_NotIn_g1up[i]):
                CombCoord[Moles_NotIn_g1up[i][j],:] = coord2[g2dn[g2dn_Index][j],:]
                CombName[Moles_NotIn_g1up[i][j]] = Name2[g2dn[g2dn_Index][j]]
        else:
            print 'found error when combing g1up and g2dn'
            print imole, Moles_In_g2dn_Name_sort
            sys.exit()
    return CombCoord, CombName

def find_method_with_prob( MethodProb ):
    nfind = 0
    while ( nfind == 0 ):
        prob = np.random.random_sample()
        find = np.where( MethodProb >= prob )[0]
        nfind = np.shape(find)[0]
    PickCrossing = np.random.randint(0,high=nfind)
    MethodSelect = find[PickCrossing]
    return MethodSelect

def random_choose_pairs(FileEnergyList,ParentSelectionScaleFactor):
    MinEnergy = min(FileEnergyList)
    EnergyRange = max(FileEnergyList) - MinEnergy
    NumCandi = len(FileEnergyList)
    c1 = None; c2 = None
    while (c1 is None):
        istruct = np.random.randint( NumCandi )
        Fitness = np.exp(-1.0*ParentSelectionScaleFactor*(FileEnergyList[istruct]-MinEnergy)/EnergyRange)
        if np.random.random_sample() <= Fitness:
            c1 = istruct
    while (c2 is None or c2 == c1):
        istruct = np.random.randint( NumCandi )
        Fitness = np.exp(-1.0*ParentSelectionScaleFactor*(FileEnergyList[istruct]-MinEnergy)/EnergyRange)
        if np.random.random_sample() <= Fitness:
            c2 = istruct
    return sorted([c1,c2])

def rotate_selected_atoms( coord, gmole, NormalVector, angle, RotationCenter ):
    RotatedCoord = np.copy(coord)
    gmoleFlat = sum(gmole,[])
    RotatingCoord = coord[gmoleFlat]
    db = rotation( RotatingCoord, NormalVector, angle, RotationCenter=RotationCenter )
    db.apply_rotation_along_v()
    RotatedCoord_tmp = db.rotated
    for i in range(0,len(gmoleFlat)):
        RotatedCoord[gmoleFlat[i],:] = RotatedCoord_tmp[i,:]
    return RotatedCoord
    
def rotate_selected_atoms_noflat( coord, gmole, NormalVector, angle, RotationCenter ):
    RotatedCoord = np.copy(coord)
    gmoleFlat = gmole[:]
    RotatingCoord = coord[gmoleFlat]
    db = rotation( RotatingCoord, NormalVector, angle, RotationCenter=RotationCenter )
    db.apply_rotation_along_v()
    RotatedCoord_tmp = db.rotated
    for i in range(0,len(gmoleFlat)):
        RotatedCoord[gmoleFlat[i],:] = RotatedCoord_tmp[i,:]
    return RotatedCoord
 
def check_and_correct_mole( Coord, Name, Lat, ForceInputMole, ForceInputMoleCoord, ifile, FixMolecule, AtomicMass ):
    RLat = np.linalg.inv( Lat )
    for j, jmole in enumerate(ForceInputMole):
        db = molecules()
        LigandName = np.array(Name)[jmole].tolist()
        SearchMole = db.search_molecules(Coord[jmole],LigandName,Lat)
        put_in_one_cell( Coord,jmole,Lat,RLat )
        if len(SearchMole) >=2:
            print 'Molecule breaks for image %s, moles %d'%(ifile, j), jmole, SearchMole, 
            if FixMolecule is not None:
                if j in FixMolecule:
                    print 'Fixed molecule is broken, exit'; sys.exit() 
            correct_coord( Coord, Name, Lat, jmole, SearchMole, ForceInputMoleCoord[j], AtomicMass )
    return

# here, it cannot handle large molecules, in particular the size of the molecule is larger than half of lattice
def put_in_one_cell( Coord, MolesRule_Flat, Lat, RLat ):
    atom0 = MolesRule_Flat[0]
    for iatom in MolesRule_Flat[1:]:
        dist, distdir = distance( Coord[atom0,:], Coord[iatom,:], Lat )
        if any(distdir) == True:
            Coord[iatom,:] = Coord[iatom,:] + distdir[0]*Lat[0,:] + distdir[1]*Lat[1,:]# + distdir[2]*Lat[2,:]
    MassCenter = find_selected_mass_center( Coord, MolesRule_Flat )
    Shift = np.dot( np.mod( np.dot( MassCenter, RLat ), 1.0 ), Lat ) - MassCenter
    for iatom in MolesRule_Flat:
        Coord[iatom,:] = Coord[iatom,:] + Shift
    return

def correct_coord( Coord, Name, Lat, Mole, SearchMole, ForceInputMoleCoord, AtomicMass ):
    SearchMoleList = [flatten_list_loops_return( imole ) for imole in SearchMole]
    ReIndexSearchMoleList = [ [Mole[i] for i,item in enumerate(imole)] for imole in SearchMoleList ]
    SearchMolesMass = [sum([AtomicMass[Name[jatom]] for jatom in imole]) for imole in ReIndexSearchMoleList]
    LargeMoleMass = SearchMolesMass.index(max(SearchMolesMass))
    COMLargeMoleMass = find_selected_mass_center( Coord, ReIndexSearchMoleList[LargeMoleMass] )
    for i, iatom in enumerate(Mole):
        Coord[iatom,:] = ForceInputMoleCoord[i,:] - _find_mass_center(ForceInputMoleCoord) + COMLargeMoleMass
    print 'Corrected the structure'
    return

def _count_mole_if_correct( g1up, g2dn, Name1, Name2, ForceInputMole, FixMolecule ): # ForceInputMole is only from Coords1
    NumMole = len(ForceInputMole)
    if NumMole != len(g1up+g2dn+FixMolecule):
        return False
    # molecules from g1up, g2dn and the FixMolecule (from coord1). 
    # Will not consider FixMolecule from coord2
    g1upName = [ resort(moles2element(x,Name1)) for x in g1up ]
    g2dnName = [ resort(moles2element(x,Name2)) for x in g2dn ]
    FixMoleculeName = [ resort(moles2element(x,Name1)) for x in FixMolecule ]
    CombName = sorted(g1upName + g2dnName + FixMoleculeName)
    ForceInputMoleName = sorted( [resort(moles2element(x,Name1)) for x in ForceInputMole] )
    if CombName == ForceInputMoleName:
        return True
    else:
        return False

def _count_atom_if_correct( g1up, g2dn, Name1, Name2, ForceInputMole, FixMolecule ): # ForceInputMole is only from Coords1
    NumMole = len(ForceInputMole)
    if NumMole != len(g1up+g2dn+FixMolecule):
        return False
    # molecules from g1up, g2dn and the FixMolecule (from coord1). 
    # Will not consider FixMolecule from coord2
    g1upName = resort(atoms2element(g1up,Name1))
    g2dnName = resort(atoms2element(g2dn,Name2))
    FixMoleculeName = resort(atoms2element(FixMolecule,Name1))
    CombName = sorted(g1upName + g2dnName + FixMoleculeName)
    ForceInputMoleName = sorted( resort(atoms2element(ForceInputMole,Name1)) )
    if CombName == ForceInputMoleName:
        return True
    else:
        return False

def check_if_same_formula( Name, MolesRule, ForceInputMole ):
    UpdateMolesRule = []; Results = True; MolesRuleCopy = MolesRule[:]
    for i, imolefor in enumerate(ForceInputMole):
        found, OneMolesRule = find_this_molecule( imolefor, MolesRuleCopy, Name )
        if found:
            UpdateMolesRule.append( OneMolesRule )
            Results = Results and True
            print 'Found expected molecule %s'%(imolefor)
        else:
            print 'Cannot find the expected moles %s'%(imolesfor); sys.exit()
            Results = Results and False
    return Results, UpdateMolesRule

def find_this_molecule( MolesFor, MolesRule, Name ):
    for j, jmoles in enumerate(MolesRule):
        if same_formular( MolesFor, jmoles, Name ):
            MolesRule.remove(jmoles)
            return True, jmoles
    for j, jmoles in enumerate(MolesRule):
        found, SubMolesRule = find_formular_in_submolecule( MolesFor, jmoles, Name )
        if found:
            MolesRule[j].remove(SubMolesRule)
            return True, SubMolesRule
    return False, []

def find_formular_in_submolecule( MolesFor, MolesRule, Name ):
    if isinstance(MolesRule, list):
        for j, jmoles in enumerate(MolesRule):
            if same_formular( MolesFor, jmoles, Name ):
                return True, MolesRule[j]
            else:
                found, SubMolesRule = find_formular_in_submolecule( MolesFor, jmoles, Name )
    else:
        if not same_formular( MolesFor, MolesRule, Name ):
            return False, []

def same_formular( MolesFor, MolesRule, Name ):
    MolesRuleFlat = flatten_list_loops_return( MolesRule )
    Formular = np.array( Name )[MolesRuleFlat].tolist()
    Results = True
    for i in range(0,len(MolesFor),2):
        Element = MolesFor[i]; NumAtom = int(MolesFor[i+1])
        if Element in Formular:
            if NumAtom == Formular.count(Element):
                Results = Results and True
            else:
                Results = False
        else:
            Results = False
    return Results

def gen_formula_from_name( Name, ForceInputMole ):
    ForceInputMoleName = [ resort([Name[jatom] for jatom in imole]) for imole in ForceInputMole ]
    Formulas = []
    for i, imole in enumerate(ForceInputMoleName):
        imoleElement = set(imole)
        FormulaMole = ''
        for j in imoleElement:
            if imole.count(j) >= 2:
                FormulaMole = FormulaMole + str(j) + str(imole.count(j))
            else:
                FormulaMole = FormulaMole + str(j)
        Formulas.append(FormulaMole)
    return Formulas
                
            
 
def _find_mass_center( Coord ):
    return np.sum( Coord, axis=0 )/np.shape(Coord)[0]

def find_selected_mass_center( Coord, Mole ):
    return np.sum( Coord[Mole], axis=0 )/float(len(Mole))



def wrapper_adjust_molecules( name, coord, lat, image, substratecoords, d_shape, d_molecule, d_adjust_positions):
    CoordAdjust =  d_adjust_positions.adjust_molecules2( name, coord, lat, image, substratecoords, d_shape, d_molecule )
    return CoordAdjust

def wrapper_adjust_atoms( name, coord, lat, image, substratecoords, d_shape, d_molecule, d_adjust_positions):
    CoordAdjust =  d_adjust_positions.adjust_atom( name, coord, lat, image, substratecoords, d_shape, d_molecule )
    return CoordAdjust



def _adjust_molecules( coord, lat, SurfacePlane_Z, UpSurfacePlane_Z,MolesRuleList,image,FixMolecule=None, WithSubstrate=True ):
    print 'Adjusting image ', image
    if FixMolecule is None: FixMolecule = []
    NMole = len( MolesRuleList )
    SurfacePlane = np.array([ 0.0,0.0,SurfacePlane_Z])
    UpSurfacePlane = np.array([ 0.0,0.0,UpSurfacePlane_Z])
    for i in range(0,MAXADJUST):
        if WithSubstrate == True:
            coord = _adjust_moles_surface_not_far( coord, lat, SurfacePlane, MolesRuleList, FixMolecule )
            coord = _adjust_moles_surface( coord, lat, SurfacePlane, UpSurfacePlane, MolesRuleList, FixMolecule )
        if WithSubstrate == False:
            coord = _adjust_moles_move_to_center( coord, lat, MolesRuleList, FixMolecule )
        coord = _adjust_moles_bond( coord, lat, MolesRuleList, FixMolecule )
        if WithSubstrate == True:
            if _if_too_close_moles(coord, lat, MolesRuleList, FixMolecule) and _if_too_close_moles_surface(coord,SurfacePlane,UpSurfacePlane,MolesRuleList,FixMolecule):
                return coord
        else:
            if _if_too_close_moles(coord, lat, MolesRuleList, FixMolecule):
                return coord
    print 'Reach MAXADJUST, exit'; print coord, image
    sys.exit()

def zip_molecules_sphere(coord, lat, MolesRuleList, MinMole_Thrd, MinAtom2Surface_Thrd):
    NMole = len( MolesRuleList )
    MassCenter = [ find_selected_mass_center(coord,imole) for imole in MolesRuleList ]
    RadiusList = [ max([ distance(coord[iatom,:],MassCenter[i],lat)[0] for iatom in imole ]) for i,imole in enumerate(MolesRuleList) ]
    if any([ (RadiusList[i]+RadiusList[j])>MinMole_Thrd for i in range(0,NMole-1) for j in range(i,NMole) ]): 
        print 'Warning: Radius sum of two molecules larger than MinMole_Thrd, increase MinMole_Thrd'
    if any([ RadiusList[i]> MinAtom2Surface_Thrd for i in range(0,NMole) ]):
        print 'Warning: Radius of atom is larger than MinAtom2Surface_Thrd, increase MinAtom2Surface_Thrd'
    return zip(MassCenter,RadiusList)

           
        
 
def _adjust_moles_surface_not_far( coord, lat, SurfacePlane, MolesRuleList, FixMolecule ):
    '''shift all the molecules (without changing relative position) so that their distance to the substrate will not be too far'''
    FixMolecule_Flat = sum(FixMolecule,[])
    NAtom = np.shape(coord)[0]
    NotFixedAtom = [ i for i in range(0,NAtom) if i not in FixMolecule_Flat ]
    WholeSysMinDist = np.amin(coord[NotFixedAtom,2]) - SurfacePlane[2]
    if WholeSysMinDist >= MINATOM2SURFACE_THRD:
        coord[NotFixedAtom] = coord[NotFixedAtom] - np.array([0.0,0.0,(WholeSysMinDist-MINATOM2SURFACE_THRD)/WHOLE_SHIFT_FACTOR])
    return coord

def _adjust_moles_surface( coord, lat, SurfacePlane, UpSurfacePlane, MolesRuleList, FixMolecule ):
    NAtom = np.shape(coord)[0]
    MolesRuleList2 = [ i for i in MolesRuleList if sorted(i) not in sorted_all(FixMolecule) ]
    for i, imoles in enumerate(MolesRuleList2):
        MolesDnDist = []; MolesUpDist = []
        for j, jatom in enumerate(imoles):
            MolesDnDist.append( _point2plane( coord[jatom]-SurfacePlane,np.array([0.0,0.0,1.0]) ) )
            MolesUpDist.append( _point2plane( coord[jatom]-UpSurfacePlane,np.array([0.0,0.0,1.0]) ) )
        if min(MolesDnDist) < MINATOM2SURFACE_THRD:
            coord[imoles] = coord[imoles] + SurfacePlane/np.linalg.norm(SurfacePlane) * (MINATOM2SURFACE_THRD + 0.2 - min(MolesDnDist)) * ADJUSTLAMBDA
        if max(MolesUpDist) > 0.0:
            coord[imoles] = coord[imoles] + UpSurfacePlane/np.linalg.norm(UpSurfacePlane) * (0.02 - max(MolesUpDist)) * ADJUSTLAMBDA
    return coord

def _if_too_close_moles_surface( coord, SurfacePlane, UpSurfacePlane, MolesRuleList,FixMolecule ):
    MolesRuleList2 = [ i for i in MolesRuleList if sorted(i) not in sorted_all(FixMolecule) ]
    Results = True
    for i, imoles in enumerate(MolesRuleList2):
        MolesDnDist = []; MolesUpDist = []
        for j, jatom in enumerate(imoles):
            MolesDnDist.append( _point2plane( coord[jatom]-SurfacePlane,np.array([0.0,0.0,1.0]) ) )
            MolesUpDist.append( _point2plane( coord[jatom]-UpSurfacePlane,np.array([0.0,0.0,1.0]) ) )
        if min(MolesDnDist) >= MINATOM2SURFACE_THRD and max(MolesUpDist) < 0.0:
            Results = Results and True
        else:
            Results = False
    return Results



def _adjust_moles_bond( coord, lat, MolesRuleList, FixMolecule ):
    NMoles = len(MolesRuleList)
    for i, imoles in enumerate(MolesRuleList):
        for j, jmoles in enumerate(MolesRuleList[i+1:]):
            MolesDist = [ [distance(coord[x,:],coord[y,:],lat),x,y] for x in imoles for y in jmoles ]
            MolesDistList = [x[0][0] for x in MolesDist]
            minDist = min(MolesDistList); minDistIndex = MolesDistList.index(minDist)
            if minDist < MINMOLE_THRD:
                ShiftDirection = MolesDist[minDistIndex][0][1]
                Vec_ij = coord[MolesDist[minDistIndex][2],:] + ShiftDirection[0]*lat[0,:] + ShiftDirection[1]*lat[1,:] - coord[MolesDist[minDistIndex][1],:]
                if sorted(imoles) in sorted_all(FixMolecule) and sorted(jmoles) not in sorted_all(FixMolecule):
                    coord[jmoles] = coord[jmoles] + Vec_ij/np.linalg.norm(Vec_ij) * (MINMOLE_THRD+0.1-minDist) * ADJUSTLAMBDA
                elif sorted(imoles) not in sorted_all(FixMolecule) and sorted(jmoles) in sorted_all(FixMolecule):
                    coord[imoles] = coord[imoles] - Vec_ij/np.linalg.norm(Vec_ij) * (MINMOLE_THRD+0.1-minDist) * ADJUSTLAMBDA
                elif sorted(imoles) in sorted_all(FixMolecule) and sorted(jmoles) in sorted_all(FixMolecule):
                    pass
                else:
                    coord[imoles] = coord[imoles] - Vec_ij/np.linalg.norm(Vec_ij) * (MINMOLE_THRD+0.1-minDist)/2.0* ADJUSTLAMBDA
                    coord[jmoles] = coord[jmoles] + Vec_ij/np.linalg.norm(Vec_ij) * (MINMOLE_THRD+0.1-minDist)/2.0* ADJUSTLAMBDA
    return coord

def _if_too_close_moles( coord, lat, MolesRuleList, FixMolecule ):
    NMoles = len(MolesRuleList); Results = True
    for i, imoles in enumerate(MolesRuleList):
        for j, jmoles in enumerate(MolesRuleList[i+1:]):
            MolesDist = [ [distance(coord[x,:],coord[y,:],lat),x,y] for x in imoles for y in jmoles ]
            MolesDistList = [x[0][0] for x in MolesDist]
            minDist = min(MolesDistList); minDistIndex = MolesDistList.index(minDist)
            if minDist >= MINMOLE_THRD:
                Results = Results and True
            else:
                Results = False
    return Results

def _adjust_coord( coord, SurfacePlane_Z ):
    natom = np.shape(coord)[0]
    SurfacePlane = np.array([ 0.0,0.0,SurfacePlane_Z])
    for i in range(0,MAXADJUST):
        coord = _adjust_surface( coord, SurfacePlane )
        coord = _adjust_bond( coord )
        if _if_too_close_bond(coord) and _if_too_close_surface(coord,SurfacePlane):
            return coord
    print 'Reach MAXADJUST, exit'; print coord
    sys.exit()

def _adjust_surface( coord, SurfacePlane ):
    natom = np.shape(coord)[0]
    for i in range(0,natom):
        Vec_i_Dist = _point2plane( coord[i],SurfacePlane )
        if Vec_i_Dist < MINATOM2SURFACE_THRD:
            coord[i,:] = coord[i,:] + SurfacePlane/np.linalg.norm(SurfacePlane) * (MINATOM2SURFACE_THRD - Vec_i_Dist ) * ADJUSTLAMBDA
    return coord

def _adjust_bond( coord ):
    natom = np.shape(coord)[0]
    for i in range(0,natom-1):
        for j in range(i+1,natom):
            Vec_ij = coord[j] - coord[i]; Vec_ij_Norm = Vec_ij/np.linalg.norm(Vec_ij); Vec_ij_Dist = _point2point( coord[i],coord[j] )
            if Vec_ij_Dist < MINBONDLENGTH_THRD:
                coord[j,:] = coord[j,:] + Vec_ij_Norm * (MINBONDLENGTH_THRD+0.1-Vec_ij_Dist)/2.0* ADJUSTLAMBDA
                coord[i,:] = coord[i,:] - Vec_ij_Norm * (MINBONDLENGTH_THRD+0.1-Vec_ij_Dist)/2.0* ADJUSTLAMBDA
    return coord

def _if_too_close_bond( coord ):
    Results = True
    natom = np.shape(coord)[0]
    for i in range(0,natom-1):
        for j in range(i+1,natom):
            Vec_ij_Dist = _point2point( coord[i,:],coord[j,:] )
            Results = Results and (Vec_ij_Dist >= MINBONDLENGTH_THRD)
    return Results               

def _if_too_close_surface( coord, SurfacePlane ):
    Results = True
    natom = np.shape(coord)[0]
    for i in range(0,natom):
        Vec_i_Dist = _point2plane( coord[i,:],SurfacePlane )
        Results = Results and (Vec_i_Dist >= MINATOM2SURFACE_THRD)
    return Results
                
def _point2plane( point, plane ):
    return np.dot(point,plane) / np.sqrt( np.dot(plane,plane) )

def _point2point( point1, point2 ):
    return np.linalg.norm( point2 - point1 )

def distance( coord1, coord2, Lat ):
    '''check x, y direction periodic boundary condition'''
    dist = []; distDirection = []
    for x in [-1, 0, 1]:
        for y in [-1, 0, 1]:
            dist.append( np.linalg.norm( coord1 - (coord2+x*Lat[0,:]+y*Lat[1,:]) ) )
            distDirection.append([x,y])
    SmallestDistIndex = np.argmin( np.array(dist) )
    return dist[SmallestDistIndex], distDirection[SmallestDistIndex]

@njit
def distance_nb( coord1, coord2, Lat ):
    '''check x, y direction periodic boundary condition'''
    dist = np.zeros(9); distDirection = np.zeros((9,2)); icount = 0
    for x in [-1, 0, 1]:
        for y in [-1, 0, 1]:
            dist[icount] = np.linalg.norm( coord1 - (coord2+x*Lat[0,:]+y*Lat[1,:]) )
            distDirection[icount,0] = x; distDirection[icount,1] = y
            icount = icount+1
    SmallestDistIndex = np.argmin( dist )
    return [ dist[SmallestDistIndex], distDirection[SmallestDistIndex,0], distDirection[SmallestDistIndex,1] ]

def distance_2d( coord1, coord2, Lat ):
    '''check x, y direction periodic boundary condition'''
    dist = []; distDirection = []
    for x in [-1, 0, 1]:
        for y in [-1, 0, 1]:
            dist.append( np.linalg.norm( coord1 - (coord2+x*Lat[0,:]+y*Lat[1,:]) ) )
            distDirection.append([x,y])
    SmallestDistIndex = np.argmin( np.array(dist) )
    return dist[SmallestDistIndex], distDirection[SmallestDistIndex]
        
@njit
def distance_2d_dist_nb( coord1, coord2, Lat ):
    '''check x, y direction periodic boundary condition'''
    dist = np.zeros(9); distDirection = np.zeros((9,2)); icount = 0
    for x in [-1, 0, 1]:
        for y in [-1, 0, 1]:
            dist[icount] = np.linalg.norm( coord1 - (coord2+x*Lat[0,:]+y*Lat[1,:]) )
            distDirection[icount,0] = x; distDirection[icount,1] = y
            icount = icount+1
    SmallestDistIndex = np.argmin( dist )
    return dist[SmallestDistIndex]

def comp_all_dist_cluster( coord, AllCoord, Num ):
    distMin = 100.0
    if Num > 0:
        for i, icoord in enumerate(AllCoord):
            dist = np.linalg.norm( icoord - coord )
            if dist < distMin:
                distMin = dist
    return distMin

def gen_rand_r_theta_phi(r):
    theta = np.random.random()*np.pi
    phi = np.random.random()*2.0*np.pi
    r_prim = np.cbrt(np.random.random())*r
    return r_prim, theta, phi


def pick_atom_and_rotate(NotFix, PickAtom, coord, MassCenter, direction):
    PickMoleMass = coord[NotFix[PickAtom],:]
    if direction == 2: 
        rot_axis_aim = np.array([0.0, 0.0, 1.0])
    elif direction == 1: 
        rot_axis_aim = np.array([0.0, 1.0, 0.0])
    else:
        rot_axis_aim = np.array([1.0, 0.0, 0.0])
    RotateAxis = np.cross( PickMoleMass-MassCenter, rot_axis_aim )
    RotateAngle = np.arccos( (PickMoleMass[direction]-MassCenter[direction])/np.linalg.norm(PickMoleMass-MassCenter) )  # unit of Rad
    if not np.any(RotateAxis): 
        RotateAxis = np.array([0.0, 1.0, 0.0])
        RotateAngle = np.random.random_sample()*np.pi*2.0-np.pi
    coordRotate = rotate_selected_atoms_noflat( coord, NotFix, RotateAxis, RotateAngle, MassCenter )
    return coordRotate

def pick_mole_and_rotate(NotFix, PickMole, coord, MassCenter, direction):
    if direction == 2:
        rot_axis_aim = np.array([0.0, 0.0, 1.0])
    elif direction == 1:
        rot_axis_aim = np.array([0.0, 1.0, 0.0])
    else:
        rot_axis_aim = np.array([1.0, 0.0, 0.0])
    PickMoleMass = find_selected_mass_center( coord, NotFix[PickMole] )
    RotateAxis = np.cross( PickMoleMass-MassCenter, rot_axis_aim )
    RotateAngle = np.arccos( (PickMoleMass[direction]-MassCenter[direction])/np.linalg.norm(PickMoleMass-MassCenter) )  # unit of Rad
    if not np.any(RotateAxis):
        RotateAxis = np.array([0.0, 1.0, 0.0])
        RotateAngle = np.random.random_sample()*np.pi*2.0-np.pi
    coordRotate = rotate_selected_atoms( coord, NotFix, RotateAxis, RotateAngle, MassCenter )
    return coordRotate

def _count_species( name ):
    SpeciesCount = {}
    for i,iatom in enumerate( name ):
        if iatom not in SpeciesCount.keys():
            SpeciesCount[iatom] = 1
        else:
            SpeciesCount[iatom] += 1
    return SpeciesCount

def resort( Name ):
    return [y for x,y in sorted(enumerate(Name),key=lambda u: ORDER.index(u[1]) if u[1] in ORDER else len(ORDER) )]

def sorted_all( ListofList ):
    if ListofList is None:
        return []
    else:
        return [ sorted(i) for i in ListofList if i is not None ]

def read_structure_energy_file( filename ):
    '''file-energy.dat file format:
         Generation_number  Num_structures
         filename_1    file1_energy
         filename_2    file2_energy
         filename_3    file3_energy
         ...
       when process=0, the second column could be empty.
    '''
    with open( filename, 'r' ) as f:
        RawInput = f.readlines()
    [Generation,NImage] = [x.strip() for x in RawInput[0].strip('\n').split()]
    FileName = []; FileEnergy = []
    for iline in RawInput[1:]:
        LineSplit = [x.strip() for x in iline.strip('\n').split()]
        FileName.append(LineSplit[0])
        if len(LineSplit) == 2:
            FileEnergy.append(float(LineSplit[1]))
#   NImage = len(RawInput)-1
    return int(Generation), int(NImage), FileName, FileEnergy

def read_method_prob_file( filename):
    ''' method-prob file format:
          Crossing_method  Crosing possibilities
          Mutation_method  Mutation_possibilities
    '''
    with open( filename, 'r' ) as f:
        RawInput = f.readlines()
    ParseLine = [float(x.strip()) for x in RawInput[0].strip('\n').split()]
    CrossingMethod = int(ParseLine[0])
    CrossingProb = np.array(ParseLine[1:])
    if len(CrossingProb) <= CrossingMethod:
        print 'Method-prob file wrong (cross), exit'
        sys.exit()
    ParseLine = [float(x.strip()) for x in RawInput[1].strip('\n').split()]
    MutationMethod = int(ParseLine[0])
    MutationProb = np.array(ParseLine[1:])
    if len(MutationProb) <= MutationMethod: 
        print 'Method-prob file wrong (mutation), exit'
        sys.exit()
    return CrossingMethod,CrossingProb,MutationMethod,MutationProb

def _read_coord_single_file( filename ):
    # use Angstrom unit
    with open(filename,'r') as f:
        raw = f.readlines()
    coord = []; name = []; fix = []; lat = np.zeros( (3,3) )
    for order,i in enumerate(raw[0:3]):
        tmp_ = [float(j.strip().strip('\n')) for j in i.split()]
        lat[order,:] = np.array( tmp_ )
    lat_const = float(raw[3].strip().strip('\n').split()[0])
    lat = lat * lat_const   # lat is Angstrom
    if len(raw) > 4:
        for i in raw[4:]:
            tmp_ = [j.strip().strip('\n') for j in i.split()]
            name.append(tmp_[0])
            tmp_2 = map(float, tmp_[1:4])
            coord.append( tmp_2 )
            if (len(tmp_)>=7):
                tmp_3 = map(int, tmp_[4:7])
                fix.append( tmp_3 )
            else:
                fix.append( np.array([1, 1, 1]) )
    else:
        print 'No coordinates found for %s'%(filename)
    return name, lat, np.array( coord ), np.array( fix ) # name, lattice vector, coord_frac, coord_cart


def print_and_exit( exit_args ):
    print exit_args
    sys.exit()



