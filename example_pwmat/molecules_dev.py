

import sys
import numpy as np
from rotation import *



TYPICAL_BONDLENGTH = {'CH':1.1,'OH':1.3,'CC':1.3,'CO':1.3, 'NH':1.1, 'CN':1.4,
                      'NO':1.4,'OO':1.6}

ATOM_MASS = {'O':16.0, 'H':1.0, 'N':14.0, 'C': 12.0}

ORDER = ['C','N','O','H']

class molecules:



    def __init__( self, Substrate=None ):
        if Substrate is not None:
            self.Substrate = Substrate


    def search_molecules( self, Coord, Name, Lat ):
        MolesRule = []
        ResortName = resort(Name)
        for Element in ResortName:
            MolesRule_Flat = []
            flatten_list_loops( MolesRule, MolesRule_Flat )
            LigandNameIndex = [x for x in range(0,len(Name)) if x not in MolesRule_Flat]
            if Element not in MolesRule_Flat:
                MolesRule.append([])
                FirstAtomIndex = Element
                MolesRule[-1].append( FirstAtomIndex )
                AttachedAtomIndex = []
                flatten_list_loops( MolesRule, AttachedAtomIndex )
                LigandNameIndex = [x for x in LigandNameIndex if x not in AttachedAtomIndex]
                find_atoms_attached_to_ligand( Coord, Name, Lat, FirstAtomIndex, MolesRule[-1], LigandNameIndex )
        return MolesRule


def flatten_list_loops_return( ListofList ):
    Flatlist = []
    flatten_list_loops( ListofList, Flatlist )
    return Flatlist

def find_atoms_attached_to_ligand( Coord, Name, Lat, FirstAtomIndex, AttachedAtomRule, LigandNameIndex ):
    '''Search atoms  linked to the FristAtomIndex'''
    if len(LigandNameIndex) == 0: return
    i = FirstAtomIndex
    for m in LigandNameIndex[:]:
        AttachedAtomIndex = []
        flatten_list_loops( AttachedAtomRule, AttachedAtomIndex )
        if m not in AttachedAtomIndex:  # this line is important, takes a lot of time to figure out
            LigandLigandDist = distance( Coord[i,:], Coord[m,:], Lat )
            if LigandLigandDist <= get_bond_length( Name[i], Name[m], TYPICAL_BONDLENGTH ):
                AttachedAtomRule.append( [m] )
                AttachedAtomIndex = []
                flatten_list_loops( AttachedAtomRule, AttachedAtomIndex )
                LigandNameIndex = [x for x in LigandNameIndex if x not in AttachedAtomIndex]
                find_atoms_attached_to_ligand( Coord, Name, Lat, m, AttachedAtomRule[-1], LigandNameIndex )
    return

def distance( coord1, coord2, Lat ):
    '''check x, y direction periodic boundary condition'''
    dist = []
    for x in [-1, 0, 1]:
        for y in [-1, 0, 1]:
            dist.append( np.linalg.norm( coord1 - (coord2+x*Lat[0,:]+y*Lat[1,:]) ) )
    return min(dist)

def flatten_list_loops( ListofList, Flatlist ):
    if isinstance(ListofList,list):
        for x in ListofList:
            if not isinstance(x, list):
                Flatlist.append(x)
            else:
                flatten_list_loops( x, Flatlist )
    else:
        Flatlist.append(ListofList)

def get_bond_length( Name1, Name2, TYPICAL_BONDLENGTH ):
    if TYPICAL_BONDLENGTH.has_key( Name1+Name2 ):
        return TYPICAL_BONDLENGTH[Name1+Name2]
    elif TYPICAL_BONDLENGTH.has_key( Name2+Name1 ):
        return TYPICAL_BONDLENGTH[Name2+Name1]
    else:
        return 0.001

def resort( Name ):
    return [x for x,y in sorted(enumerate(Name),key=lambda u: ORDER.index(u[1]) if u[1] in ORDER else len(ORDER) )]

def find_mass_weight_selected_mass_center( Name, Coord, Mole ):
    MassCenter = np.zeros(3); TotalMass = 0.0
    for i in Mole:
        MassCenter = MassCenter + Coord[i,:]*ATOM_MASS[Name[i]]
        TotalMass = TotalMass + ATOM_MASS[Name[i]]
    return MassCenter/TotalMass

