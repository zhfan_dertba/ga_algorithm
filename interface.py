
import sys, os, re
import shutil
from  subprocess import Popen, PIPE, STDOUT
from  collections import Counter
import multiprocessing as mp
import signal, time
import numpy as np


atom_spec  = {'Cu':'29', 'O':'8', 'H':'1', 'C':'6', 'N':'7', 'S':'16', 'Li':'3', 'Pt':'78'}
atom_spec2 = {'29':'Cu', '8':'O', '1':'H', '6':'C', '7':'N', '16':'S', '3':'Li', '78':'Pt'}
fix_d2b = {0:'F', 1:'T'}   # 0/F: fixed, 1/T: allow to move
fix_b2d = {'F':0, 'T':1}   # 0/F: fixed, 1/T: allow to move


class shells:

     def shell_cmd(self, cmd, output=False, maxtime=10000):
         self.maxtime=maxtime
         if output:
             p = self.shell_command_output(cmd)
         else:
             self.shell_command(cmd)
             p = None
         return p

     def shell_command_output(self,cmd):
         p=Popen(cmd,shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
         # for every 10s, it will check if the calculation is finished
         for i in range(0,self.maxtime/10):
             poll = p.poll()
             if poll is None:
                 time.sleep(10)
             else:
                 break
         return p.stdout.read()

     def shell_command(self,cmd):
         p=Popen(cmd,shell=True)
         # for every 10s, it will check if the calculation is finished
         for i in range(0,self.maxtime/10):
             poll = p.poll()
             if poll is None:
                 time.sleep(10)
             else:
                 break
         return


class relaxation_io:

     def __init__(self, package_str, pwd):
         self.package_str = package_str
         self.package_inputfilename = {'pwmat' : 'atom.config',
                                       'vasp'  : 'POSCAR',
                                       'lammps': 'input.data',
                                       'qe'    : 'in_relax' }
         self.package_outputfilename = {'pwmat' : 'MOVEMENT',
                                        'vasp'  : 'POSCAR',
                                        'lammps': 'log.xyz',
                                        'qe'    : 'out_relax' }
         if self.package_str == 'pwmat':
             if not os.path.exists( pwd+'/etot.input' ):
                 print 'etot.input is necessary for PWmat DFT relaxation'; sys.exit()
         if self.package_str == 'qe':
             if not os.path.exists( pwd+'/in_relax' ):
                 print 'in_relax is necessary for QE DFT relaxation'; sys.exit()
             else:
                 with open('in_relax','r') as f:
                     raw = f.read()
                 p = re.compile("\s*celldm\(1\)\s*=\s*([\d\.]+)")
                 alat = float(p.search(raw).group(1))
                 if abs(alat-1.88972)>0.01:
                     print 'QE in_relax celldm(1) must be set to 1.889716'; sys.exit()

     def work_coords2relax(self,InputFileList):
         Destination = './work/'
         for ifile in InputFileList:
             InputFile = Destination+'%s-relax/%s-RelaxIn'%(ifile,ifile)
             OutputFile = Destination+'%s-relax/'%(ifile)+self.package_inputfilename[self.package_str]
             self.coords2relax(InputFile,OutputFile)
         return

     def work_relax2coords(self,InputFileList):
         Destination = './work/'
         Energy = []
         for ifile in InputFileList:
             InputFile = Destination+'%s-relax/'%(ifile)+self.package_outputfilename[self.package_str]
             OutputFile = Destination+'%s-relax/%s-RelaxOut'%(ifile,ifile)
#            self.relax2coords(InputFile,OutputFile)
             Energy_relax = self.relax2coords_return_energy(InputFile,OutputFile)
             Energy.append( Energy_relax )
         return  Energy

     def coords2relax(self,InputFile,OutputFile):
         return {  'pwmat'  : lambda : self.coords2pwmat(InputFile,OutputFile), 
                   'vasp'   : lambda : self.coords2vasp(InputFile,OutputFile), 
                   'lammps' : lambda : self.coords2lammps(InputFile,OutputFile), 
                   'qe'     : lambda : self.coords2qe(InputFile,OutputFile)  }[self.package_str]()

     def relax2coords(self,InputFile,OutputFile):
         return {  'pwmat'  : lambda : self.pwmat2coords(InputFile,OutputFile), 
                   'vasp'   : lambda : self.vasp2coords(InputFile,OutputFile), 
                   'lammps' : lambda : self.lammps2coords(InputFile,OutputFile), 
                   'qe'     : lambda : self.qe2coords(InputFile,OutputFile)  }[self.package_str]()

     def relax2coords_return_energy(self,InputFile,OutputFile):
         if self.package_str == 'pwmat'  : energy = self.pwmat2coords_energy(InputFile,OutputFile)
         if self.package_str == 'vasp'   : energy = self.vasp2coords_energy(InputFile,OutputFile)
         if self.package_str == 'lammps' : energy = self.lammps2coords_energy(InputFile,OutputFile)
         if self.package_str == 'qe'     : energy = self.qe2coords_energy(InputFile,OutputFile)
         return energy

     def copy_output(self,DestiPath,InputFile):
         for ifile in InputFile:
             WorkDestiPath = DestiPath + '/work/%s-relax/%s-RelaxOut'%(ifile,ifile)
             shutil.copy( WorkDestiPath, DestiPath )
         return

     def copy_input(self,DestiPath,InputFile,flag=None):
         for i,ifile in enumerate(InputFile):
             if not os.path.exists( DestiPath+'%s-relax'%(ifile) ):
                 os.makedirs( DestiPath+'%s-relax'%(ifile) )
             if flag is None:
                shutil.copy( ifile, DestiPath+'%s-relax/'%(ifile)+'%s-RelaxIn'%(ifile) )
             else:
                shutil.copy( ifile+'-'+flag, DestiPath+'%s-relax/'%(ifile)+'%s-RelaxIn'%(ifile) )
         return

     def copy_setup_input( self, DestiPath, FileNameList, SetupInput ):
         for ifile in SetupInput:
             for jfile in FileNameList:
                 shutil.copy( ifile, DestiPath+'%s-relax/'%(jfile) )
         return

     def dump_files(self,pwd,GenerationIndex,PopInputFileList,RelaxInputFileList,StructureEnergyFile,MethodProbFile=None,DumpFilesList=None,InitialStep=False):
         dump_files_list = { 'pwmat' : ['REPORT','RELAXSTEPS','OUT.OCC','atom.config'],
                             'vasp'  : ['POSCAR','INCAR'],
                             'lammps': ['lmp.log'],
                             'qe'    : ['in_relax','out_relax']  }   # select which files are saved in dump folder
         # move files ($i, $i-RelaxIn, $i-RelaxOut) to dump directory
         Destination = pwd+'/dump/%s-Generation'%(str(GenerationIndex))
         # if %s-Generation not exist; make one
         if not os.path.exists( Destination ):
             os.makedirs( Destination )
         # if %s-Generation exists already; rename the existed one. Always higher number is newer
         else:
             i=1
             while os.path.exists(Destination+'.%s'%(str(i))):
                 i=i+1
             os.rename( Destination, Destination+'.%s'%(str(i)) )
             os.makedirs( Destination )
         for ifile in PopInputFileList:
             shutil.move( ifile, Destination )
         for ifile in RelaxInputFileList:
             shutil.move( ifile+'-RelaxOut', Destination )
             # for initial step, $i is the $i-RelaxIn, so $i-RelaxIn will not be in parent directory
             if not InitialStep:
                 shutil.move( ifile+'-RelaxIn' , Destination )
         shutil.copy( StructureEnergyFile, Destination )
         if MethodProbFile is not None: shutil.move( MethodProbFile, Destination )
         # move work/ files to dump directory
         Destination2 = pwd+'/dump/%s-Generation/Relax/'%(str(GenerationIndex))
         if not os.path.exists( Destination2 ):
             os.makedirs( Destination2 )
         if DumpFilesList is None:
             DumpFilesList = dump_files_list[self.package_str]
         for ifile in RelaxInputFileList:
             if not os.path.exists( Destination2+'%s-relax'%(ifile) ): os.makedirs( Destination2+'%s-relax'%(ifile) )
             for jfile in DumpFilesList:
                 shutil.move( pwd+'/work/%s-relax/%s'%(ifile,jfile), Destination2+'%s-relax/'%(ifile) )  # it will overwrite automatically
         # delete all contents in ./work/
         for ifile in RelaxInputFileList:
             shutil.rmtree(pwd+'/work/%s-relax'%(ifile))
         self.clean_work_dir(pwd)
         # rename $i-GA_1 to $i ($i-GA is the resorted filename for next crossing, generated by process=2)
         for ifile in PopInputFileList:
             os.rename(ifile+'-GA_1', ifile)

     def clean_dump_dir(self,pwd,GenerationIndex):
         if os.path.exists(pwd+'/dump/%s-Generation/'%s(GenerationIndex)): 
             shutil.rmtree(pwd+'/dump/%s-Generation/'%s(GenerationIndex))

     def clean_work_dir(self,pwd):
#        print os.path.exists(pwd+'/work')
         if os.path.exists(pwd+'/work'):
             shutil.rmtree(pwd+'/work')
             os.makedirs(pwd+'/work')
         else:
             os.makedirs(pwd+'/work')

     def coords2pwmat(self,inputfilename,outputfilename):
         # pwmat only uses fractional coords
         name, lat, coord, fix = _read_coord_single_file(inputfilename)
         natom = len(name)
         reclat = np.linalg.inv( lat )
         coord_frac = np.dot( coord, reclat )
         with open(outputfilename,'w') as f:
             f.write('%d \n'%(natom))
             f.write('Lattice vectors\n')
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[0,0],lat[0,1],lat[0,2]))
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[1,0],lat[1,1],lat[1,2]))
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[2,0],lat[2,1],lat[2,2]))
             f.write('Position\n')
             for i in range(0,natom):
                 f.write('%s   %20.12f  %20.12f  %20.12f  %d  %d  %d\n'%(atom_spec[name[i]],coord_frac[i,0],coord_frac[i,1],coord_frac[i,2],fix[i,0],fix[i,1],fix[i,2]))
         return

     def pwmat2coords(self,inputfilename,outputfilename):
         name, lat, coord, fix = _read_pwmat_single_file(inputfilename)
         coord_cart = np.dot( coord, lat )
         natom = len(name)
         with open(outputfilename,'w') as f:
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[0,0],lat[0,1],lat[0,2]))
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[1,0],lat[1,1],lat[1,2]))
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[2,0],lat[2,1],lat[2,2]))
             f.write('1.0  %d \n'%(natom))
             for i in range(0,natom):
                 f.write('%s   %20.12f  %20.12f  %20.12f  %d  %d  %d\n'%(atom_spec2[name[i]],coord_cart[i,0],coord_cart[i,1],coord_cart[i,2],fix[i,0],fix[i,1],fix[i,2]))
         return

     def pwmat2coords_energy(self,inputfilename,outputfilename):
         # inputfilename: MOVEMENT;  outputfilename: $i-RelaxOut
         try:
             Time, e_tot, lattice, coordinates, force, velocity, potential, charge = read_movement_file( inputfilename )
         except:
             print 'MOVEMENT file reading error'; sys.exit()
         coord = coordinates[-1][:,1:4]
         fix = coordinates[-1][:,4:].astype(int)
         name = coordinates[-1][:,0].astype(int)
         lat = lattice[-1]
         coord_cart = np.dot( coord, lat )
         natom = np.shape(name)[0]
         with open(outputfilename,'w') as f:
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[0,0],lat[0,1],lat[0,2]))
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[1,0],lat[1,1],lat[1,2]))
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[2,0],lat[2,1],lat[2,2]))
             f.write('1.0  %d \n'%(natom))
             for i in range(0,natom):
                 f.write('%s   %20.12f  %20.12f  %20.12f  %d  %d  %d\n'%(atom_spec2[str(name[i])],coord_cart[i,0],coord_cart[i,1],coord_cart[i,2],fix[i,0],fix[i,1],fix[i,2]))
         return e_tot[-1]


     def coords2qe(self,inputfilename,outputfilename):
         # inputfilename: $i-RelaxIn; outputfilename: in_relax
         name, lat, coord, fix = _read_coord_single_file(inputfilename)
         natom = len(name)
         # QE input coord will be written with Angstrom coords
         # QE outputfilename will be "in_relax"
         #  coords will be appended to "in_relax"
         with open(outputfilename,'a') as f:
             f.write('ATOMIC_POSITIONS (angstrom)\n')
             for i in range(0,natom):
                 f.write('%s   %20.12f  %20.12f  %20.12f  %d  %d  %d\n'%(name[i],coord[i,0],coord[i,1],coord[i,2],fix[i,0],fix[i,1],fix[i,2]))
         return

     def qe2coords_energy(self,inputfilename,outputfilename):
         # inputfilename: out_relax; outputfilename: $i-RelaxOut
         try:
             with open(inputfilename,"r") as f:
                 qe_raw = f.read()
         except:
             print 'out_relax file reading error'; sys.exit()
         lat = qe_find_lattice(qe_raw)
         e_tot_all = qe_find_energy(qe_raw)
         name_all,opt_coord_all,fix_all = find_opt_coord(qe_raw)
         if len(e_tot_all)==0 or len(opt_coord_all)==0:
             print 'No relaxation found from out_relax'; sys.exit()
         ind = min(len(e_tot_all),len(opt_coord_all))-1
         opt_coord = opt_coord_all[ind]; e_tot = e_tot_all[ind]; name = name_all[ind]; fix = fix_all[ind]
         natom = len(name)
         with open(outputfilename,'w') as f:
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[0,0],lat[0,1],lat[0,2]))
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[1,0],lat[1,1],lat[1,2]))
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[2,0],lat[2,1],lat[2,2]))
             f.write('1.0  %d \n'%(natom))
             for i in range(0,natom):
                 f.write('%s   %20.12f  %20.12f  %20.12f  %d  %d  %d\n'%(name[i],opt_coord[i,0],opt_coord[i,1],opt_coord[i,2],fix[i,0],fix[i,1],fix[i,2]))
         return float(e_tot)

     def vasp2coords_energy(self,inputfilename,outputfilename):
         # inputfilename: OUTCAR; outputfilename: $i-RelaxOut
         try:
             with open(inputfilename,"r") as f:
                 vasp_raw = f.read()
         except:
             print 'OUTCAR file reading error'; sys.exit()
         p_e = re.compile('free  energy   TOTEN  = \s*([\-\.\E\d]+)\s*eV')
         e_tot = float( p_e.findall( vasp_raw )[-1] )
         p_lat = re.compile('\s+direct lattice vectors \s+ reciprocal lattice vectors\s*(([\d\.\-]+\s*){9,})')
         lat = np.array(map(float, [ i for i in  p_lat.findall( vasp_raw )[-1][0].strip().strip('\n').split() ] )).reshape((3,-1))[:,0:3]
         p_cart = re.compile('\s+POSITION \s+ TOTAL\-FORCE \(eV\/Angst\)\s*[\-]+\s*(([\-]*\d\.[\d]{5,6}\s*){3,})[\-]+')
         opt_coord = np.array(map(float, [ i for i in p_cart.findall( vasp_raw )[-1][0].strip().strip('\n').split() ] )).reshape((-1,6))[:,0:3]
         natom = np.shape(opt_coord)[0]
         # obtain fix and name info
         filename = outputfilename.split('-')[0]+'-RelaxIn'
         name, lat_tmp, coord_tmp, fix = _read_coord_single_file(filename)
         with open(outputfilename,'w') as f:
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[0,0],lat[0,1],lat[0,2]))
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[1,0],lat[1,1],lat[1,2]))
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[2,0],lat[2,1],lat[2,2]))
             f.write('1.0  %d \n'%(natom))
             for i in range(0,natom):
                 f.write('%s   %20.12f  %20.12f  %20.12f  %d  %d  %d\n'%(name[i],opt_coord[i,0],opt_coord[i,1],opt_coord[i,2],fix[i,0],fix[i,1],fix[i,2]))
         return float(e_tot)

     def coords2vasp(self,filename):
         name, lat, coord, fix = _read_coord_single_file(filename)
         natom = len(name)
         name_set = Counter( name )
         with open('POSCAR.'+filename,'w') as f:
             f.write('%s \n'%('VASP '+filename))
             f.write('1\n')
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[0,0],lat[0,1],lat[0,2]))
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[1,0],lat[1,1],lat[1,2]))
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[2,0],lat[2,1],lat[2,2]))
             f.write('%s\n'%('  '.join(['%2s'%(i) for i in name_set.keys()])))
             f.write('%s\n'%('  '.join([str('%3d'%(i)) for i in name_set.values()])))
             for ispec in name_set.keys():
                 for i in range(0,natom):
                     if name[i]==ispec: f.write('%20.12f  %20.12f  %20.12f  %d  %d  %d\n'%(coord[i,0],coord[i,1],coord[i,2],fix_d2b[i,0],fix_d2b[i,1],fix_d2b[i,2]))
         return 'POSCAR.'+filename

     def vasp2coords(self,filename):
         name, lat, coord, fix = _read_vasp_single_file(filename)
         natom = len(name)
         with open('coords.'+filename,'w') as f:
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[0,0],lat[0,1],lat[0,2]))
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[1,0],lat[1,1],lat[1,2]))
             f.write('%20.12f   %20.12f  %20.12f\n'%(lat[2,0],lat[2,1],lat[2,2]))
             f.write('1.0  %d \n'%(natom))
             for i in range(0,natom):
                 f.write('%s   %20.12f  %20.12f  %20.12f  %d  %d  %d\n'%(name[i],coord[i,0],coord[i,1],coord[i,2],fix[i,0],fix[i,1],fix[i,2]))
         return 'coords.'+filename

     def coords2lammps(self,filename):
         pass

     def lammps2coords(self,filename):
         pass

     def clean_work(self, path):
         return {  'pwmat'  : lambda : self.clean_pwmat(path), 
                   'vasp'   : lambda : self.clean_vasp(path), 
                   'lammps' : lambda : self.clean_lammps(path), 
                   'qe'     : lambda : self.clean_qe(path)  }[self.package_str]()


class relaxation_run:

     def __init__(self, package_str, nproc=1, para_cmd='mpirun -np 1', package_cmd_path='./', package_exec=None, max_run_time=20000.0):
         '''  package_str: which package used to do relaxation.
              nproc: how many tasks will be lanuched at the same time.
                 The total number of procs will be nproc*nproc_task.
              para_cmd: wrapper to launch MPI task (e.g. mpirun, aprun, ...).
              package_cmd_path: *path* for the executable.
              max_run_time: maximum wall time for one DFT relaxation
         '''
         package_exec_dic = {'pwmat':'PWmat', 'vasp':'VASP', 'lammps':'lmp', 'qe':'pw.x'}
         if package_exec is None:
             self.package_exec = package_exec_dir[package_str.lower()]
         else:
             self.package_exec = package_exec
         self.package_str = package_str
         self.nproc = nproc
         self.para_cmd = para_cmd
         if package_cmd_path is None:
             self.package_cmd = self.package_exec
         else:
             self.package_cmd = package_cmd_path + '/' + self.package_exec
         self.max_run_time = max_run_time

     def opt_relaxation(self,FileNameList,pwd):
         Destination = pwd+'/work'
         nprocess = len(FileNameList)
         if nprocess > self.nproc:
             print 'Not sufficient nodes for external relaxation'; sys.exit()
         pool = mp.Pool(processes=nprocess)
         ErrorHandler = [ pool.apply_async(run_relaxation,args=(self.package_str,self.para_cmd,self.package_cmd,self.max_run_time,Destination+'/%s-relax'%(ifile),pwd)) for i, ifile in enumerate(FileNameList) ]
         pool.close()
         pool.join()
         return 
 
     def extract_energies(self,FileNameList,pwd):
         Destination = pwd+'/work'
         RelaxationEnergy = [ extract_energy(self.package_str,Destination+'/%s-relax'%(ifile)) for i, ifile in enumerate(FileNameList) ]
         return RelaxationEnergy



def _read_pwmat_single_file( filename ):
    # use Angstrom unit
    with open(filename,'r') as f:
        raw = f.readlines()
    coord = []; name = []; lat = np.zeros( (3,3) ); fix = []
    natom = int( raw[0].split()[0] )
    for order,i in enumerate(raw[2:5]):
        tmp_ = [float(j.strip().strip('\n')) for j in i.split()]
        lat[order,:] = np.array( tmp_ )
    if len(raw) > 4:
        for i in range(0,natom):
            raw_i = raw[i+6]
            tmp_ = [j.strip().strip('\n') for j in raw_i.split()]
            name.append(str(tmp_[0]))
            tmp_2 = list(map(float, tmp_[1:4]))
            coord.append( tmp_2 )
            if len(tmp_) > 4:
                tmp_3 = list(map(int, tmp_[4:7]))
                fix.append( tmp_3 )
            else:
                fix.append( np.array([1, 1, 1]) )
    else:
        print ('No coordinates found, exit'); sys.exit()
    return name, lat, np.array( coord ), np.array( fix ) # name, lattice vector, coord_frac, coord_cart

def _read_vasp_single_file( filename ):
    # use Angstrom unit
    with open(filename,'r') as f:
        raw = f.readlines()
    coord = []; name = []; fix = []; lat = np.zeros( (3,3) )
    scale_factor = float(raw[1].split()[0])
    for order,i in enumerate(raw[2:5]):
        tmp_ = [float(j.strip().strip('\n')) for j in i.split()]
        lat[order,:] = np.array( tmp_ )
    name_set = raw[5].split()
    name_count = [int(j) for j in raw[6].split()]
    name_count_spec = len(name_set)
    natom = sum( name_count )
    name = [ name_count[iname] for j in range(name_count[iname]) for iname in range(name_count_spec) ]
    for i in raw[6:natom+6]:
        tmp_ = [j.strip().strip('\n') for j in i.split()]
        tmp_2 = map(float, tmp_[0:3])
        coord.append( tmp_2 )
        if (len(tmp_)>4):
            tmp_3 = [ fix_b2d[j] for j in tmp_[3:6] ]
            fix.append( tmp_3 )
        else:
            fix.append( [1, 1, 1] )
    return name, lat, np.array( coord ), np.array( fix ) # name, lattice vector, coord_frac, coord_cart

def _read_coord_single_file( filename ):
    # use Angstrom unit
    with open(filename,'r') as f:
        raw = f.readlines()
    coord = []; name = []; fix = []; lat = np.zeros( (3,3) )
    for order,i in enumerate(raw[0:3]):
        tmp_ = [float(j.strip().strip('\n')) for j in i.split()]
        lat[order,:] = np.array( tmp_ )
    lat_const = float(raw[3].strip().strip('\n').split()[0])
    lat = lat * lat_const   # lat is Angstrom
    if len(raw) > 4:
        for i in raw[4:]:
            tmp_ = [j.strip().strip('\n') for j in i.split()]
            name.append(tmp_[0])
            tmp_2 = map(float, tmp_[1:4])
            coord.append( tmp_2 )
            if (len(tmp_)>4):
                tmp_3 = map(int, tmp_[4:7])
                fix.append( tmp_3 )
            else:
                fix.append( np.array([1, 1, 1]) )
    else:
        print 'No coordinates found for %s'%(filename)
    return name, lat, np.array( coord ), np.array( fix ) # name, lattice vector, coord_frac, coord_cart


def run_relaxation(package_str,para_cmd,package_cmd,max_run_time,Destination_dir,Parent_dir):
    if package_str == 'pwmat': run_pwmat(para_cmd,package_cmd,max_run_time,Destination_dir,Parent_dir)
    if package_str == 'vasp': run_vasp(para_cmd,package_cmd,max_run_time,Destination_dir,Parent_dir)
    if package_str == 'lammps': run_lammps(para_cmd,package_cmd,max_run_time,Destination_dir,Parent_dir)
    if package_str == 'qe': run_qe(para_cmd,package_cmd,max_run_time,Destination_dir,Parent_dir)     

def run_pwmat(mpicmd,package_cmd,max_run_time,Destination_dir,Parent_dir):
    os.chdir( Destination_dir )
    cmd = mpicmd + ' ' + package_cmd
    shell_command_nooutput(cmd,maxtime=max_run_time,outputfile='log')
    return

def run_qe(mpicmd,package_cmd,max_run_time,Destination_dir,Parent_dir):
    os.chdir( Destination_dir )
    cmd = mpicmd + ' ' + package_cmd
    shell_command_nooutput(cmd,maxtime=max_run_time,inputfile='in_relax',outputfile='out_relax')
    return

def run_vasp(mpicmd,package_cmd,max_run_time,Destination_dir,Parent_dir):
    os.chdir( Destination_dir )
    cmd = mpicmd + ' ' + package_cmd
    shell_command_nooutput(cmd,maxtime=max_run_time,outputfile='log')
    return

def check_pwmat_end( path ):
    if os.path.exists( path+'/REPORT' ):
        with open(path+'/REPORT','r') as f:
            raw = f.read()
            p = re.compile("force_max,stress.lt.tolforce, finished")
            m = p.search( raw )
        # converged case, pick up final.config
        if m is not None:
            if os.path.exists( path+'/RELAXSTEPS' ):
                if os.path.exists( path+'/final.config' ):
                    end_ind = 1
                else:
                    print 'Lost final.config'; end_ind = -2
            else:
                print 'Lost RELAXSTEPS'; end_ind = -3
        # non-converged case, check how many steps it relaxed
        else:
            if os.path.exists( path+'/RELAXSTEPS' ):
                if os.path.exists( path+'/final.config' ):
                    end_ind = 2
                else:
                    print 'Non-converged, no final.config generated'; end_ind = -4
            else:
                print 'Non-converged, no RELAXSTEPS generated, exit'; end_ind = -5
    else:
        print 'No REPORT generated, fatal error'; end_ind = -1
    return end_ind

def extract_pwmat_E( path ):
    with open(path+'/RELAXSTEPS', 'r') as f:
        raw = f.readlines()[-1]
        E = float( raw.split()[4] )
    return E
 
def extract_energy( package_str, path ):
    check_end = { 'pwmat' : lambda path: check_pwmat_end( path ), 
                  'vasp'  : lambda path: check_vasp_end( path ),
                  'lammps': lambda path: check_lmp_end( path ),
                  'qe'    : lambda path: check_qe_end( path )  }
    End_flag = check_end[package_str](path)
    if End_flag < 0: print '  Fatal error, exit', End_flag; sys.exit()
    extract_E = { 'pwmat' : lambda path: extract_pwmat_E( path ), 
                  'vasp'  : lambda path: extract_vasp_E( path ),
                  'lammps': lambda path: extract_lmp_E( path ),
                  'qe'    : lambda path: extract_qe_E( path )  }
    Energy = extract_E[package_str](path)
    return Energy

# kill an MPI job could be challenging. It is better to setup max relaxation
#    step in a DFT input and give long enough MaxRunTime to finish the DFT
#    relaxations by itself
def shell_command_nooutput(cmd,maxtime,inputfile=None,outputfile=None):
    fin = None; fout = None
    if inputfile is not None: fin = open(inputfile,'r')
    if outputfile is not None: fout = open(outputfile,'w')
    p=Popen(cmd.split(),shell=False,stdin=fin,stdout=fout)
    # for every 10s, it will check if the calculation is finished
    for i in range(0,maxtime/10):
        poll = p.poll()
        if poll is None:
            time.sleep(10)
        else:
            break
    if p.poll() is None:
        p.kill()
        os.killpg(os.getpgid(p.pid), signal.SIGKILL)
    time.sleep(10)
    icount = 0
    while check_pid(p.pid):
        p.kill()
        os.killpg(os.getpgid(p.pid), signal.SIGKILL)
        time.sleep(10)
        icount = icount + 1
        if icount >= 40:
            print 'Cannot kill process'
            sys.exit()
            break
    if inputfile is not None: fin.close()
    if outputfile is not None: fout.close()
    return

def check_pid(pid):
    """ Check For the existence of a unix pid. """
    try:
        os.kill(pid, 0)
    except OSError:
        # process is not running
        return False
    else:
        # process is still running
        return True

def shell_command_nooutput_simple(cmd):
    Popen(cmd,shell=True)
    return

def rm_rf(d):
    for path in (os.path.join(d,f) for f in os.listdir(d)):
        if os.path.isdir(path):
            rm_rf(path)
        else:
            os.unlink(path)
    os.rmdir(d)

def rm_rf_shell(path):
    shell_command_nooutput_simple('rm -r %s'%(path))

def clean_pwmat(path):
    if os.path.exists(path+'/OUT.WG'): os.remove(path+'/OUT.WG')
    if os.path.exists(path+'/OUT.RHO'): os.remove(path+'/OUT.RHO')
    if os.path.exists(path+'/OUT.WG_2'): os.remove(path+'/OUT.WG_2')
    if os.path.exists(path+'/OUT.RHO_2'): os.remove(path+'/OUT.RHO_2')
    if os.path.exists(path+'/OUT.VR'): os.remove(path+'/OUT.VR')
    if os.path.exists(path+'/OUT.VR_2'): os.remove(path+'/OUT.VR_2')
    if os.path.exists(path+'/MOVEMENT'): os.remove(path+'/MOVEMENT')

### QE: find lattice parameter
def qe_find_parameters(qefile_in):
    n=re.compile(r"\s*bravais-lattice index\s+=\s+([\-\d]*)")
    p=re.compile(r"\s*celldm\(1\)=\s+([\d\.\d]+)\s")
    q=re.compile(r"\s*unit-cell volume[\s\w\(\)]+=\s+([\d\.\d]+)\s+\(a\.u\.\)\^3")
    r=re.compile(r"\s*number of atoms\/cell[\s\w\(\)]+=\s+([\d]+)")
    s=re.compile(r"\s*number of electrons[\s\w\(\)]+=\s+([\d\.\d]+)")
    t=re.compile(r"\s*number of Kohn-Sham states=\s+([\d]+)")
    v=re.compile(r"\s*Exchange-correlation\s+=\s+([A-Z\s]+)")
    u=re.compile(r"\s*Program PWSCF (v[\d\.]+)")

    ibrav_index=int(n.findall(qefile_in)[-1] )
    alat=float_all(p.findall(qefile_in))[-1]
    volume=float_all(q.findall(qefile_in))[-1]
    natom=int(r.search(qefile_in).group(1))
    nelec=float(s.search(qefile_in).group(1))
    nbnd=int(t.search(qefile_in).group(1))
    ex=v.search(qefile_in).group(1)
    version=u.search(qefile_in).group(1)
    return ibrav_index, alat, volume, natom, nelec, nbnd, ex, version

### QE: find lattice vector
def qe_find_lattice(qefile_in):
    p=re.compile(r"\s*a\(\d\)\s=\s\(([\s\d\.\-]+)\s+\)")
    lattice=np.array([ map(float,iraw.split()) for iraw in p.findall(qefile_in) ])
    return lattice

### QE: find total energy
def qe_find_energy(qefile_in):
    p=re.compile(r"!\s+total energy\s+=\s+([\-\d\.]+)\s+Ry")
    energy = p.findall(qefile_in)
    return energy

### QE: find atomic coordinates
def qe_find_coord(qefile_in,natom):
    name=[];coord=[]
    p=re.compile(r"\s*\d+\s*([A-Z][a-z]*)\s+tau\(\s*\d+\)\s*=\s*\(([\s\d\.\-]+)\)")
    rawcoord=p.findall(qefile_in)
    for i in range(0,len(rawcoord)):
        name.append(rawcoord[i][0])
        coord.append(s_string(rawcoord[i][1]))
    return name[0:natom],np.array(coord)

### QE: find optimized atomic coordinates
def find_opt_coord(qefile_in):
    coord = []; name = [];  fix = []
    p = re.compile(r"ATOMIC_POSITIONS \([a-z]*\)\n(([A-Z][a-z]*\s*[\s\d\-\.]+)+)\n")
    rawcoord = p.findall(qefile_in)
    if len(rawcoord)!= 0:
        for iraw in rawcoord:
            iraw_split = iraw[0].rstrip('\n').split('\n')
            name_single = []; coord_single = []; fix_single = []
            for jraw in iraw_split:
                name_single.append( jraw.split()[0] )
                coord_single.append( map(float,jraw.split( )[1:4]) )
                if len(jraw.split()) >4:
                    fix_single.append( map(int,jraw.split( )[4:]) )
                else:
                    fix_single.append( [1, 1, 1] )
            name.append(name_single)
            coord.append(np.array(coord_single))
            fix.append(np.array(fix_single))
    else:
        print 'Cannot find optimized coordinates, exit()'
        sys.exit()
    return name, coord, fix

### QE: find optimized lattice coordinates
def find_opt_vect(qefile_in):
    vect = []
    p = re.compile(r"CELL_PARAMETERS \(alat= [\d\.]*\)\n\s*([\d\.\s\-]+)\n " )
    rawvect = p.findall(qefile_in)
    return rawvect

def line2array( lines, line_ind ):
    array = []
    for ind,iline in enumerate(lines):
        iline_tmp = iline.strip().strip('\n').split()
        try:
            iline_float = [float(i) for i in iline_tmp]
            array.append(iline_float)
        except:
            print 'Error when reading MOVEMENT at line: ', line_ind+ind
            print iline
            sys.exit()
    return np.array( array )

def read_movement_file( filename, UseForce=False, UseVel=False, UsePot=False, UseChar=False ):
    with open(filename, 'r') as f:
        raw = f.readlines()
    nline = len(raw)
    natom = int(raw[0].strip().split()[0])
    coords = []; lat = []; e_tot = []
    force = None; velocity = None; potential = None; Time = None; charge = None
    with_force = False; with_velocity = False; with_potential = False; with_charge = False
    info_line = 0; jump = None
    if 'iteration' in raw[0].strip().lower(): Time = []
    if 'MD_INFO' in raw[1]:
        info_line = 4
    jump = 7 + natom + info_line
    coord_range = [6+info_line,6+info_line+natom]
    if 'force' in raw[jump-1].strip().lower():
        force_range = [jump,jump+natom]
        jump = jump + 1 + natom
        with_force = True
    if 'velocity' in raw[jump-1].strip().lower():
        velocity_range = [jump,jump+natom]
        jump = jump + 1 + natom
        with_velocity = True
    if 'potential' in raw[jump-1].strip().lower():
        potential_range = [jump,jump+natom]
        jump = jump + 1 + natom
        with_potential = True
    if 'atomic_charge' in raw[jump-1].strip().lower():
        charge_range = [jump,jump+natom]
        jump = jump + 1 + natom
        with_charge = True
    if with_force: force = []
    if with_velocity: velocity= []
    if with_potential: potential = []
    if with_charge: charge = []
    p1=re.compile(r"Iteration\s*=\s*([\d\.\-\E\+]+)")
    p2=re.compile(r"Etot,Ep,Ek\s*=\s*([\d\.\-E\+]+)")
    for i in range(0,nline,jump):
        Time.append( p1.search( raw[i] ).group(1) )
        e_tot.append( float(p2.search( raw[i] ).group(1)) )
        lat.append( line2array(raw[i+2+info_line:i+5+info_line],i) )
        coords.append( line2array(raw[i+coord_range[0]:i+coord_range[1]],i) )
        if with_force is True and UseForce is True:
            force.append( line2array(raw[i+force_range[0]:i+force_range[1]],i) )
        if with_velocity is True and UseVel is True:
            velocity.append( line2array(raw[i+velocity_range[0]:i+velocity_range[1]],i) )
        if with_potential is True and UsePot is True:
            potential.append( line2array(raw[i+potential_range[0]:i+potential_range[1]],i) )
        if with_charge is True and UseChar is True:
            charge.append( line2array(raw[i+charge_range[0]:i+charge_range[1]],i) )
    return Time, e_tot, lat, coords, force, velocity, potential, charge


#
