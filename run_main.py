
import sys, os
import numpy as np
from ga import shape, input_molecules, input_atoms, cross_mutation, structure_energy_list, ga_search, adjust_positions
from interface import relaxation_io, relaxation_run
import shutil


pwd = os.getcwd()


# =========  setup system parameters =========
structure_energy_file = 'structure-energy.dat'   # specify the file name to record structure and energy
Num_Process = 9   # how many relaxations are launched at one time
cores_adjust = 9  # number of processes for GA adjusting (usually is the number of populations)
para_cmd = 'srun -n 2 -N 1 '   # MPI launch wrapper 
Package = 'pwmat'        # package string
exec_cmd = 'PWmat   '    # executable
MaxGenerations = 1000    # maximum generation allowed
MaxRunTime = 25000   # how long (second) can a DFT relaxation run (will kill job if it exceeds this time)
files_dump = ['REPORT','RELAXSTEPS']   # DFT output files you want to archive (saved in dump folder)
# kill an MPI job could be challenging. It is better to setup max relaxation
#    step in a DFT input and give long enough MaxRunTime to finish the DFT
#    relaxations by itself

# this file list contains the files used for DFT relaxations, 
#    depending on the specific package used.
SetupInput = ['etot.input','O.SG15.NCPP.PBE.UPF','H.SG15.NCPP.PBE.UPF','ONCV.PWM.Cu.UPF','IN.SOLVENT']

# =========  setup system parameters =========



# ==============  setup restart  =============
# if restart, the ga_cycle will do ga_cycle(1) at the beginning, 
#    so structure-energy.dat should be in the directory, 
#    $i coords file should also be there
#Restart = True
Restart = False
with open('structure-energy.dat','r') as f:
    strgen = int(f.readlines()[0].split()[0])
StartGeneration = strgen
if not Restart and StartGeneration != 0:
    print 'Start from scratch but with non-zero StartGeneration index'
    sys.exit()
if Restart: 
    print ''
    print ''
    print 'Restart from Generation %d'%(StartGeneration)
# ==============  setup restart  =============



# ===========  setup system details ==========
# start here to setup the model, please check each parameter to change for the specific system

#  set up structure_energy instance
struct_energy = structure_energy_list( structure_energy_file )

d_shape = shape('cylinder', OriginCoord=np.array([4.4, 7.60, 6.]), Radius=7.2, MaxZ=8.0, MinZ=6.3, WithSubstrate=True, SubstrateShapeStr='surface', SubstrateAtomsSpecies=['Cu'])

NWater = 20
InputMole = [[x,x*2+NWater,x*2+NWater+1] for x in range(0,NWater)]
    
#     The name of each atom 
InputMoleName =  ['O']*NWater + ['H']*NWater*2
InputMoleCoord = [np.array([[0.0,0.0,0.0],[-0.7563,0.5867,0.0],[0.7563,0.5867,0.0]])]*NWater   # unfixed nine molecules H2O

#FixMolecules = []
d_molecules = input_molecules(ForceInputMole=InputMole, ForceInputMoleName=InputMoleName, ForceInputMoleCoord=InputMoleCoord )
#d_atoms = input_atoms(ForceInputMole=InputMole, ForceInputMoleName=InputMoleName, ForceInputMoleCoord=InputMoleCoord, FixMolecule=None)

# set up cross/mutation methods
d_crossmut = cross_mutation( Mutation=True, MutationMethod=2, EveryNStep_Random=10, CrossingMethod=6, CrossRotation=False, MethodProbFile=None, FixCrossingMethod=True, FixMutationMetion=True, Mute2_Search_Region_Radius=3.5 )

# set up molecular position adjusting methods
d_adjust_positions = adjust_positions(Repul_Force_Thrd=0.2, MinMole_Thrd=2.7,MinAtom2Plane_Thrd=0.2,RepulsionOnly=True)

# set up IO
IOrelaxation = relaxation_io( Package, pwd )

# set up parallelization
MPrelaxation = relaxation_run( Package, nproc=Num_Process, para_cmd=para_cmd, package_cmd_path='/home/zhengf/scratch/box-of-water/ga_tmp/checkGA3', package_exec=exec_cmd, max_run_time=MaxRunTime )

# initialize ga class
ga = ga_search(  struct_energy, d_shape, d_crossmut, d_adjust_positions, d_molecule=d_molecules, UseInputCoord=False, NProc=cores_adjust, FixMole_DFT=True, Energy_Selection_Thrd=0.020 )
#ga = ga_search(  struct_energy, d_shape, d_crossmut, d_adjust_positions, d_atom=d_atoms, UseInputCoord=False, NProc=cores_adjust, FixMole_DFT=True, Energy_Selection_Thrd=0.005 )
# ===========  setup system details ==========



# ==== following does not need necessary setup ====

# start from process=0 (random generation of initial molecules)

# if start from scratch
if not Restart:

    #    initialize the molecules (generate them randomly if no InputCoords are given)
    print ''
    print '-------------------------- Entering Generation  : %d -------------------------------'%(0)
    ga.ga_cycle(0)
    PopFileNameList = ga.PopFileNameList


    # start the external relaxation 

    #    copy generated coords to ./work/$i-relax
    IOrelaxation.copy_input( pwd+'/'+'work/',PopFileNameList )

    #    copy setup files to ./work/$i-relax
    IOrelaxation.copy_setup_input( pwd+'/'+'work/', PopFileNameList, SetupInput )

    #    transform format
    IOrelaxation.work_coords2relax( PopFileNameList )

    #    run relaxation
    print 'Performing external relaxations ...'
    MPrelaxation.opt_relaxation( PopFileNameList, pwd )

    #    transform format and extract tot energy
   #PopFileNameList = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    RelaxationEnergy = IOrelaxation.work_relax2coords(PopFileNameList)

    #    copy output structure to parent dir
    IOrelaxation.copy_output( pwd,PopFileNameList )

    #    append to structure-energy file for newly relaxed structure
    struct_energy.append_file( PopFileNameList, RelaxationEnergy, flag='RelaxOut' )

    #    based on relaxed energy, perform natural selection (Process = 2)
    ga.ga_cycle(2)

    #    dump/clean parent and work dirs
    IOrelaxation.dump_files(pwd,0,PopFileNameList,PopFileNameList,ga.StructureEnergyFile,MethodProbFile=d_crossmut.MethodProbFile,DumpFilesList=files_dump,InitialStep=True)



if Restart:
    IOrelaxation.clean_work_dir(pwd)

if not Restart:
    StartGeneration = 1



# after initial step, enter the looping for following generations
for igen in range(StartGeneration,MaxGenerations):

    print ''
    print ''
    print '-------------------------- Entering Generation  : %d -------------------------------'%(igen)

    #    performing crossing/mutation 
    ga.ga_cycle(1)
    RelaxFileNameList = ga.RelaxFileNameList[:]
    TestGenInd = ga.GenerationIndex
    if igen != TestGenInd:
        print '   WARNING: Inconsistent Generation Index from structure-energy file'

    # start the external relaxation 

    #    copy generated coords to ./work/$i-relax
    IOrelaxation.copy_input( pwd+'/work/',RelaxFileNameList, flag='RelaxIn' )

    #    copy setup files to ./work/$i-relax
    IOrelaxation.copy_setup_input( pwd+'/work/', RelaxFileNameList, SetupInput )

    #    transform format
    IOrelaxation.work_coords2relax( RelaxFileNameList )

    #    run relaxation
    print 'Performing external relaxations ...'
    MPrelaxation.opt_relaxation( RelaxFileNameList, pwd )

    #    transform format
    RelaxationEnergy = IOrelaxation.work_relax2coords(RelaxFileNameList)

    #    copy output structure to parent dir
    IOrelaxation.copy_output( pwd,RelaxFileNameList )

    #    append to structure-energy file for newly relaxed structure
    struct_energy.append_file( RelaxFileNameList, RelaxationEnergy, flag='RelaxOut' )

    #    based on relaxed energy, perform natural selection (Process = 2)
    ga.ga_cycle(2)
    PopFileNameList = ga.PopFileNameList[:]

    #    dump/clean parent and work dirs
    IOrelaxation.dump_files(pwd,igen,PopFileNameList,RelaxFileNameList,ga.StructureEnergyFile,MethodProbFile=d_crossmut.MethodProbFile,DumpFilesList=files_dump)




