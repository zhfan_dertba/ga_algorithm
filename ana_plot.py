
import numpy as np
import sys
import matplotlib.pyplot as plt

def _read_coord_single_file( filename ):
    # use Angstrom unit
    with open(filename,'r') as f:
        raw = f.readlines()
    coord = []; name = []; fix = []; lat = np.zeros( (3,3) )
    for order,i in enumerate(raw[0:3]):
        tmp_ = [float(j.strip().strip('\n')) for j in i.split()]
        lat[order,:] = np.array( tmp_ )
    lat_const = float(raw[3].strip().strip('\n').split()[0])
    lat = lat * lat_const   # lat is Angstrom
    if len(raw) > 4:
        for i in raw[4:]:
            tmp_ = [j.strip().strip('\n') for j in i.split()]
            name.append(tmp_[0])
            tmp_2 = map(float, tmp_[1:4])
            coord.append( tmp_2 )
            if (len(tmp_)>=7):
                tmp_3 = map(int, tmp_[4:7])
                fix.append( tmp_3 )
            else:
                fix.append( np.array([1, 1, 1]) )
    else:
        print 'No coordinates found for %s'%(filename)
    return name, lat, np.array( coord ), np.array( fix ) # name, lattice vector, coord_frac, coord_cart

def color_func( val ):
    low = 30.; high = 240.
    bot_z = 0.4; top_z = 2.1
    if val <= bot_z:
        return [low/255., low/255., low/255.]
    else:
        tmp = (val-bot_z)/(top_z-bot_z)*(high-low)/255. + low/255.
        return [ tmp, tmp, tmp]

name, lat, coord, fix = _read_coord_single_file( sys.argv[1] )

Cu_atm_ind = [i for i, iname in enumerate(name) if iname == 'Cu']

Cu_z_top = np.amax( coord[Cu_atm_ind][:,2] )

Cu_top_ind = [i for i, iname in enumerate(name) if iname == 'Cu' and coord[i,2]>=3.5]
Cu_coord = coord[Cu_top_ind]


z_thread = 4.13

O_atm_ind = [i for i, iname in enumerate(name) if iname == 'O' and coord[i,2]-Cu_z_top <= z_thread]

O_coord = coord[O_atm_ind]

O_z_bot = np.amin( O_coord[:,2] )
O_z_top = np.amax( O_coord[:,2] )
O_num = len(O_atm_ind)

print O_z_bot, O_z_top

for i in range(0,O_num):
    color = color_func( O_coord[i,2]-O_z_bot )
    print O_coord[i,2]-O_z_bot
    plt.scatter(O_coord[i,0], O_coord[i,1], s=500, marker='o', c=color)

for i in range(0,O_num-1):
    for j in range(i+1,O_num):
        if np.linalg.norm( O_coord[i,:]-O_coord[j,:] ) <= 3.0:
            plt.plot( [O_coord[i,0], O_coord[j,0]], [O_coord[i,1],O_coord[j,1]], 'r--' )

for i in range(0,len(Cu_top_ind)):
    plt.scatter(Cu_coord[i,0], Cu_coord[i,1], s=50, marker='x', c='b')
    

plt.xlim(-5.0, 13.0)
plt.ylim(-0.5, 17.5)
plt.show()
    

