
import sys, os
import numpy as np
from ga import shape, input_molecules, atoms_class, cross_mutation, structure_energy_list, ga_search, adjust_positions
from interface import relaxation_io, relaxation_run


pwd = os.getcwd()


# =========  setup system parameters =========
structure_energy_file = 'structure-energy.dat'   # specify the file name to record structure and energy
Num_Process = 10   # how many relaxations are launched at one time
cores_adjust = 6   # number of processes for GA adjusting (usually is the number of populations)
para_cmd = 'srun -n 64 -N 2 '   # MPI launch wrapper 
Package = 'qe'        # package string
exec_cmd = 'pw.x -npool 1 -ndiag 16 -ntg 4  '    # executable
MaxGenerations = 1000    # maximum generation allowed
MaxRunTime = 25000    # how long (second) can a DFT relaxation run (will kill job if it exceeds this time)
files_dump = ['in_relax','out_relax']   # DFT output files you want to archive (saved in dump folder)
# kill an MPI job could be challenging. It is better to setup max relaxation
#    step in a DFT input and give long enough MaxRunTime to finish the DFT
#    relaxations by itself


# this file list contains the files used for DFT relaxations, 
#    depending on the specific package used.
SetupInput = ['in_relax','O.SG15.NCPP.PBE.UPF','H.SG15.NCPP.PBE.UPF','C.SG15.NCPP.PBE.UPF','Cu.SG15.NCPP.PBE.UPF']
# =========  setup system parameters =========



# ==============  setup restart  =============
# if restart, the ga_cycle will do ga_cycle(1) at the beginning, 
#    so structure-energy.dat should be in the directory, 
#    $i coords file should also be there
#Restart = True
Restart = False
with open('structure-energy.dat','r') as f:
    strgen = int(f.readlines()[0].split()[0])
StartGeneration = strgen
if not Restart and StartGeneration != 0:
    print 'Start from scratch but with non-zero StartGeneration index'
    sys.exit()
if Restart: print 'Restart from Generation %d'%(StartGeneration)
# ==============  setup restart  =============



# ===========  setup system details ==========
# start here to setup the model, please check each parameter to change for the specific system

#  set up structure_energy instance
struct_energy = structure_energy_list( structure_energy_file )

# d_shape: set up the system description (surface/cluster/with substrate?)
#     e.g.(surface case) d_shape = shape('Surface',WithSubstrate=True,SubstrateAtomsSpecies=['Cu'],IniSampleDistToCenter=4.)
#     e.g.(cluster case) d_shape = shape('Cluster',WithSubstrate=True,SubstrateAtomsSpecies=['Ti','Ot'],InitOuterRadius=5.5,MoveToCenter='xyz' )
#     e.g.(cluster case with constrain) d_shape = shape( 'Cluster', WithSubstrate=False, BoxSize=8.0, MoveToCenter='xyz',ConstrainPlane=ConstrainPlane, ConstrainPolar=ConstrainPolar )
#     Specify constrain planes (this is mainly to limit molecules to form in a specific shape)
#ConstrainPlane = [[[0.0, 0.0, 1.0, 2.1], 1],[[0.0,0.0,1.0, 14.0],-1]]  means two planes, first-plane function z=2.1 (Angstrom), molecules should be above this plane
#                                                                             the other plane z=14.0, molecules should be below this plane.
#ConstrainPolar = [[[3.15, 6.83, 5.44],None, 5.5]]  means one spherical constrain, its center is at coord (3.15, 6.83, 5.44), the inner radius is not needed,
#                                                                             the outer radius is 5.5 Angstrom, molecules should be within outer- and innter-radius
d_shape = shape( SubstrateShapeStr='surface', WithSubstrate=True, SubstrateAtomsSpecies=['Cu'] )

# set up input molecules  ! remember PWmat will re-sort atom for final.config
#     Specify which atoms belong to A molecule
InputMole = [[0,1],[2,17,18, 19],[3,20,21],[4,22,23],[5,24,25],[6,26,27],[7,28,29],[8,30,31],[9,32,33],[10,34,35],[11,36,37],[12,38,39],[13,40,41],[14,42,43],[15,44,45],[16,46,47]]
#     The name of each atom 
InputMoleName =  ['C'] + ['O']*16 + ['H']*31
#     The coordinates for each atom mentioned in InputMoleName
InputMoleCoord = [ np.array([[2.607064 ,  4.600990 ,  4.152463],
                             [2.598904 ,  4.613181 ,  5.331681]]), 
                   np.array([[4.690877 ,  4.732276 ,  7.288227],
                             [4.711815 ,  5.760573 ,  7.542812],
                             [4.351210 ,  4.094700 ,  8.114064],
                             [4.068133 ,  4.621081 ,  6.538410]]),
                   np.array([[2.173721 ,  8.145266 ,  8.173797],
                             [2.137906 ,  8.777692 ,  8.919514],
                             [1.848341 ,  8.648043 ,  7.399736]]),
                   np.array([[1.262592 ,  3.397285 ,  9.396255],
                             [0.918079 ,  3.107119 , 10.263202],
                             [0.896307 ,  4.301945 ,  9.249942]]),
                   np.array([[3.919505 ,  3.200274 ,  9.077585],
                             [4.406701 ,  3.269019 ,  9.923079],
                             [2.942043 ,  3.301851 ,  9.278010]]),
                   np.array([[4.759049 ,  7.171030 ,  7.925046],
                             [3.860692 ,  7.589241 ,  7.950799],
                             [5.325180 ,  7.730763 ,  7.356735]]),
                   np.array([[0.206246 ,  5.956350 ,  8.946425],
                             [ 0.818419,   6.644462,   8.619243],
                             [-0.559787,   5.979268,   8.340709]]) ] + [np.array([[0.0,0.0,0.0],[-0.7563,0.5867,0.0],[0.7563,0.5867,0.0]])]*9
#     the 0-th to 6-th molecules (from InputMole list) are fixed during the GA, its cooridnates will be from InputMoleCoord directly. (There position may not be fixed during DFT relaxation, set FixMole_DFT=True to also fix them in DFT.)
FixMolecules = [0,1,2,3,4,5,6]
d_molecules = input_molecules(ForceInputMole=InputMole, ForceInputMoleName=InputMoleName, ForceInputMoleCoord=InputMoleCoord, FixMolecule=FixMolecules)

# set up cross/mutation methods
d_crossmut = cross_mutation( Mutation=True, MutationMethod=2, EveryNStep_Random=10, CrossingMethod=6, CrossRotation=False, MethodProbFile=None, FixCrossingMethod=True, FixMutationMetion=True, Mute2_Search_Region_Radius=3.5 )

# set up molecular position adjusting methods
d_adjust_positions = adjust_positions(Repul_Force_Thrd=0.2, MinMole_Thrd=2.7,MinAtom2Plane_Thrd=0.2,RepulsionOnly=True)

# set up IO
IOrelaxation = relaxation_io( Package, pwd )

# set up parallelization
MPrelaxation = relaxation_run( Package, nproc=Num_Process, para_cmd=para_cmd, package_exec=exec_cmd, max_run_time=MaxRunTime )

# initialize ga class
ga = ga_search(  struct_energy, d_shape, d_crossmut, d_adjust_positions, d_molecule=d_molecules, UseInputCoord=False, NProc=cores_adjust, FixMole_DFT=True, Energy_Selection_Thrd=0.005 )
# ===========  setup system details ==========




# ==== following does not need necessary setup ====

# start from process=0 (random generation of initial molecules)

# if start from scratch
if not Restart:

    #    initialize the molecules (generate them randomly if no InputCoords are given)
    ga.ga_cycle(0)
    PopFileNameList = ga.PopFileNameList

    # start the external relaxation 

    #    copy generated coords to ./work/$i-relax
    IOrelaxation.copy_input( pwd+'/'+'work/',PopFileNameList )

    #    copy setup files to ./work/$i-relax
    IOrelaxation.copy_setup_input( pwd+'/'+'work/', PopFileNameList, SetupInput )

    #    transform format
    IOrelaxation.work_coords2relax( PopFileNameList )
    sys.exit()
    #    run relaxation
    print 'Performing external relaxations ...'
    MPrelaxation.opt_relaxation( PopFileNameList, pwd )

    #    transform format and extract tot energy
    RelaxationEnergy = IOrelaxation.work_relax2coords(PopFileNameList)

    #    copy output structure to parent dir
    IOrelaxation.copy_output( pwd,PopFileNameList )

    #    append to structure-energy file for newly relaxed structure
    struct_energy.append_file( PopFileNameList, RelaxationEnergy, flag='RelaxOut' )

    #    based on relaxed energy, perform natural selection (Process = 2)
    ga.ga_cycle(2)

    #    dump/clean parent and work dirs
    IOrelaxation.dump_files(pwd,0,PopFileNameList,PopFileNameList,ga.StructureEnergyFile,MethodProbFile=d_crossmut.MethodProbFile,DumpFilesList=files_dump,InitialStep=True)


if Restart:
    IOrelaxation.clean_work_dir(pwd)

if not Restart:
    StartGeneration = 1


# after initial step, enter the looping for following generations
for igen in range(StartGeneration,MaxGenerations):

    print ''
    print ''
    print '-------------------------- Entering Generation  : %d -------------------------------'%(igen)

    #    performing crossing/mutation 
    ga.ga_cycle(1)
    RelaxFileNameList = ga.RelaxFileNameList[:]
    TestGenInd = ga.GenerationIndex
    if igen != TestGenInd:
        print '   WARNING: Inconsistent Generation Index from structure-energy file'

    # start the external relaxation 

    #    copy generated coords to ./work/$i-relax
    IOrelaxation.copy_input( pwd+'/work/',RelaxFileNameList, flag='RelaxIn' )

    #    copy setup files to ./work/$i-relax
    IOrelaxation.copy_setup_input( pwd+'/work/', RelaxFileNameList, SetupInput )

    #    transform format
    IOrelaxation.work_coords2relax( RelaxFileNameList )

    #    run relaxation
    print 'Performing external relaxations ...'
    MPrelaxation.opt_relaxation( RelaxFileNameList, pwd )

    #    transform format
    RelaxationEnergy = IOrelaxation.work_relax2coords(RelaxFileNameList)

    #    copy output structure to parent dir
    IOrelaxation.copy_output( pwd,RelaxFileNameList )

    #    append to structure-energy file for newly relaxed structure
    struct_energy.append_file( RelaxFileNameList, RelaxationEnergy, flag='RelaxOut' )

    #    based on relaxed energy, perform natural selection (Process = 2)
    ga.ga_cycle(2)
    PopFileNameList = ga.PopFileNameList[:]

    #    dump/clean parent and work dirs
    IOrelaxation.dump_files(pwd,igen,PopFileNameList,RelaxFileNameList,ga.StructureEnergyFile,MethodProbFile=d_crossmut.MethodProbFile,DumpFilesList=files_dump)




